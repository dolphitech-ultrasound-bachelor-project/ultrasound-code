import pickle
from typing import List

import cv2

from project.archive.featurePoints import brisk
from sklearn.ensemble import RandomForestClassifier

from project.experimental.datasetCreator import Entry

with open('../resources/dataset/dataset.dta', 'rb') as inputData:
    dta: List[Entry] = pickle.load(inputData)

entries: List = []
for i in dta:
    entries.append(dta.pop().maxampcscan)

briskEntries = brisk(entries)

kp: List[cv2.KeyPoint] = []
for i, item in enumerate(briskEntries):
    kp.append(item[0])

desc: List[cv2.DescriptorMatcher] = []
for i, item in enumerate(briskEntries):
    desc.append(item[1])

clf = RandomForestClassifier(max_depth=2, random_state=0)
clf.fit(desc, kp)
print(clf.feature_importances_)
print(clf.predict([[0, 0, 0, 0]]))
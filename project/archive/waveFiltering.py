import pickle

import numpy as np
import matplotlib.pyplot as plt

from general import ustool
from dataset.trainingDtaCreator import EntryMaxamp


def lowPassFilter(waveform, cutFreq=0.1, band=0.08):
    """
    Removes the higher frequencies in a signal of data
    From https://plot.ly/python/v3/fft-filters/, 2.3.2020
    Returns:

    """
    N = int(np.ceil((4 / band)))
    if not N % 2:
        N += 1
    n = np.arange(N)

    sinc_func = np.sinc(2 * cutFreq * (n - (N - 1) / 2.))
    window = 0.42 - 0.5 * np.cos(2 * np.pi * n / (N - 1)) + 0.08 * np.cos(4 * np.pi * n / (N - 1))
    sinc_func = sinc_func * window
    sinc_func = sinc_func / np.sum(sinc_func)

    new_signal = np.convolve(wave, sinc_func)

    plt.plot(new_signal)
    plt.title("New signal")
    plt.show()


with open('../resources/dataset/dataset.dta', 'rb') as inputData:
    dta: list = pickle.load(inputData)

# Load four samples; '123_3', '123_5', 'airbus1_1', and 'airbus1_5'
# 123_3
sampleA: EntryMaxamp = dta[0]
waveA = sampleA.maxamp
if sampleA.name != "123_3":
    raise ValueError("Sample A is incorrect, should be 123_3 but is " + sampleA.name)

# 123_5
sampleB: EntryMaxamp = dta[2]
waveB = sampleB.maxamp
if sampleB.name != "123_5":
    raise ValueError("Sample B is incorrect, should be 123_5 but is " + sampleB.name)

# airbus1_1
sampleC: EntryMaxamp = dta[4]
waveC = sampleC.maxamp
if sampleC.name != "airbus1_1":
    raise ValueError("Sample C is incorrect, should be airbus1_1 but is " + sampleC.name)

# airbus1_5
sampleD: EntryMaxamp = dta[6]
waveD = sampleD.maxamp
if sampleD.name != "airbus1_5":
    raise ValueError("Sample D is incorrect, should be airbus1_5 but is " + sampleD.name)

# List of the image databb
waves = [waveA, waveB, waveC, waveD]

x = 10
y = 110
wave = ustool.getAScan(waveA, x, y)
plt.plot(wave)
plt.title("Original wave, from " + str(x) + " to " + str(y))
plt.show()

lowPassFilter(wave)
lowPassFilter(np.abs(wave))

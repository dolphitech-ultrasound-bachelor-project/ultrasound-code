import numpy as np
import tensorflow as tf
from project.archive.featurePoints import brisk


def getFeaturePoints(data):
    """

    Args:
        data:

    Returns:

    """
    brisk(data)



def fcnn(data, labels):
    """
    Load data, construct model, fit/run model
    Returns: nothing

    """
    data = np.asarray(data)
    labels = np.asarray(labels)

    # Construct model
    model = tf.keras.models.Sequential()

    model.add(tf.keras.layers.Flatten(input_shape=(128, 128)))

    # Four dense (fully connected) layers
    model.add(tf.keras.layers.Dense((128 * 128)))
    model.add(tf.keras.layers.Activation('relu'))

    model.add(tf.keras.layers.Dense(10))
    model.add(tf.keras.layers.Activation('relu'))

    model.add(tf.keras.layers.Dense(8))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(1))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.compile(loss="binary_crossentropy",
                  optimizer="adam",
                  metrics=['accuracy'])

    return model.fit(data, labels, batch_size=1, validation_split=0.1)


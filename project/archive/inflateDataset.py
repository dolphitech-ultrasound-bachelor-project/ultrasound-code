"""
Run from command line. Takes a folder as argument.
Will take each file in this folder, import it as an image, and perform transformations to it.
Transformed images will be saved in a new subfolder.

@author: Franz Vrolijk
@author: Shubham Pachori (original noise function)
"""

import sys
from pathlib import Path
import os
import cv2
from dataset.datasetAugmenterRandom import randomTransform

if len(sys.argv) > 1:
    # Make sure results-directory exists
    if not os.path.exists("../resources/inflateresults"):
        os.mkdir("../resources/inflateresults")
    # Get path from args, and retrieve all image files from this folder
    PATH_LIST = Path(sys.argv[1]).glob('**/*.png')
    N_IM = 0
    # For each file path, read the image and run transformation function
    for path in PATH_LIST:
        path_in_str = str(path)
        im = cv2.imread(path_in_str, 0)
        # Run the mutation method on each image
        N_IM, temp = randomTransform(im, N_IM, save=True)
        N_IM += 1
    print(N_IM)

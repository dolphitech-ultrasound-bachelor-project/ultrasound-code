"""
    Module with 3 functions (two of which you should use externally).
    See individual functions for documentation.
"""

import h5py
import numpy as np
import cv2


def loadAndCrop(path):
    """
    Not meant to be called externally.
    Loads .h5 file, crops it and returns the cropped array.
    Crops 10 units on each side.

    Args:
        path: file path to .h5 file

    Returns:
        Cropped array
    """
    h5file = h5py.File(path, "r")
    rawData = h5file["DCV2/frames/1/Yic"]

    # Data is stored as signed 16 bit integers.
    maxamp = 32767

    # rawData has on A-scan per column. Aggregate normalized max amplitudes into a new list.
    maxAmps = [max(rawData[:, i]) / maxamp for i in range(0, 16384)]

    #
    # A-scans are sampled column-major by the transducer matrix.
    # Lay out maxAmps correspondingly to produce a C-scan.
    scan = np.zeros((108, 108))
    for col in range(10, 118):
        for row in range(10, 118):
            scan[row-10, col-10] = maxAmps[col * 128 + row]
    return scan


def cropSave(fpath):
    """
    Loads, crops and saves one image from the file path.
    Image name will be "cropped_cscan.png".
    Args:
        fpath: file path to .h5 file

    Returns:
        Nothing
    """
    if fpath.endswith(".h5"):
        cscan = loadAndCrop(fpath)
        cscan *= 255
        cv2.imwrite("cropped_cscan.png", cscan)


def cropSaveAll(fpaths):
    """
    Loads, crops and saves several image from the list of file paths
    Image names will be "cropped_cscan0.png", "cropped_cscan1.png", and so on.
    Args:
        fpaths: list of file paths to .h5 files

    Returns:
        Nothing
    """
    for path in enumerate(fpaths, 1):
        if path.endswith(".h5"):
            cscan = loadAndCrop(path)
            cscan *= 255
            cv2.imwrite("cropped_cscan" + str(path) + ".png", cscan)

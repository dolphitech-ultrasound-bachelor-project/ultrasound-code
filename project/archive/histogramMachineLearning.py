import pickle
import pprint
from typing import List
import cv2
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.model_selection import train_test_split
import numpy as np

from dataset.trainingDtaCreator import EntryMaxamp

"""
Uses histograms of pixel intensities, extracted from C-Scan maximum amplitude images, in various machine learning
algorithms, and displays the results of these
"""

# Load image dataset
with open('../resources/dataset_maxamp.dta', 'rb') as inputData:
    dta: List[EntryMaxamp] = pickle.load(inputData)

# Create lists for the data (histograms) and labels (damage bool)
histograms: List = []
labels: List = []
entry: EntryMaxamp

# Fetch data and label from each sample (Entry)
for i, entry in enumerate(dta):
    labels.append(entry.damaged)
    histograms.append(np.ravel(cv2.calcHist([entry.maxamp], [0], None, [256], [0, 256])))

# Split into 80% training, 20% testing
TRAINFEATURES, TESTFEATURES, TRAINLABELS, TESTLABELS = train_test_split(histograms, labels, test_size=0.4)

# List of scores
scores: List = []

# RANDOM FOREST
print("Random Forest")
# Run random forest classification
CLF = RandomForestClassifier(max_depth=2, n_estimators=100)
CLF.fit(TRAINFEATURES, TRAINLABELS)

# Print the importance of each variable
# print(CLF.feature_importances_)

# Print prediction score
print("Predictions:\t", CLF.predict(TESTFEATURES))
print("Actual: \t\t", TESTLABELS)
scores.append("RandomForest Accuracy: " + str(CLF.score(TESTFEATURES, TESTLABELS) * 100) + "%")
print(scores[-1])

# AdaBoostClassifier
print("\n\nAda Boost")
CLF = AdaBoostClassifier()
CLF.fit(TRAINFEATURES, TRAINLABELS)

# Print prediction score
print("Predictions:\t", CLF.predict(TESTFEATURES))
print("Actual: \t\t", TESTLABELS)
scores.append("AdaBoost Accuracy: " + str(CLF.score(TESTFEATURES, TESTLABELS) * 100) + "%")
print(scores[-1])

# SVM (SVC)
print("\n\nSVC")
# Run SVC classification
CLF = SVC(gamma='auto')
CLF.fit(TRAINFEATURES, TRAINLABELS)

# Print prediction score
print("Predictions:\t", CLF.predict(TESTFEATURES))
print("Actual: \t\t", TESTLABELS)
scores.append("SVC Accuracy: " + str(CLF.score(TESTFEATURES, TESTLABELS) * 100) + "%")
print(scores[-1])

# SVM (Linear SVC)
print("\n\nLinear SVC")
# Run Linear SVC classification
CLF = LinearSVC(max_iter=10000)
CLF.fit(TRAINFEATURES, TRAINLABELS)

# Print prediction score
print("Predictions:\t", CLF.predict(TESTFEATURES))
print("Actual: \t\t", TESTLABELS)
scores.append("LinearSVC Accuracy: " + str(CLF.score(TESTFEATURES, TESTLABELS) * 100) + "%")
print(scores[-1])

# MLPClassifier
print("\n\nMultilayer Perceptron (MLP)")
CLF = MLPClassifier()
CLF.fit(TRAINFEATURES, TRAINLABELS)

# Print prediction score
print("Predictions:\t", CLF.predict(TESTFEATURES))
print("Actual: \t\t", TESTLABELS)
scores.append("MLP Accuracy: " + str(CLF.score(TESTFEATURES, TESTLABELS) * 100) + "%")
print(scores[-1])

# KNN
print("\n\nK Nearest Neighbours (KNN)")
CLF = KNeighborsClassifier()
CLF.fit(TRAINFEATURES, TRAINLABELS)

# Print prediction score
print("Predictions:\t", CLF.predict(TESTFEATURES))
print("Actual: \t\t", TESTLABELS)
scores.append("KNN Accuracy: " + str(CLF.score(TESTFEATURES, TESTLABELS) * 100) + "%")
print(scores[-1])

# GaussianProcessClassifier
print("\n\nGaussian Process (with Radial-Basis Function Kernel / Squared-Exponential Kernel:")
CLF = GaussianProcessClassifier(1.0 * RBF(1.0))
CLF.fit(TRAINFEATURES, TRAINLABELS)

# Print prediction score
print("Predictions:\t", CLF.predict(TESTFEATURES))
print("Actual: \t\t", TESTLABELS)
scores.append("Gaussian Process Accuracy: " + str(CLF.score(TESTFEATURES, TESTLABELS) * 100) + "%")
print(scores[-1])

# Decision Tree
print("\n\nDecision Tree")
CLF = DecisionTreeClassifier()
CLF.fit(TRAINFEATURES, TRAINLABELS)

# Print prediction score
print("Predictions:\t", CLF.predict(TESTFEATURES))
print("Actual: \t\t", TESTLABELS)
scores.append("Decision Tree Accuracy: " + str(CLF.score(TESTFEATURES, TESTLABELS) * 100) + "%")
print(scores[-1])

# GaussianNB
print("\n\nGaussian Naive Bayes")
CLF = GaussianNB()
CLF.fit(TRAINFEATURES, TRAINLABELS)

# Print prediction score
print("Predictions:\t", CLF.predict(TESTFEATURES))
print("Actual: \t\t", TESTLABELS)
scores.append("GaussianNB Accuracy: " + str(CLF.score(TESTFEATURES, TESTLABELS) * 100) + "%")
print(scores[-1])

# Quadratic Discriminant Analysis
print("\n\nQuadratic Discriminant Analysis")
CLF = QuadraticDiscriminantAnalysis()
CLF.fit(TRAINFEATURES, TRAINLABELS)

# Print prediction score
print("Predictions:\t", CLF.predict(TESTFEATURES))
print("Actual: \t\t", TESTLABELS)
scores.append("Quadratic Discriminant Analysis Accuracy: " + str(CLF.score(TESTFEATURES, TESTLABELS) * 100) + "%")
print(scores[-1])

# Comparison
print("\n\n")
PP = pprint.PrettyPrinter(indent=4)
PP.pprint(scores)

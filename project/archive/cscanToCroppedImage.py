"""
    Run fromm the command line.
    Takes any amount of .h5 files as argument. Will crop the 10 outer pixels in all directions,
    and save as image.

    Use argument "-o" to open files automatically.

    Usage example: python3 cscanToCroppedImage.py ../Data/ultrasonic_data.h5 -o

"""

import sys
import re
import cv2
from PIL import Image
from general.ustool import getImageByMaximum

print("Number of arguments (files and options): " + str((len(sys.argv) - 1)))

if len(sys.argv) > 1:
    for scan in range(1, len(sys.argv)):
        if str(sys.argv[scan]).endswith(".h5"):
            # sys.argv entries contain relative file paths, we need to fetch only the file name, without file type
            temp = re.search('resources/(.*).h5', sys.argv[scan])
            if temp:
                resource_name = temp.group(1)

                print("Processing file number " + str(scan) + ": " + resource_name + "...")

                cscan = getImageByMaximum(resource_name)
                cv2.imwrite("../resources/maxamp_cscan/cropped_cscan" + str(scan) + ".png", cscan)

                if "-o" in sys.argv:
                    img = Image.open("../resources/cropped_cscan" + str(scan) + ".png")
                    img.show()
    print("Processing complete.")

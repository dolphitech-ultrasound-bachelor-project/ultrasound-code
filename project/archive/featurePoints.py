"""
Helper class containing functions to extract feature points
"""
import pickle

import cv2
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

from project.experimental.imageFiltering import medianFilter
from project.experimental.datasetCreator import Entry


def sift(images: list):
    """

    Args:
        images:

    Returns:

    """

    for i, image in enumerate(images):
        # TODO: CV2 er lame, må manuelt implementere SIFT siden xfeatures2d ikke finnes lengre
        siftObject = cv2.xfeatures2d.SIFT_create()
        kp = siftObject.detect(image, None)
        siftImage = np.array(image)
        cv2.drawKeypoints(image, kp, siftImage)
        images[i] = siftImage

    return images


def orb(images):
    """

    Code from https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_feature2d/py_orb/py_orb.html,
    retrieved 4.3.2020

    Args:
        images:

    Returns: List of images with ORB keypoints added

    """
    orbObject: cv2.ORB = cv2.ORB_create(edgeThreshold=5, patchSize=8, nlevels=1, fastThreshold=20, scaleFactor=0.2,
                                        WTA_K=2,
                                        scoreType=cv2.ORB_HARRIS_SCORE, firstLevel=0, nfeatures=200)

    images = images.copy()

    for i, image in enumerate(images):
        image = np.asarray(image)
        image = image.astype(np.uint8, copy=False)
        images[i] = image

        kp: cv2.KeyPoint
        desc: cv2.DescriptorMatcher
        kp, desc = orbObject.detectAndCompute(image, mask=None)

        #images[i] = cv2.drawKeypoints(image, kp, color=(0, 255, 0), outImage=images[i])
        images[i] = kp, desc

    return images


def brisk(images):
    """

    Args:
        images:

    Returns:

    """
    briskObject: cv2.BRISK = cv2.BRISK_create()
    images = images.copy()

    for i, image in enumerate(images):
        image = np.asarray(image)
        image = image.astype(np.uint8, copy=False)
        images[i] = image

        kp: cv2.KeyPoint
        desc: cv2.DescriptorMatcher
        kp, desc = briskObject.detectAndCompute(image, mask=None)

        #images[i] = cv2.drawKeypoints(image, kp, color=(0, 255, 0), outImage=images[i])
        images[i] = kp, desc

    return images


def fast(images):
    """
    Uses Features from Accelerated Segment Test (FAST) to find edges on given images

    Args:
        images: The images to apply FAST to

    Returns: Images with keypoints from FAST overlayed

    """
    fastObject: cv2.FastFeatureDetector = cv2.FastFeatureDetector_create(threshold=25)
    images = images.copy()

    for i, image in enumerate(images):
        image = np.asarray(image)
        image = image.astype(np.uint8, copy=False)
        images[i] = image

        kp: cv2.KeyPoint = fastObject.detect(image, mask=None)
        images[i] = cv2.drawKeypoints(image, kp, color=(0, 255, 0), outImage=images[i])

    return images


def kaze(images):
    """

    Args:
        images:

    Returns:

    """
    kazeObject: cv2.KAZE = cv2.KAZE_create()
    images = images.copy()

    for i, image in enumerate(images):
        image = np.asarray(image)
        image = image.astype(np.uint8, copy=False)
        images[i] = image

        kp: cv2.KeyPoint
        desc: cv2.DescriptorMatcher
        kp, desc = kazeObject.detectAndCompute(image, mask=None)

        images[i] = cv2.drawKeypoints(image, kp, color=(0, 255, 0), outImage=images[i])

    return images

    pass


def getFeaturePoints(images: list = None):
    """
    Opens four samples from the dataset, and plots different filtering techniques for these images
    Args:
        images:         Images to plot

    Returns: None
    """
    # Open/load the dataset
    if images is None:
        with open('../../resources/dataset.dta', 'rb') as inputData:
            dta: list = pickle.load(inputData)

        # Load four sample images; '123_3', '123_5', 'airbus1_1', and 'airbus1_5'
        # 123_3
        sampleA: Entry = dta[0]
        imageA = Image.fromarray(sampleA.maxampcscan)
        if sampleA.name != "123_3":
            raise ValueError("Sample A is incorrect, should be 123_3 but is " + sampleA.name)

        # 123_5
        sampleB: Entry = dta[2]
        imageB = Image.fromarray(sampleB.maxampcscan)
        if sampleB.name != "123_5":
            raise ValueError("Sample B is incorrect, should be 123_5 but is " + sampleB.name)

        # airbus1_1
        sampleC: Entry = dta[4]
        imageC = Image.fromarray(sampleC.maxampcscan)
        if sampleC.name != "airbus1_1":
            raise ValueError("Sample C is incorrect, should be airbus1_1 but is " + sampleC.name)

        # airbus1_5
        sampleD: Entry = dta[6]
        imageD = Image.fromarray(sampleD.maxampcscan)
        if sampleD.name != "airbus1_5":
            raise ValueError("Sample D is incorrect, should be airbus1_5 but is " + sampleD.name)

        # List of the image data
        images = [imageA, imageB, imageC, imageD]

    # Fetch all feature points
    if images is not None:
        # Median filtering first
        images = medianFilter(images)

        # ORB
        orbImages = orb(images)
        plt.imshow(orbImages[2], "gray")
        plt.title("ORB")
        plt.imsave("orbC.eps", orbImages[2])
        plt.show()

        # BRISK
        briskImages = brisk(images)
        plt.imshow(briskImages[2])
        plt.title("BRISK")
        plt.imsave("briskC.eps", briskImages[2])
        plt.show()

        # FAST
        fastImages = fast(images)
        plt.imshow(fastImages[2])
        plt.title("FAST")
        plt.imsave("fastC.eps", fastImages[2])
        plt.show()

        # KAZE
        kazeImages = kaze(images)
        plt.imshow(kazeImages[2])
        plt.title("KAZE")
        plt.imsave("kazeC.eps", kazeImages[2])
        plt.show()

        # siftImages = sift(images)
        # plt.plot(siftImages[4])
        # plt.title("SIFT test")
        # plt.show()
import pickle

import cv2
import numpy as np
import tensorflow as tf
from sklearn.utils import shuffle
from general import confusionMatrix as confMat
from cscan import runSVM
from image import runFCNN


def cScanCNNTest():
    with open('resources/dataset/dataset_test_cscan.dta', 'rb') as input1:
        dta = pickle.load(input1)

    dataset = []
    labels = []

    for entry in dta:
        dataset.append(entry.data)
        labels.append(int(entry.damaged))

    dataset, labels = shuffle(dataset, labels)
    data = np.zeros((len(dataset), 50, 128, 128))

    for y in range(len(dataset)):
        # Make a temporary object
        copy = np.zeros((50, 128, 128))
        # For each row or column
        for i in range(128):
            # Reshape each layer individually
            copy[:, :, i] = cv2.resize(dataset[y][:, :, i], (128, 50))
        data[y, :, :, :] = copy

    model = tf.keras.models.load_model("resources/model/cScanCNN")

    predictions = model.predict(data)

    predictions[predictions >= 0.5] = 1
    predictions[predictions < 0.5] = 0

    confMat.getConfusionMatrix(labels, predictions)


def cScanSVMTest():
    runSVM.cScanSVM()


def maxAmpFCNNTest():
    with open('resources/dataset/dataset_test_image.dta', 'rb') as input1:
        dta = pickle.load(input1)

    dataset = []
    labels = []

    for entry in dta:
        dataset.append(entry.maxamp)
        labels.append(int(entry.damaged))

    model = runFCNN.maxAmpFCNN()
    dataset = np.asarray(dataset)
    predictions = model.predict(dataset)
    predictions[predictions >= 0.5] = 1
    predictions[predictions < 0.5] = 0
    confMat.getConfusionMatrix(labels, predictions)


if __name__ == "__main__":
    maxAmpFCNNTest()

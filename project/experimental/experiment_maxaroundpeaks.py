# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 18:55:47 2020

@author: Stig
"""

# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import scipy.signal
import numpy as np
import cv2 as cv

from sources import ustool
from sources import loader

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#


# Load the data
cScan, startGate, endGate = loader.load("../resources/ultrasound1.h5")
gatedCScan = gatedCScan[startGate:endGate]

responseFunction, localMinimas = ustool.getResponseFunction(gatedCScan)

plt.figure(); plt.plot(responseFunction)
for localMinima in localMinimas: plt.axvline(x=localMinima, color='red')

# List to store contours that will be extracted
contoursList = []

# For each slice, take the max and showcase a subplot
# of the normal image, blurred image and thresholded image
for i in range(1,len(localMinimas)):
    # Process image with standard computervision methods
    image = np.amax(data[localMinimas[i-1]:localMinimas[i]], axis=0)
    image = (ustool.normalize(image)*255).astype(np.uint8)
    blur = cv.bilateralFilter(image,7,100,100)
    __,threshold = cv.threshold(blur,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)

    # Save the contours
    contours,__ = cv.findContours(threshold, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    contoursList.append(contours)

    # Showcase the results of this layer
#    fig, ax = plt.subplots(1, 3,figsize=(10,10))
#    ax[0].imshow(image, "gray")
#    ax[1].imshow(blur,"gray")
#    ax[1].set_title("SLICE: "+str(dips[i-1])+" - "+str(dips[i]), fontsize=16)
#    ax[2].imshow(threshold,"gray")
#    plt.show()
    plt.figure(); plt.title("SLICE: "+str(localMinimas[i-1])+" - "+str(localMinimas[i]), fontsize=16); plt.imshow(image,"gray")

# In the end we also apply contours to the original image,
# here i only choose contour '0' since that is the one we are after
image = np.amax(data, axis=0)
image = image[:,:,None] + np.zeros(3)[None,None,:]
image = (ustool.normalize(image)*255).astype(np.uint8)
contourImage = image.copy()
cv.drawContours(contourImage, contoursList[0], -1, (255,255,0), 1)

fig, ax = plt.subplots(1,2,figsize=(10,10))
ax[0].imshow(image)
ax[1].imshow(contourImage)
plt.show()
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 19:06:24 2020

@author: Stig
"""
import sys
import matplotlib.pyplot as plt
from scipy.signal import resample
from scipy.signal import argrelmin
from scipy.signal import argrelmax
from scipy.ndimage import median_filter
import tensorflow as tf
import numpy as np
import pickle
import cv2
import os

import sources.loader as loader
import sources.ustool as ustool

np.set_printoptions(suppress=True)
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(linewidth=1000)

results = []
with open("./resources/imageresults.dta", 'rb') as binaryFile:
    while True:
        try:
            results = pickle.load(binaryFile)
        except EOFError:
            break

for i in range(len(results)):
    figure, axes = plt.subplots(1,4,figsize=(12,12))
    axes[0].imshow(results[i][0])
    axes[1].imshow(results[i][1])
    axes[2].imshow(results[i][2])
    axes[3].imshow(results[i][3])

#figure, axes = plt.subplots(15,12,figsize=(12,12))
#
#for i in range(len(results)):
#    j = np.floor(i/3)
#    k = (i%3)*4
#    axes[j,k+0].imshow(results[i,0])
#    axes[j,k+1].imshow(results[i,1])
#    axes[j,k+2].imshow(results[i,2])
#    axes[j,k+3].imshow(results[i,3])

    plt.setp(axes, xticks=[], yticks=[])
    plt.show()

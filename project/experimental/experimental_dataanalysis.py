import h5py
import matplotlib.pyplot as plt
import numpy as np
from h5py import File

path: str = "../resources/h5/test/ultrasound1.h5"
mode: str = "r"

h5file: File = h5py.File(path, mode)

dataset: dict = h5file['DCV2']
print(list(dataset.keys()))

# Fetches amplitude and time of flight (128x128)
amp: np.float64 = dataset['calibration/amp']
tof: np.float64 = dataset['calibration/tof']

# Gets the date of the measurement
date: str = (dataset['date'])[0]

# Gets the HDF5 file version
file_version: str = (dataset['file_version'])[0]

# Gets frame information
frames: dict = dataset['frames/1']
print(list(frames.items()))

state: dict = frames['state']
print(list(state.items()))

plt.imshow(amp, "gray")
plt.show()

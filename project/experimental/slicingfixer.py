# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 15:16:22 2020

@author: Stig
"""

import matplotlib.pyplot as plt
from scipy.ndimage import median_filter
from scipy.signal import argrelmin
from scipy.signal import argrelmax
import scipy as sp
import numpy as np
import pickle
import cv2
import sys
import os

import sources.loader as loader
import sources.ustool as ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

# Retrieve all the files
filepaths = []
for root, directories, files in os.walk("./resources/h5"):
    for file in files:
        if file.endswith(".h5"):
            filepaths.append(os.path.normpath(os.path.join(root, file)))

for i, path in enumerate(filepaths):
    print("[", i, "]: ", path)
path = filepaths[int(input("Choose file: "))]


# -----------------------------------------------------------------------------


# Retrieve the scan and smooth it
originalScan, startGate, endGate = loader.load(path)
smoothScan = median_filter(originalScan, (3, 3, 3), mode="constant", cval=0.0)

# Retrieve the response function and its peaks /w widths
responseFunction = ustool.getResponseFunction(smoothScan, 0.01)
peaks = sp.signal.find_peaks(responseFunction)[0]
widths = sp.signal.peak_widths(responseFunction, peaks)[0]

# Retrieve all the slicing information around each peak
indices = []
for i, peak in enumerate(peaks):
    start = int(peak - widths[i])
    end = int(peak + widths[i])
    if start < 0: start = 0
    if end > responseFunction.size: end = responseFunction.size
    indices.append([(start, end), peak, responseFunction[peak]])

# Sorted so that largest peaks are first (Most likely the interesting slices)
indices = sorted(indices, key = lambda x : x[2], reverse = True)

# Retrieve the slices
scanSlices = []
for (start, end), peak, height in indices:
    scanSlices.append(smoothScan[start:end])


# -----------------------------------------------------------------------------


# Display main information
plt.figure(figsize=(12,6)); plt.plot(responseFunction)
plt.xticks(range(0, responseFunction.size, 10), fontsize=6)
figure, axes = plt.subplots(1, 2, figsize=(8, 8))
axes[0].imshow(ustool.getImageByMaximum(smoothScan), "gray")
axes[1].imshow(ustool.getImageByMaximum(smoothScan[startGate:endGate]), "gray")
plt.show(block=False)


# -----------------------------------------------------------------------------


# Display slices and their thresholds
for i, scanSlice in enumerate(scanSlices):
    sliceImage = ustool.getImageByMaximum(scanSlice)
    thresholded = cv2.threshold(sliceImage, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, (3, 3))
    thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, (3, 3))
    figure, axes = plt.subplots(1, 2, figsize=(4, 4))
    axes[0].imshow(sliceImage, "gray")
    axes[1].imshow(thresholded, "gray")
    axes[1].text(172, 64, "SLICE ("+str(i)+")\n["+str(indices[i][0][0])+"-"+str(indices[i][0][1])+"]", horizontalalignment='center', verticalalignment='center')
    plt.setp(axes, xticks=[], yticks=[])
    plt.show(block=False)


# -----------------------------------------------------------------------------


# Choose the slices
chosenIndexes = input("Enter the slicenumbers to choose (separated by space),\nor a single '-1' for no damage: ").strip().split()
chosenSlices = [scanSlices[int(number)] for number in chosenIndexes]

if int(chosenIndexes[0]) != -1:
    
    # Create the merged mask of the chosen slices
    mask = np.zeros((128,128), np.uint8)
    for chosenSlice in chosenSlices:
        sliceImage = ustool.getImageByMaximum(chosenSlice)
        sliceMask = cv2.threshold(sliceImage, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        mask[sliceMask==255] = 255

    # Retrieve the original images, and apply the mask to them
    originalImage = cv2.cvtColor(ustool.getImageByMaximum(smoothScan), cv2.COLOR_GRAY2RGB)
    gatedImage = cv2.cvtColor(ustool.getImageByMaximum(smoothScan[startGate:endGate]), cv2.COLOR_GRAY2RGB)
    
    figure, axes = plt.subplots(1, 3, figsize=(8, 8))
    axes[0].imshow(originalImage, "gray")
    axes[1].imshow(gatedImage, "gray")
    axes[2].imshow(mask, "gray")
    plt.show(block=False)
    
    yellow = np.zeros((128,128,3), np.uint8); yellow[:, :] = (255, 255, 0)
    yellowMask = cv2.bitwise_and(yellow, yellow, mask=mask)
    cv2.addWeighted(yellowMask, 0.25, originalImage, 1, 0, originalImage)
    cv2.addWeighted(yellowMask, 0.25, gatedImage, 1, 0, gatedImage)
    
    figure, axes = plt.subplots(1, 2, figsize=(8, 8))
    axes[0].imshow(originalImage, "gray")
    axes[1].imshow(gatedImage, "gray")
    plt.show(block=False)
    
    
else:
    
    # TODO - Non damage apply
    sys.exit(0)

# -*- coding: utf-8 -*-
"""
Performs binary classification on A-scans using a combined neural network + contour analysis method
"""
import sys
import matplotlib.pyplot as plt
from scipy.signal import resample
from scipy.signal import argrelmin
from scipy.signal import argrelmax
from scipy.ndimage import median_filter
import tensorflow as tf
import numpy as np
import time
import pickle
import cv2
import os

import sources.loader as loader
import sources.ustool as ustool

np.set_printoptions(suppress=True)
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(linewidth=1000)

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

# Retrieve all the h5 files
filepaths = []
for root, directories, files in os.walk("./resources/h5"):
    for file in files:
        if file.endswith(".h5"):
            filepaths.append(os.path.normpath(os.path.join(root, file)))

avgWhole = []
avgLoading = []
avgMedianFiltering = []
avgDownsample = []
avgPrediction = []
avgResponseFunction = []
avgSlicing = []
avgContourProcessing = []

results  = []
for i,path in enumerate(filepaths):
    if i == 10: break
    print(str(np.floor((i/len(filepaths))*100))+"%")
    
    
    wholeStartClock = time.clock()
    # Load the C-scan sample to predict
    originalScan, start, end = loader.load(path)
    
    # Load the model to predict with
    model = tf.keras.models.load_model("./resources/model/ascan_nn_31032020.h5")
    endClock = time.clock()
    deltaLoading = endClock-wholeStartClock
    
    
    startClock = time.clock()
    # Preprocess the data by median filtering
    smoothScan = median_filter(originalScan, (3, 3, 3), mode="constant", cval=0.0)
    endClock = time.clock()
    deltaMedianFiltering = endClock-startClock
    
    
    # Extract the original image, and make it multichannel for later use
    originalImage = ustool.getImageByMaximum(smoothScan[start:end])
    originalImage = cv2.cvtColor(originalImage, cv2.COLOR_GRAY2RGB)
    
    
    startClock = time.clock()
    # Preprocess by downsampling to the standard length
    downsampled = resample(smoothScan, 128, axis=0)
    endClock = time.clock()
    deltaDownsample = endClock-startClock
    
    
    startClock = time.clock()
    downsampled = np.reshape(downsampled, (downsampled.shape[0], downsampled.shape[1] * downsampled.shape[2]))
    downsampled = np.swapaxes(downsampled, 0, 1)
    # Predict where the damage is by the model
    predictions = model.predict(downsampled).reshape(128, 128)
    endClock = time.clock()
    deltaPrediction = endClock-startClock
    
    
    # Threshold the predictions, IE. we trust
    # anything that the network is more than
    # 50% sure about when predicting
    threshold = 0.50
    predictions[predictions >= threshold] = 1
    predictions[predictions < threshold] = 0
    predictions = (predictions * 255).astype(np.uint8)
    
    # Show the heatmap image of the predictions
    color = np.zeros(originalImage.shape, originalImage.dtype)
    color[:, :] = (255, 0, 0)
    colormask = cv2.bitwise_and(color, color, mask=predictions)
    
    
    startClock = time.clock()
    # Retrieve the different segments by response functions local minima
    responseFunction = ustool.getResponseFunction(originalScan)
    endClock = time.clock()
    deltaResponseFunction = endClock-startClock
    
    
    startClock = time.clock()
    # Retrieve indices to separate by
    localMinimas = argrelmin(responseFunction)[0].tolist()
    localMaximas = argrelmax(responseFunction)[0].tolist()
    indices = localMinimas + localMaximas + [start, end]
    
    smoothScan[:start] = 0
    smoothScan[end:] = 0
    slicedScans = ustool.getSlicedCScan(smoothScan, indices)
    endClock = time.clock()
    deltaSlicing = endClock-startClock
    
    
    startClock = time.clock()
    # Retrieve all the contours from the segments
    contourList = []
    for scanSlice, start, end in slicedScans:
        temporary = ustool.getImageByMaximum(scanSlice)
        thresholded = cv2.threshold(temporary, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, (3, 3))
        thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, (3, 3))
        
        contours = cv2.findContours(thresholded, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
        contourList.extend(contours)
    
    # Normalize the predictions
    predictions[predictions == 0] = 1E-6
    predictions = ustool.normalize(predictions)
    
    # Refine what contours to keep based on the predictions
    refinedContourList = []
    for index, contour in enumerate(contourList):
        indexation = np.zeros((128, 128), np.uint8)
        indexation = (cv2.drawContours(indexation, [contour], -1, 255, -1) == 255)
    
        vote = predictions[indexation].sum() / predictions[indexation].size
        if vote > 0.50 and cv2.contourArea(contour) > 128:
            refinedContourList.append(contour)
    
    # Show the colormask
    temporary1 = originalImage.copy()
    cv2.addWeighted(colormask, 0.25, temporary1, 1, 0, temporary1)
    
    # Show the final result
    temporary2 = originalImage.copy()
    cv2.drawContours(temporary2, refinedContourList, -1, (255, 255, 0), 1)
    
    # Create union mask
    unionMask = np.zeros((128, 128), np.uint8)
    cv2.drawContours(unionMask, refinedContourList, -1, 255, -1)
    
    # Create the union
    temporary3 = originalImage.copy()
    contours = cv2.findContours(unionMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    cv2.drawContours(temporary3, contours, -1, (255, 255, 0), 1)
    wholeEndClock = time.clock()
    deltaContourProcessing = wholeEndClock-startClock
    
    
    deltaWhole = wholeEndClock-wholeStartClock
    
    avgWhole.append(deltaWhole)
    avgLoading.append(deltaLoading)
    avgMedianFiltering.append(deltaMedianFiltering)
    avgDownsample.append(deltaDownsample)
    avgPrediction.append(deltaPrediction)
    avgResponseFunction.append(deltaResponseFunction)
    avgSlicing.append(deltaSlicing)
    avgContourProcessing.append(deltaContourProcessing)
    
#    results.append([originalImage.copy(),temporary1,temporary2,temporary3])
#
#path = input("Give a path to a folder (From working directory): ")
#filename = input("Give a filename (Without extension): ")
#with open(os.path.normpath(path + "/" + filename + '.dta'), 'wb') as output:
#    pickle.dump(results, output, pickle.HIGHEST_PROTOCOL)
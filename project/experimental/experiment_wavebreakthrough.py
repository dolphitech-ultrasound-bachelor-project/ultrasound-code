# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 23:55:56 2020

@author: Stig
"""
import matplotlib.pyplot as plt
import numpy as np
from sources import ustool
from sources import utility
from sources import loader

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#
resourcename = "ultrasound1"
data = loader.load("../resources/" + resourcename + ".h5")

cumulator = np.zeros(data.shape[0])
for y in range(128):
    for x in range(128):
        index = np.argmax(np.cumsum(data[:, y, x]))
        # index = np.argmax(data[:,y,x])
        cumulator[index] += 1

plt.figure()
plt.plot(cumulator)
plt.figure()
plt.imshow(ustool.returnImageByLayer(resourcename, 50), plt.cm.gray)

# SUMMING AFTER AND BEFORE THE MASSIVE REFLECTION (50<->150)
# MAKES THE AREA OF CONTEXT MORE HIGHTLIGHTED :O
damage = np.zeros((128, 128))
for i in range(50, 150):
    damage += np.abs(data[i])

damage = (utility.normalize(damage) * 255).astype(np.uint8)
utility.display(damage, histogram=True)
utility.threshold(damage, 80, displaysetting=True)

import pickle

from general import loader
import matplotlib.pyplot as plt

from dataset.trainingDtaCreator import EntryMaxamp
from general.ustool import getImageByMaximum

data, startGate, endGate = loader.load("../resources/h5/airbus2_2.h5")
plt.imshow(getImageByMaximum(data[startGate:endGate]), "gray")
plt.title("airbus2_2 from h5 file")
plt.show()

with open('../resources/dataset/Image dataset/dataset_test_image.dta', 'rb') as dtaFile:
    dataset = pickle.load(dtaFile)

image: EntryMaxamp = dataset[9]
plt.imshow(image.maxamp, "gray")
plt.title(image.name + "from dataset")
plt.show()


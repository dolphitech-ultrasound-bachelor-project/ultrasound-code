# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np

from sources import ustool
from sources import utility
from sources import loader

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#


x = 50; y = 90
resourcename = "ultrasound2"


wave = ustool.returnwavebysensor(resourcename,x,y)
abswave = np.abs(wave)


# The domain of X values we are working with
domain = abswave.size


# Generates a matrix of values that represents a subtraction of
# -1, -2, ..., -N of each row as it goes downwards. Each row
# in this matrix represents a gaussian distribution
# on every value of the X-axis from 0-abswave.size.
# When this is multiplied with the absolute wave values,
# and then summed, we in fact get a smoothed distribution
# of our original wavepeaks

# Example of original matrix before gaussian:
# +0 +1 +2 +3 +4 +5
# -1 +0 +1 +2 +3 +4
# -2 -1 +0 +1 +2 +3
# -3 -2 -1 +0 +1 +2
# ... and so on ...
matrix = np.arange(domain)[None,:] - np.arange(domain)[:,None]


# We assume a width of distribution that is 5% of the X-axis size
sigma = 0.05*abswave.size


# The actual calculation, which creates a 
# gaussian distribution of 5% width on each point
c1 = 1 / (np.sqrt(2*np.pi)*sigma)
c2 = -0.5*(matrix/sigma)**2
gaussians = c1*np.exp(c2)


# Applies the absolute wave y-values to each gaussian distribution
multiplicator = np.zeros(domain)[None,:] + abswave[:,None]
gaussians = gaussians * multiplicator


# Sum all the peaks into eachother, thus creating a 'smoothed'
# version of our original absolute wave function
gaussian = gaussians.sum(axis=0)
plt.plot(gaussian)

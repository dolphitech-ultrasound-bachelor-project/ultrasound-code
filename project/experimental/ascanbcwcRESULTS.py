# -*- coding: utf-8 -*-
"""
Performs binary classification on A-scans using a combined neural network + contour analysis method
"""
import sys
import matplotlib.pyplot as plt
from scipy.ndimage import median_filter
from scipy.signal import find_peaks
from scipy.signal import peak_widths
from scipy.signal import argrelmin
from scipy.signal import resample
import tensorflow as tf
import numpy as np
import pickle
import time
import cv2
import os

import sources.loader as loader
import sources.ustool as ustool

np.set_printoptions(suppress=True)
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(linewidth=1000)


def __clear():
    """
    Void function to 'clear' the screen
    """

    print("\n" * 100)
    os.system('clear')


def __loadSample():
    """
    Function to return a sample from user choice
    """

    # Retrieve all the h5 files
    filepaths = []
    for root, directories, files in os.walk("./resources/h5"):
        for file in files:
            if file.endswith(".h5"):
                filepaths.append(os.path.normpath(os.path.join(root, file)))

    print("Files found in the '//project/resources/h5' folder...")
    for i, path in enumerate(filepaths):
        print("[", i, "]: ", path)

    # Load the C-scan sample to predict
    scanpath = filepaths[int(input("Enter number to load: "))]
    
    filename = os.path.basename(scanpath)
    filename = os.path.splitext(filename)[0]
    path = None
    guess1 = "resources/dataset/ascan/trainingdata/" + filename + ".dta"
    guess2 = "resources/dataset/ascan/testdata/" + filename + ".dta"
    
    try:
        file = open(guess1)
        path = guess1
    except IOError:
        path = guess2
    
    dataset = []
    with open(path, 'rb') as file:
        while True:
            try:
                dataset = pickle.load(file)
            except EOFError:
                break
        
    originalMask = np.zeros((128, 128), dtype=np.uint8)
    for resource, (start, end), (y, x), aScan, damaged in dataset:
        if damaged:
            originalMask[y, x] = 255
    
    originalScan, startGate, endGate = loader.load(scanpath)
    return originalScan, startGate, endGate, originalMask


def __retrieveSlices(smoothScan, method="localminima", percentage=0.01, constant=10):
    """
    Function to return a slicing of the scan by a given method,
    which is either 'peakwidth', 'constant' or 'localminima'
    """
    
    # Extract at a constant rate
    if method.lower() == "constant":
        indices = list(range(0, smoothScan.shape[0], constant))
        return ustool.getSlicedCScan(smoothScan, indices)
    
    # Extract around peaks, by their corresponding widths
    elif method.lower() == "peakwidth":
        responseFunction = ustool.getResponseFunction(smoothScan, percentage)
        peaks = find_peaks(responseFunction)[0]
        widths = peak_widths(responseFunction, peaks)[0]
    
        indices = []
        for i, peak in enumerate(peaks):
            start = int(peak - widths[i])
            end = int(peak + widths[i])
            if start < 0:
                start = 0
            if end > responseFunction.size:
                end = responseFunction.size
            indices.append([(start, end), peak, responseFunction[peak]])
    
        scanSlices = []
        for (start, end), peak, height in indices:
            scanSlices.append([smoothScan[start:end], (start, end)])
    
        return scanSlices

    # Extract by local minima separation
    else:
        responseFunction = ustool.getResponseFunction(smoothScan, percentage)
        localMinima = argrelmin(responseFunction)[0].tolist()
        return ustool.getSlicedCScan(smoothScan, localMinima)


def __retrieveContours(scanSlices):
    """
    Function to retrieve contours from a list of scan slices. Each scan slice
    will have its maximum amplitude image extracted, and thresholded
    """

    # List to collect the contours
    contours = []

    # Iterate over every slice, and add the found contours
    for scanSlice, (start, end) in scanSlices:
        maximumAmplitudeImage = ustool.getImageByMaximum(scanSlice)
        thresholded = cv2.threshold(maximumAmplitudeImage, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, (3, 3))
        thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, (3, 3))
        contours.extend(cv2.findContours(thresholded, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2])

    return contours


def __refineContoursWithPredictions(contours, refinedPredictions, voteThreshold=0.50, areaThreshold=128):
    """
    Function to refine a list of contours, based on the given predictions
    """

    # Normalize the predictions
    normalizedPredictions = refinedPredictions.copy()
    normalizedPredictions[normalizedPredictions == 0] = 1E-6
    normalizedPredictions = ustool.normalize(normalizedPredictions)

    refinedContours = []

    # Refine the contours based on the predictions
    for contour in contours:
        indexation = np.zeros((128, 128), np.uint8)
        indexation = (cv2.drawContours(indexation, [contour], -1, 255, -1) == 255)
        vote = normalizedPredictions[indexation].sum() / normalizedPredictions[indexation].size
        if vote >= voteThreshold and cv2.contourArea(contour) >= areaThreshold:
            refinedContours.append(contour)

    return refinedContours


# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

# Load a sample and preprocess it with median filtering
originalScan, startGate, endGate, originalMask = __loadSample()
smoothScan = median_filter(originalScan, (3, 3, 3), mode="constant", cval=0.0)


# Preprocess by downsampling to the standard length
downsampled = resample(smoothScan, 128, axis=0)
downsampled = np.reshape(downsampled, (downsampled.shape[0], downsampled.shape[1] * downsampled.shape[2]))
downsampled = np.swapaxes(downsampled, 0, 1)


# Load the model prediction model, and retrieve predictions
model = tf.keras.models.load_model("resources/model/downsampledAScanFCNNModel")
predictions = model.predict(downsampled).reshape(128, 128)


# Threshold the predictions, IE. we trust
# anything that the network is more than X% sure of
threshold = 0.50
thresholdedPredictions = predictions.copy()
thresholdedPredictions[predictions >= threshold] = 1
thresholdedPredictions[predictions < threshold] = 0


# Retrieve all the slice segments, a response function of 1%
# which is separated by local minima is chosen
scanSlices = __retrieveSlices(smoothScan, "localminima", percentage=0.01)


# Retrieve all the found contours from the list of scan slices,
# and refine them by looking at the predictions
contours = __retrieveContours(scanSlices)
contours = __refineContoursWithPredictions(contours, thresholdedPredictions, 0.75, 64)


# Prepare information for display
maximumAmplitudeImage = ustool.getImageByMaximum(smoothScan[startGate:endGate])
maximumAmplitudeImage = cv2.cvtColor(maximumAmplitudeImage, cv2.COLOR_GRAY2RGB)

unionMask = np.zeros((128, 128), np.uint8)
cv2.drawContours(unionMask, contours, -1, 255, -1)
outerUnionContours = cv2.findContours(unionMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

contourRefinedImage = maximumAmplitudeImage.copy()
cv2.drawContours(contourRefinedImage, contours, -1, (255, 255, 0), 1)

contourUnionImage = maximumAmplitudeImage.copy()
cv2.drawContours(contourUnionImage, outerUnionContours, -1, (255, 255, 0), 1)




predictionMask = (thresholdedPredictions * 255).astype(np.uint8)

unionMask = np.zeros((128, 128), np.uint8)
cv2.drawContours(unionMask, outerUnionContours, -1, 255, -1)

gpmask = np.zeros((128,128,3), np.uint8)
gpmask[:,:,1] = originalMask
gpmask[:,:,2] = predictionMask

gcmask = np.zeros((128,128,3), np.uint8)
gcmask[:,:,1] = originalMask
gcmask[:,:,2] = unionMask


plt.figure()
plt.title("Refined Predictions")
plt.imshow((thresholdedPredictions * 255).astype(np.uint8), "gray")

plt.figure()
plt.title("Ground Truth")
plt.imshow(originalMask, "gray")

# Display the final results
figure, axes = plt.subplots(1,6,figsize=(24,24))
axes[0].imshow(maximumAmplitudeImage)
axes[1].imshow((ustool.normalize(predictions) * 255).astype(np.uint8), "gray")
axes[2].imshow(contourRefinedImage)
axes[3].imshow(contourUnionImage, "gray")
axes[4].imshow(gpmask)
axes[5].imshow(gcmask)
plt.setp(axes, xticks=[], yticks=[])
plt.show(block=False)

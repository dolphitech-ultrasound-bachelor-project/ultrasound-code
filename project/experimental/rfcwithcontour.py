# -*- coding: utf-8 -*-
"""
Performs binary classification on A-scans using a combined neural network + contour analysis method
"""
import sys
import matplotlib.pyplot as plt
from scipy.signal import resample
from scipy.signal import argrelmin
from scipy.signal import argrelmax
from scipy.ndimage import median_filter
import sklearn
from sklearn.ensemble import RandomForestClassifier
import tensorflow as tf
import numpy as np
import pickle
import cv2
import os

import sources.loader as loader
import sources.ustool as ustool

np.set_printoptions(suppress=True)
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(linewidth=1000)


def __clear():
    """
    Void function to clear the screen
    """

    print("\n" * 100)
    os.system('clear')


# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

# Retrieve all the h5 files
filepaths = []
for root, directories, files in os.walk("./resources/h5"):
    for file in files:
        if file.endswith(".h5"):
            filepaths.append(os.path.normpath(os.path.join(root, file)))

print("Files found in the '//project/resources/h5' folder...")
for i, path in enumerate(filepaths):
    print("[", i, "]: ", path)

# Load the C-scan sample to predict
originalScan, start, end = loader.load(filepaths[int(input("Enter number to load: "))])

__clear()

model = None
# Retrieve model
with open('./resources/model/rfc.model', 'rb') as file:
        model = pickle.load(file)

__clear()

# Preprocess the data by median filtering
smoothScan = median_filter(originalScan, (3, 3, 3), mode="constant", cval=0.0)

# Extract the original image, and make it multichannel for later use
originalImage = ustool.getImageByMaximum(smoothScan[start:end])
originalImage = cv2.cvtColor(originalImage, cv2.COLOR_GRAY2RGB)

# Preprocess by downsampling to the standard length
downsampled = resample(smoothScan, 128, axis=0)
downsampled = np.reshape(downsampled, (downsampled.shape[0], downsampled.shape[1] * downsampled.shape[2]))
downsampled = np.swapaxes(downsampled, 0, 1)

# Predict where the damage is by the model
predictions = model.predict(downsampled).reshape(128, 128)

plt.figure()
plt.title("model Predictions")
plt.imshow(predictions, "gray")

# Threshold the predictions, IE. we trust
# anything that the network is more than
# 50% sure about when predicting
threshold = 0.50
predictions[predictions >= threshold] = 1
predictions[predictions < threshold] = 0
predictions = (predictions * 255).astype(np.uint8)

plt.figure()
plt.title("Refined Predictions")
plt.imshow(predictions, "gray")

# Show the heatmap image of the predictions
temporary = originalImage.copy()
color = np.zeros(originalImage.shape, originalImage.dtype)
color[:, :] = (255, 0, 0)
colormask = cv2.bitwise_and(color, color, mask=predictions)
cv2.addWeighted(colormask, 0.25, temporary, 1, 0, temporary)
plt.figure()
plt.imshow(temporary)

# Retrieve the different segments by response functions local minima
responseFunction = ustool.getResponseFunction(originalScan)

# Retrieve indices to separate by
localMinimas = argrelmin(responseFunction)[0].tolist()
localMaximas = argrelmax(responseFunction)[0].tolist()
indices = localMinimas + localMaximas + [start, end]

smoothScan[:start] = 0
smoothScan[end:] = 0
slicedScans = ustool.getSlicedCScan(smoothScan, indices)

# Retrieve all the contours from the segments
contourList = []
for scanSlice, start, end in slicedScans:
    temporary = ustool.getImageByMaximum(scanSlice)
    thresholded = cv2.threshold(temporary, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, (3, 3))
    thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, (3, 3))

    plt.figure()
    plt.title("Segment (" + str(start) + "-" + str(end) + ")")
    plt.imshow(thresholded, "gray")

    contours = cv2.findContours(thresholded, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    contourList.extend(contours)

# Normalize the predictions
predictions[predictions == 0] = 1E-6
predictions = ustool.normalize(predictions)

# Refine what contours to keep based on the predictions
refinedContourList = []
for index, contour in enumerate(contourList):
    indexation = np.zeros((128, 128), np.uint8)
    indexation = (cv2.drawContours(indexation, [contour], -1, 255, -1) == 255)

    vote = predictions[indexation].sum() / predictions[indexation].size
    if vote > 0.50 and cv2.contourArea(contour) > 128:
        refinedContourList.append(contour)

# Show the colormask
temporary = originalImage.copy()
cv2.addWeighted(colormask, 0.25, temporary, 1, 0, temporary)
plt.figure()
plt.title("Heatmap of predictions")
plt.imshow(temporary)

# Show the final result
temporary = originalImage.copy()
cv2.drawContours(temporary, refinedContourList, -1, (255, 255, 0), 1)
plt.figure()
plt.title("Finished Prediction by Contour Refinement")
plt.imshow(temporary)

# Create union mask
unionMask = np.zeros((128, 128), np.uint8)
cv2.drawContours(unionMask, refinedContourList, -1, 255, -1)
plt.figure()
plt.title("Union of Contours mask")
plt.imshow(unionMask, "gray")

# Create the union
temporary = originalImage.copy()
contours = cv2.findContours(unionMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
cv2.drawContours(temporary, contours, -1, (255, 255, 0), 1)
plt.figure()
plt.title("Union of Contours result")
plt.imshow(temporary, "gray")

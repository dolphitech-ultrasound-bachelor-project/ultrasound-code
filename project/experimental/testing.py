# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 14:00:00 2020

@author: StigA
"""

import matplotlib.pyplot as plt
from scipy.signal import argrelmin
import numpy as np
import h5py
import cv2

from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

threshold = 165

cScan, startGate, endGate = loader.load("../resources/airbus1_4.h5")
image = ustool.getImageByMaximum(cScan[startGate:endGate])
histogram = cv2.calcHist([image], [0], None, [256], [0, 256])
thresholded = ustool.getThresholdedImage(image,threshold)

fig, axes = plt.subplots(1,3,figsize=(14,14))
axes[0].imshow(image,"gray")
axes[1].plot(histogram)
axes[1].set_aspect(np.diff(axes[1].get_xlim())[0] / np.diff(axes[1].get_ylim())[0])
axes[1].axvline(x=threshold,color="red")
axes[2].imshow(thresholded,"gray")
plt.gca().set_aspect('equal', adjustable='box')
plt.show()

# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 17:43:26 2020

@author: StigA
"""

import pickle
import matplotlib.pyplot as plt
import numpy as np
import cv2
import os

from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#
print ("\n" * 100)
filepaths = [
        "../resources/ultrasound1.h5",
        "../resources/ultrasound2.h5",
        "../resources/airbus1_1.h5",
        "../resources/airbus1_4.h5",
        "../resources/airbus2_1.h5",
        "../resources/airbus2_2.h5",
        "../resources/airbus2_3.h5",
        "../resources/airbus2_5.h5",
        "../resources/123_4.h5",
        "../resources/123_5.h5",
        "../resources/123_6.h5",
        "../resources/airbus1_5.h5",
        "../resources/airbus1_6.h5",
        "../resources/airbus2_4.h5",
        "../resources/boeing1_1.h5",
        "../resources/mono1_1.h5"
]




damagedIndexes = None;
notDamageIndexes = None

for i,p in enumerate(filepaths): print('\n',"["+str(i)+"] "+p)
dataindexation = int(input("\n\nVelg data("+str(0)+"-"+str(len(filepaths))+"), -1 = AVSLUTT: ")); print ("\n" * 100)
while True:
    if(dataindexation == -1): break
    filename = os.path.basename(filepaths[dataindexation])

    while True:
        cScan, startGate, endGate = loader.load(filepaths[dataindexation])
        original = ustool.getImageByMaximum(cScan)
        original = cv2.cvtColor(original,cv2.COLOR_GRAY2RGB)
        gated = ustool.getImageByMaximum(cScan[startGate:endGate])
        gated = cv2.cvtColor(gated,cv2.COLOR_GRAY2RGB)


        responseFunction, localMinimas = ustool.getResponseFunction(cScan)
        plt.figure(); plt.plot(responseFunction)
        plt.xticks(list(plt.xticks()[0]) + localMinimas);
        plt.xticks(list(plt.xticks()[0]) + [startGate,endGate])
        for l in localMinimas:plt.axvline(x=l,color='green')
        plt.axvline(x=startGate,color='yellow'); plt.axvline(x=endGate,color='yellow');
        plt.show(block=False)

        slices = [int(x) for x in input("Delepunkter: ").split()]; print ("\n" * 100)
        dataSlices = ustool.getSlicedCScan(cScan,slices)
        plt.figure(); plt.plot(responseFunction)
        for s in slices:plt.axvline(x=s,color='red')
        plt.axvline(x=startGate,color='yellow'); plt.axvline(x=endGate,color='yellow');
        plt.xticks(list(plt.xticks()[0]) + slices);
        plt.xticks(list(plt.xticks()[0]) + [startGate,endGate]); plt.show(block=False)
        plt.show(block=False)

        for i,(scan,start,end) in enumerate(dataSlices):
                print("\n SLICE NUMBER: "+str(i))
                image = ustool.getImageByMaximum(scan)
                image = cv2.bilateralFilter(image,3,50,50)
                __,thresholded = cv2.threshold(image,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
                thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, (3,3), 10)
                thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, (3,3))
                thresholded = cv2.GaussianBlur(thresholded,(3,3),3)
                thresholded[thresholded>0] = 255
                fig,ax = plt.subplots(1,3)
                ax[0].imshow(gated,"gray")
                ax[1].imshow(image,"gray")
                ax[2].imshow(thresholded,"gray")
                plt.show(block=False)

        choice = str(input("Videre(y/N)?: "));
        while True:
            if choice.capitalize() == "Y": break
            print ("\n" * 100)

            plt.figure(); plt.plot(responseFunction)
            for s in slices:plt.axvline(x=s,color='red')
            plt.axvline(x=startGate,color='yellow'); plt.axvline(x=endGate,color='yellow');
            plt.xticks(list(plt.xticks()[0]) + slices);
            plt.xticks(list(plt.xticks()[0]) + [startGate,endGate]); plt.show(block=False)
            slices = [int(x) for x in input("Delepunkter: ").split()]; print ("\n" * 100)
            dataSlices = ustool.getSlicedCScan(cScan,slices)

            for i,(scan,start,end) in enumerate(dataSlices):
                print("\n SLICE NUMBER: "+str(i))
                image = ustool.getImageByMaximum(scan)
                image = cv2.bilateralFilter(image,3,50,50)
                __,thresholded = cv2.threshold(image,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
                thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, (3,3), 10)
                thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, (3,3))
                thresholded = cv2.GaussianBlur(thresholded,(3,3),3)
                thresholded[thresholded>0] = 255
                fig,ax = plt.subplots(1,3)
                ax[0].imshow(gated,"gray")
                ax[1].imshow(image,"gray")
                ax[2].imshow(thresholded,"gray")
                plt.show(block=False)

            choice = str(input("Videre(y/N)?: "));



        mask = np.zeros((128,128),dtype=np.uint8)
        thresholded = np.zeros((128,128),dtype=np.uint8)
        choosen = int(input("\n\nVelg slice(-1 = ingen): "))
        if(choosen != -1):
            scan,start,end = dataSlices[choosen]
            image = ustool.getImageByMaximum(scan)
            image = cv2.bilateralFilter(image,3,50,50)
            __,thresholded = cv2.threshold(image,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
            thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, (3,3), 10)
            thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, (3,3))
            thresholded = cv2.GaussianBlur(thresholded,(3,3),3)
        mask[thresholded>0] = 255

        showcase1 = gated.copy()
        showcase2 = original.copy()
        yel = np.zeros(original.shape, original.dtype); yel[:,:] = (255,255,0)
        yelmask = cv2.bitwise_and(yel, yel, mask=mask)
        cv2.addWeighted(yelmask, 0.25, showcase1, 1, 0, showcase1)
        cv2.addWeighted(yelmask, 0.25, showcase2, 1, 0, showcase2)
        plt.figure(); plt.imshow(showcase1); plt.show(block=False)
        plt.figure(); plt.imshow(showcase2); plt.show(block=False)

        damagedIndexes = np.where(mask>0)
        damagedIndexes = list(zip(damagedIndexes[0],damagedIndexes[1]))

        notDamagedIndexes = np.where(mask==0)
        notDamagedIndexes = list(zip(notDamagedIndexes[0],notDamagedIndexes[1]))


        choice = str(input("\nOK(y/N)?: ")); print ("\n" * 100)
        if choice.capitalize() == "Y": break




    dataset = []

    for y,x in damagedIndexes:
        aScan = cScan[:,y,x]
        dataset.append([filename,(startGate,endGate),(y,x),aScan,True])

    for y,x in notDamagedIndexes:
        aScan = cScan[:,y,x]
        dataset.append([filename,(startGate,endGate),(y,x),aScan,False])

    with open('../resources/ascan_dataset'+str(dataindexation)+'.dta', 'wb') as output:
            pickle.dump(dataset, output, pickle.HIGHEST_PROTOCOL)


    for i,p in enumerate(filepaths): print('\n',"["+str(i)+"] "+p)
    dataindexation = int(input("\n\nVelg data("+str(0)+"-"+str(len(filepaths))+"), -1 = AVSLUTT: ")); print ("\n" * 100)











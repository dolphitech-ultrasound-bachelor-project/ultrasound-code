# -*- coding: utf-8 -*-
"""
Created on Sat Feb 22 14:34:35 2020

@author: StigA
"""

import pickle
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.ticker as plticker
import scipy.signal as sig
from scipy.interpolate import interp1d
from sklearn import preprocessing
import skimage as sk
import scipy as scp
import numpy as np
import h5py
import cv2

import dataset_creator as dc
from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

dataset = []

for i in range(0,16):
    with open('../resources/ascan_dataset'+str(i)+'.dta', 'rb') as file:
        data = []
        while True:
            try:
                data = pickle.load(file)
            except EOFError:
                break

        mask = np.zeros((128,128),dtype=np.uint8)
        for resource,(start,end),(y,x),aScan,damaged in data:
            if(damaged): mask[y,x] = 255
        plt.figure(); plt.imshow(mask,"gray")

        dataset.extend(data)

with open('../resources/dataset/ascan_dataset.dta', 'wb') as output:
            pickle.dump(dataset, output, pickle.HIGHEST_PROTOCOL)
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 22:10:43 2020

@author: StigA
"""

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from scipy.signal import argrelmin
from skimage import data
import numpy as np
import h5py
import cv2

from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#


__TOTALDEGREES = 360
__BINDELTA = 45
__BINSIZE = int(__TOTALDEGREES/__BINDELTA) + 1

__IMAGESIZE = 128
__BLOCKSIZE = 4
__NUMBEROFBLOCKS = int(__IMAGESIZE/__BLOCKSIZE)


cScan, startGate, endGate = loader.load("../resources/ultrasound1.h5")
cScan = cScan[startGate:endGate]

original = ustool.normalize(ustool.getImageByMaximum(cScan))


gy, gx = np.gradient(original)
gx[gx==0] = 1E-09
mg = np.sqrt(gx**2 + gy**2)
th = np.round(np.rad2deg(np.arctan(gy/gx))).astype(np.int)
plt.figure(figsize=(10,10)); plt.imshow(mg,"gray")

hogs = np.zeros((__NUMBEROFBLOCKS,__NUMBEROFBLOCKS,__BINSIZE))

indexer = (
        __IMAGESIZE*np.arange(__BLOCKSIZE)[:,None,None,None]
        + np.arange(__BLOCKSIZE)[None,:,None,None]
        +__BLOCKSIZE*__IMAGESIZE*np.arange(__NUMBEROFBLOCKS)[None,None,:,None]
        +__BLOCKSIZE*np.arange(__NUMBEROFBLOCKS)[None,None,None,:]
)

indexer = np.transpose(indexer,axes=(2,3,0,1))

th_convolutions = th.flatten()[indexer.flatten()].reshape(indexer.shape)
mg_convolutions = mg.flatten()[indexer.flatten()].reshape(indexer.shape)

hog_indexes = (th_convolutions / __BINDELTA)
hog_indexes_ceiling = np.ceil(hog_indexes).astype(np.int)
hog_indexes_floor = np.floor(hog_indexes).astype(np.int)

floor_percentages = hog_indexes_ceiling - hog_indexes
ceiling_percentages = 1 - floor_percentages
ceiling_votes = ceiling_percentages*mg_convolutions
floor_votes = floor_percentages*mg_convolutions

for y in range(__NUMBEROFBLOCKS):
    for x in range(__NUMBEROFBLOCKS):
        np.add.at(hogs[y,x],hog_indexes_ceiling[y,x].flatten(),ceiling_votes[y,x].flatten())
        np.add.at(hogs[y,x],hog_indexes_floor[y,x].flatten(),floor_votes[y,x].flatten())

for y in range(__NUMBEROFBLOCKS):
    for x in range(__NUMBEROFBLOCKS):
        degree = (hogs[y,x].argmax())*__BINDELTA
        plt.arrow(x*__BLOCKSIZE+((x+1)*__BLOCKSIZE)/2, y*__BLOCKSIZE+((y+1)*__BLOCKSIZE)/2, np.cos(np.deg2rad(degree))*1, np.sin(np.deg2rad(degree))*1, head_width=1, head_length=1, fc='r', ec='r')

y = 0
x = 0
print(__BINSIZE)
print(th[0:__BLOCKSIZE,0:__BLOCKSIZE])
print(th_convolutions[y,x,0:__BLOCKSIZE,0:__BLOCKSIZE])
print(hogs[y,x])
print((hogs[y,x].argmax())*__BINDELTA)






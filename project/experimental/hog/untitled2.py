# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 18:13:29 2020

@author: StigA
"""

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from scipy.signal import argrelmin
import numpy as np
import h5py
import cv2

from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

__IMAGESIZE = 128

__TOTALDEGREES = 360
__BINSIZE = 18
__BINDELTA = int(__TOTALDEGREES/__BINSIZE)

__BLOCKSIZE = 8
__NUMBEROFBLOCKS = int(__IMAGESIZE/__BLOCKSIZE)




cScan, startGate, endGate = loader.load("../resources/ultrasound1.h5")
cScan = cScan[startGate:endGate]

original = ustool.normalize(ustool.getImageByMaximum(cScan))
plt.figure(); plt.imshow(original,"gray")

gy, gx = np.gradient(original)
gx[gx==0] = 1E-09
mg = np.sqrt(gx**2 + gy**2)
th = np.round(np.rad2deg(np.arctan(gy/gx))+180).astype(np.int)

#plt.figure(); plt.imshow(gx,"gray")
#plt.figure(); plt.imshow(gy,"gray")
#plt.figure(); plt.imshow(mg,"gray")

X,Y=np.meshgrid(np.arange(0,__IMAGESIZE),np.arange(0,__IMAGESIZE))
skip=(slice(None,None,4),slice(None,None,4))
#plt.figure(); plt.imshow(mg,"gray")
#plt.quiver(X[skip],Y[skip],(gx/mg)[skip],(gy/mg)[skip],color="red",units='width',scale=32,width=0.005)




histograms = np.zeros((__NUMBEROFBLOCKS,__NUMBEROFBLOCKS,__BINSIZE))
#print(histograms.shape)




#indexer = (                                                                 #MAKE A GIANT ARRAY THAT WILL INDEX ALL THE KERNELS
#    paddedImage.shape[1]*np.arange(inputImage.shape[0])[:,None,None,None]   #AXIS FOR Y DIRECTION KERNELS
#    + np.arange(inputImage.shape[1])[None,:,None,None]                      #AXIS FOR X DIRECTION KERNELS
#    + paddedImage.shape[1]*np.arange(self.size)[None,None,:,None]           #AXIS FOR Y DIRECTION PIXELS
#    + np.arange(self.size)[None,None,None,:]                                #AXIS FOR X DIRECTION PIXELS
#)




#x = 0
#y = 0
#indexer = (
#        __IMAGESIZE*np.arange(__BLOCKSIZE)[:,None]+x*__BLOCKSIZE
#        + np.arange(__BLOCKSIZE)[None,:]+y*__BLOCKSIZE*__IMAGESIZE
#)

#print(indexer)
#print(original.shape)
#plt.figure(); plt.imshow(original.flatten()[indexer.flatten()].reshape(__BLOCKSIZE,__BLOCKSIZE),"gray")
#print(original.flatten()[indexer.flatten()].reshape(__BLOCKSIZE,__BLOCKSIZE).shape)




indexer = (
        __IMAGESIZE*np.arange(__BLOCKSIZE)[:,None,None,None]
        + np.arange(__BLOCKSIZE)[None,:,None,None]
        +__IMAGESIZE*np.arange(__NUMBEROFBLOCKS)[None,None,:,None]
        +np.arange(__NUMBEROFBLOCKS)[None,None,None,:]
)

print(indexer[:,:,0,0])
print(indexer.shape)



















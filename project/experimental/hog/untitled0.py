# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 17:14:34 2020

@author: StigA
"""

import matplotlib.pyplot as plt
from scipy.signal import argrelmin
import numpy as np
import h5py
import cv2

from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

cScan, startGate, endGate = loader.load("../resources/airbus2_2.h5")
cScan = cScan[startGate:endGate]

original = ustool.getImageByMaximum(cScan)
plt.figure(); plt.imshow(original,"gray")

responseFunction, localMinimas = ustool.getResponseFunction(cScan)
plt.figure(); plt.plot(responseFunction)
for lm in localMinimas: plt.axvline(x=lm,color="red")

segments =[]
cScanSlices = ustool.getSlicedCScan(cScan,localMinimas)
for data,start,end in cScanSlices:
    image = ustool.getImageByMaximum(data)
    __,thresholded = cv2.threshold(image,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    plt.figure(); plt.title("SLICE: "+str(start)+" - "+str(end), fontsize=16); plt.imshow(image, "gray")
    plt.figure(); plt.imshow(thresholded, "gray")

    segments.append(thresholded)


original = (original[:,:,None]+ np.zeros(3)[None,None,:]).astype(np.uint8)

red = np.zeros(original.shape, original.dtype); red[:,:] = (255,0,0)
blu = np.zeros(original.shape, original.dtype); blu[:,:] = (0,255,0)
#gre = np.zeros(original.shape, original.dtype); gre[:,:] = (0,0,255)
#yel = np.zeros(original.shape, original.dtype); yel[:,:] = (255,255,0)

redmask = cv2.bitwise_and(red, red, mask=segments[0])
cv2.addWeighted(redmask, 0.25, original, 1, 0, original)

blumask = cv2.bitwise_and(blu, blu, mask=segments[1])
cv2.addWeighted(blumask, 0.25, original, 1, 0, original)

#gremask = cv2.bitwise_and(gre, gre, mask=segments[2])
#cv2.addWeighted(gremask, 0.25, original, 1, 0, original)

#yelmask = cv2.bitwise_and(yel, yel, mask=segments[3])
#cv2.addWeighted(yelmask, 0.25, original, 1, 0, original)

plt.figure(); plt.imshow(original)
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 14:03:53 2020

@author: StigA
"""

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.ticker as plticker
from scipy.signal import argrelmin
from skimage import data
import numpy as np
import h5py
import cv2

from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#


__TOTALDEGREES = 360
__BINDELTA = 45
__BINSIZE = int(__TOTALDEGREES/__BINDELTA) + 1

__IMAGESIZE = 128
__BLOCKSIZE = 6
__NUMBEROFBLOCKS = int(__IMAGESIZE/__BLOCKSIZE)


#cScan, startGate, endGate = loader.load("../resources/ultrasound1.h5")
#cScan = cScan[startGate:endGate]
#original = ustool.getImageByMaximum(cScan)
#original = ustool.normalize(cv2.bilateralFilter(original,3,50,50))
original = data.checkerboard()
original = cv2.resize(original,(128,128),interpolation=cv2.INTER_AREA)
original = ustool.normalize(original)


gy = np.gradient(original,axis=0)
gx = np.gradient(original,axis=1)
mg = np.sqrt(gx**2 + gy**2)
th = np.arctan2(gy,gx)


y = 2
x = 2
dx = 1
dy = 1
xs = slice(x*__BLOCKSIZE+dx,x*__BLOCKSIZE+dx+__BLOCKSIZE,1)
ys = slice(y*__BLOCKSIZE+dy,y*__BLOCKSIZE+dy+__BLOCKSIZE,1)

plt.figure(figsize=(10,10)); plt.imshow(mg,"gray")
fig,ax = plt.subplots(1,figsize=(10,10));
ax.imshow(original,"gray")
rect = patches.Rectangle((x*__BLOCKSIZE+dx,y*__BLOCKSIZE+dy),__BLOCKSIZE-1,__BLOCKSIZE-1,linewidth=1,edgecolor='y',facecolor='none')
ax.add_patch(rect)
plt.show()

dec = 3
print('\n',np.around(original[ys,xs],decimals=dec))
print('\n',np.around(gy[ys,xs],decimals=dec))
print('\n',np.around(gx[ys,xs],decimals=dec))
print('\n',np.around(mg[ys,xs],decimals=dec))
print('\n',np.around(np.rad2deg(th[ys,xs]),decimals=dec))

plt.figure();
ax = plt.gca()
ax.set_xticks(np.arange(-.5, 10, 1))
ax.set_yticks(np.arange(-.5, 10, 1))
ax.set_xticklabels(np.arange(1, 12, 1))
ax.set_yticklabels(np.arange(1, 12, 1))
plt.imshow(original[ys,xs],"gray")

startx = x*__BLOCKSIZE+dx
starty = y*__BLOCKSIZE+dy
for yy in range(__BLOCKSIZE):
    for xx in range(__BLOCKSIZE):
        plt.arrow(
                xx,
                yy,
                np.cos(th[starty+yy,startx+xx])*0.1,
                np.sin(th[starty+yy,startx+xx])*0.1,
                head_width=0.1,
                head_length=0.1,
                fc='r',
                ec='r'
                )






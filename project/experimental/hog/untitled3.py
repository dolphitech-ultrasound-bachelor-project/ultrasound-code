# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 19:29:16 2020

@author: StigA
"""

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from scipy.signal import argrelmin
import numpy as np
import h5py
import cv2

from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

__IMAGESIZE = 128

__TOTALDEGREES = 360
__BINSIZE = 18
__BINDELTA = int(__TOTALDEGREES/__BINSIZE)

__BLOCKSIZE = 64
__NUMBEROFBLOCKS = int(__IMAGESIZE/__BLOCKSIZE)


cScan, startGate, endGate = loader.load("../resources/ultrasound1.h5")
cScan = cScan[startGate:endGate]

original = ustool.normalize(ustool.getImageByMaximum(cScan))
plt.figure(); plt.imshow(original,"gray")

gy, gx = np.gradient(original)
gx[gx==0] = 1E-09
mg = np.sqrt(gx**2 + gy**2)
th = np.round(np.rad2deg(np.arctan(gy/gx))+180).astype(np.int)

X,Y=np.meshgrid(np.arange(0,__IMAGESIZE),np.arange(0,__IMAGESIZE))
skip=(slice(None,None,4),slice(None,None,4))


histograms = np.zeros((__NUMBEROFBLOCKS,__NUMBEROFBLOCKS,__BINSIZE))


indexer = (
        __IMAGESIZE*np.arange(__BLOCKSIZE)[:,None,None,None]
        + np.arange(__BLOCKSIZE)[None,:,None,None]
        +__BLOCKSIZE*__IMAGESIZE*np.arange(__NUMBEROFBLOCKS)[None,None,:,None]
        +__BLOCKSIZE*np.arange(__NUMBEROFBLOCKS)[None,None,None,:]
)

print(indexer[:,:,0,0])
print(indexer.shape)

shape = indexer.shape
convolutions = original.flatten()[indexer.flatten()].reshape(shape)
print(convolutions.shape)
for y in range(__NUMBEROFBLOCKS):
    for x in range(__NUMBEROFBLOCKS):
        plt.figure(); plt.imshow(convolutions[:,:,y,x],"gray")













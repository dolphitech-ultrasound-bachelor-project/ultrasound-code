# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 19:48:53 2020

@author: StigA
"""

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from scipy.signal import argrelmin
import numpy as np
import h5py
import cv2

from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

__IMAGESIZE = 128

__TOTALDEGREES = 360
__BINSIZE = 18
__BINDELTA = int(__TOTALDEGREES/__BINSIZE)

__BLOCKSIZE = 4
__NUMBEROFBLOCKS = int(__IMAGESIZE/__BLOCKSIZE)


cScan, startGate, endGate = loader.load("../resources/ultrasound1.h5")
cScan = cScan[startGate:endGate]

original = ustool.getImageByMaximum(cScan)
plt.figure(); plt.imshow(original,"gray")

gy, gx = np.gradient(original)
gx[gx==0] = 1E-09
mg = np.sqrt(gx**2 + gy**2)
th = np.round(np.rad2deg(np.arctan(gy/gx))+180).astype(np.int)

X,Y=np.meshgrid(np.arange(0,__IMAGESIZE),np.arange(0,__IMAGESIZE))
skip=(slice(None,None,4),slice(None,None,4))


histograms = np.zeros((__NUMBEROFBLOCKS,__NUMBEROFBLOCKS,__BINSIZE))
print('\nHISTOGRAMS SHAPE\n',histograms.shape)


indexer = (
        __IMAGESIZE*np.arange(__BLOCKSIZE)[:,None,None,None]
        + np.arange(__BLOCKSIZE)[None,:,None,None]
        +__BLOCKSIZE*__IMAGESIZE*np.arange(__NUMBEROFBLOCKS)[None,None,:,None]
        +__BLOCKSIZE*np.arange(__NUMBEROFBLOCKS)[None,None,None,:]
)
print('\nINDEXER SHAPE\n',indexer.shape)
print('\nINDEXER (0,0)\n',indexer[:,:,0,0])


th_convolutions = th.flatten()[indexer.flatten()].reshape(indexer.shape)
mg_convolutions = mg.flatten()[indexer.flatten()].reshape(indexer.shape)
print('\nTH CONVOLUTIONS (0,0)\n',th_convolutions[:,:,0,0])
print('\nMG CONVOLUTIONS (0,0)\n',mg_convolutions[:,:,0,0])

histogram_indexes = (th_convolutions / __BINDELTA) - 1
histogram_indexes_ceiling = np.ceil(histogram_indexes).astype(np.int)
histogram_indexes_floor = np.floor(histogram_indexes).astype(np.int)
print('\nHISTOGRAM INDEXES (0,0)\n',histogram_indexes[:,:,0,0])
print('\nHISTOGRAM CEILINGS (0,0)\n',histogram_indexes_ceiling[:,:,0,0])
print('\nHISTOGRAM FLOORS (0,0)\n',histogram_indexes_floor[:,:,0,0])


floor_percentages = histogram_indexes_ceiling - histogram_indexes
ceiling_percentages = 1 - floor_percentages
ceiling_votes = ceiling_percentages*mg_convolutions
floor_votes = floor_percentages*mg_convolutions
print('\nCEILING VOTES (0,0)\n',ceiling_votes[:,:,0,0])
print('\nFLOOR VOTES (0,0)\n',floor_votes[:,:,0,0])


print('\n',histograms.shape)
print('\n',histogram_indexes_ceiling.shape)
print('\n',ceiling_votes.shape)

print('\n',histograms[0,0,:])
print('\n',histogram_indexes_ceiling[:,:,0,0])
print('\n',ceiling_votes[:,:,0,0])


print('\n',histograms[0,0,:])
print('\n',histogram_indexes_ceiling[:,:,0,0].flatten())
print('\n',ceiling_votes[:,:,0,0].flatten())

histograms[0,0,histogram_indexes_ceiling[:,:,0,0].flatten()] += histogram_indexes_ceiling[:,:,0,0].flatten()
#histograms[0,0,histogram_indexes_floor[:,:,0,0].flatten()] += floor_votes[:,:,0,0].flatten()

print('\n',histograms[0,0,:])


#for y in range(__NUMBEROFBLOCKS):
#    for x in range(__NUMBEROFBLOCKS):
#            histograms[y,x,histogram_indexes_ceiling[:,:,y,x].flatten()] += ceiling_votes[:,:,y,x].flatten()
#            histograms[y,x,histogram_indexes_floor[:,:,y,x].flatten()] += floor_votes[:,:,y,x].flatten()
#
#print('\n',histograms[0,0,:])


#histograms[histogram_indexes_ceiling] += ceiling_votes
#histograms[histogram_indexes_floor] += floor_votes

#index = (th[ys,xs].flatten()/bin_delta) - 1
#ceiling = np.ceil(index).astype(np.int)
#floor = np.floor(index).astype(np.int)
#
#print(index)
## how much should go to bottom
#print(np.ceil(index)-index)
## how much should go to top
#print(index-np.floor(index))
#
#bins[ceiling] += (index-np.floor(index))*mg[ys,xs].flatten()
#bins[floor] += (np.ceil(index)-index)*mg[ys,xs].flatten()
#degree = (bins.argmax()+1)*bin_delta




















# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 18:32:42 2020

@author: StigA
"""

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.ticker as plticker
from scipy.signal import argrelmin
from skimage import data
import numpy as np
import h5py
import cv2

from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

# We load the image
cScan, startGate, endGate = loader.load("../resources/ultrasound1.h5")
cScan = cScan[startGate:endGate]
original = ustool.getImageByMaximum(cScan)
original = cv2.bilateralFilter(original,3,50,50)
original = ustool.normalize(original)
featureVector = ustool.getHOGFeatureVector(original)

plt.figure(); plt.imshow(original,"gray")
plt.figure(); plt.plot(featureVector)
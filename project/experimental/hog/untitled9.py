# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 17:35:45 2020

@author: StigA
"""

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.ticker as plticker
from scipy.signal import argrelmin
from skimage import data
import numpy as np
import h5py
import cv2

from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

# Adjustable parameters
__TOTALDEGREES = 360
__BINDELTA = 45
__BINSIZE = int(__TOTALDEGREES/__BINDELTA) + 1

__IMAGESIZE = 128
__BLOCKSIZE = 4
__NUMBEROFBLOCKS = int(__IMAGESIZE/__BLOCKSIZE)


# We load the image
cScan, startGate, endGate = loader.load("../resources/airbus2_3.h5")
cScan = cScan[startGate:endGate]
original = ustool.getImageByMaximum(cScan)
original = cv2.bilateralFilter(original,3,50,50)
original = ustool.normalize(original)

plt.figure(); plt.imshow(original,"gray")

# We calculate the needed data
# Note:
# +180 is for bin division (range is -180 <-> 180, but now its 0-360).
# To display arrows one should subtract 180 again
gy = np.gradient(original,axis=0)
gx = np.gradient(original,axis=1)
mg = np.sqrt(gx**2 + gy**2)
th = np.rad2deg(np.arctan2(gy,gx))+180


# Empty array for the hogs
hogs = np.zeros((__NUMBEROFBLOCKS,__NUMBEROFBLOCKS,__BINSIZE))


# Indexer that lets us vectorize alot of the operations
indexer = np.transpose((
            __IMAGESIZE*np.arange(__BLOCKSIZE)[:,None,None,None]
            + np.arange(__BLOCKSIZE)[None,:,None,None]
            +__BLOCKSIZE*__IMAGESIZE*np.arange(__NUMBEROFBLOCKS)[None,None,:,None]
            +__BLOCKSIZE*np.arange(__NUMBEROFBLOCKS)[None,None,None,:]
        ),axes=(2,3,0,1))


# The convolution from vectorization
th_convolutions = th.flatten()[indexer.flatten()].reshape(indexer.shape)
mg_convolutions = mg.flatten()[indexer.flatten()].reshape(indexer.shape)


# Calculate the divide
hog_indexes = (th_convolutions / __BINDELTA)
hog_indexes_ceiling = np.ceil(hog_indexes).astype(np.int)
hog_indexes_floor = np.floor(hog_indexes).astype(np.int)


# Calculate the divide votes
floor_percentages = hog_indexes_ceiling - hog_indexes
ceiling_percentages = 1 - floor_percentages
ceiling_votes = ceiling_percentages*mg_convolutions
floor_votes = floor_percentages*mg_convolutions


# Add the votes into the hogs
featureVector = []
for y in range(__NUMBEROFBLOCKS):
    for x in range(__NUMBEROFBLOCKS):
        np.add.at(hogs[y,x],hog_indexes_ceiling[y,x].flatten(),ceiling_votes[y,x].flatten())
        np.add.at(hogs[y,x],hog_indexes_floor[y,x].flatten(),floor_votes[y,x].flatten())


# We create the featureVectore of normalized hogs
featureVector = []
for y in range(__NUMBEROFBLOCKS-1):
    for x in range(__NUMBEROFBLOCKS-1):
        flatHogs = np.concatenate([
            hogs[y,x],   hogs[y,x+1],
            hogs[y+1,x], hogs[y+1,x+1]
        ],axis=0)
        normalizedFlatHogs = flatHogs / np.sqrt((flatHogs).sum())
        featureVector.extend(normalizedFlatHogs)


#plt.figure(); plt.plot(hogs.flatten())
plt.figure(); plt.plot(featureVector)
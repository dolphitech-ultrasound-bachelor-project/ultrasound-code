# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 15:40:54 2020

@author: StigA
"""

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from scipy.signal import argrelmin
import numpy as np
import h5py
import cv2

from sources import loader
from sources import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

cScan, startGate, endGate = loader.load("../resources/ultrasound1.h5")
cScan = cScan[startGate:endGate]

original = ustool.normalize(ustool.getImageByMaximum(cScan))
plt.figure(); plt.imshow(original,"gray")

gy, gx = np.gradient(original)
gx[gx==0] = 1E-09
mg = np.sqrt(gx**2 + gy**2)
th = np.round(np.rad2deg(np.arctan(gy/gx))+180).astype(np.int)

plt.figure(); plt.imshow(gx,"gray")
plt.figure(); plt.imshow(gy,"gray")
fig,ax = plt.subplots(1);
plt.imshow(mg,"gray")

gx = gx/mg
gy = gy/mg

X,Y=np.meshgrid(np.arange(0,128),np.arange(0,128))
skip=(slice(None,None,10),slice(None,None,10))
plt.quiver(X[skip],Y[skip],gx[skip],gy[skip],color="red",units='width',scale=32,width=0.005)

# <-----------   er 0 grader mens --------> da er 180 grader


x = 0
y = 1

# BLOCK CREATION
block_size = 64

# BIN CREATION
number_of_bins = 8
bin_delta = 360 / number_of_bins
bins = np.zeros(number_of_bins)

xs = slice(x*block_size,(x+1)*block_size,1)
ys = slice(y*block_size,(y+1)*block_size,1)

print(th[ys,xs].flatten())

index = (th[ys,xs].flatten()/bin_delta) - 1
ceiling = np.ceil(index).astype(np.int)
floor = np.floor(index).astype(np.int)

print(index)
# how much should go to bottom
print(np.ceil(index)-index)
# how much should go to top
print(index-np.floor(index))

bins[ceiling] += (index-np.floor(index))*mg[ys,xs].flatten()
bins[floor] += (np.ceil(index)-index)*mg[ys,xs].flatten()
degree = (bins.argmax()+1)*bin_delta

rect = patches.Rectangle((x*block_size,y*block_size),block_size,block_size,linewidth=1,edgecolor='y',facecolor='none')
ax.add_patch(rect)
plt.show()

print(degree)

original_convolution = mg[ys,xs]
plt.figure(); plt.imshow(original_convolution,"gray")














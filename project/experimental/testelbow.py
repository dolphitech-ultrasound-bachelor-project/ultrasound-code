# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 01:07:09 2020

@author: StigA
"""

import matplotlib.pyplot as plt

data = [0.48,0.14,0.05,0.03,0.02,0.02,0.02,0.02]

figure, axes = plt.subplots(figsize=(16,8))

axes.set_xlabel('Blocksizes', fontsize=20)
axes.set_xticklabels(('0','1','2','4','8','16','32','64',''))
plt.xticks(fontsize=20)

axes.axvline(3,linestyle=':',color='red', lw=3)

axes.set_ylabel('Processing Times (Seconds)', fontsize=20)
plt.yticks(fontsize=20)

axes.text(2,0.52,"Limit of reasonable block size",color='red',fontsize=20)

axes.plot(data, linewidth=3)

plt.show()
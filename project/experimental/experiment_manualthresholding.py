# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 22:00:12 2020

@author: Stig
"""

from sources import ustool
from sources import utility

# -----------------------------------------------------------------------------#
#                                   CODE                                      #
# -----------------------------------------------------------------------------#

# Retrieves the image from a scan, and makes it openCV compliant
image = ustool.returnImageByMaximum("ultrasound1")
utility.display(image)

utility.threshold(image, 130, displaysetting=True)
utility.threshold(image, 150, displaysetting=True)
utility.threshold(image, 170, displaysetting=True)
utility.threshold(image, 190, displaysetting=True)
utility.threshold(image, 210, displaysetting=True)

# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 22:55:32 2020

@author: Stig
"""
import sys
import matplotlib.pyplot as plt
import numpy as np
import cv2


def __checkImageValidity(image):
    """

    Args:
        image:
    """
    if not image.ndim == 2:
        print("Function can only receive grayscale images")
        sys.exit(1)
    elif not image.dtype == np.uint8:
        print("Function can only receive images of type np.uint8")
        sys.exit(1)


def display(image, histogram=False):
    """
    Function that displays a given image, with its properties as title

    Args:
        image:
        histogram:
    """
    __checkImageValidity(image)

    if histogram:
        __, ax = plt.subplots(1, 2)
        ax[0].imshow(image, "gray")
        ax[0].set_title(
            "[DTYPE: " + str(image.dtype) + "][" + "INTERVAL: " + str(image.min()) + " - " + str(image.max()) + "]\n")
        ax[1].plot(cv2.calcHist([image], [0], None, [256], [0, 256]))
        plt.gca().set_aspect('equal', adjustable='box')
        plt.show()

    else:
        __, ax = plt.subplots()
        ax.imshow(image, "gray")
        ax.set_title(
            "[DTYPE: " + str(image.dtype) + "][" + "INTERVAL: " + str(image.min()) + " - " + str(image.max()) + "]\n")
        plt.show()


def normalize(data):
    """
    Function that returns a normalized version of the data

    Args:
        data:

    Returns:

    """
    temp = data.copy()
    temp = temp - temp.min()
    temp = (temp / (temp.max()-temp.min())).astype(np.float64)

    return temp


def threshold(image, thresholdLevel, displaySetting=False):
    """
    Function that thresholds a given image by a given treshold,
    if the display flag is set, it will display results before return
    Args:
        image:
        thresholdLevel:
        displaySetting:

    Returns:

    """
    __checkImageValidity(image)

    temp = image.astype(np.uint8)
    temp[image > thresholdLevel] = 255
    temp[image <= thresholdLevel] = 0

    if displaySetting:
        __, ax = plt.subplots(1, 2)
        histogram = cv2.calcHist([image], [0], None, [256], [0, 256])
        ax[0].imshow(temp, "gray")
        ax[1].plot(histogram)
        plt.axvline(x=thresholdLevel, color='red')
        plt.gca().set_aspect('equal', adjustable='box')
        plt.show()

    return temp

# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from scipy.ndimage import median_filter
import numpy as np
import os

from project.sources.general import loader
from project.sources.general import ustool

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

# Retrieve all the files
filepaths = []
for root, directories, files in os.walk('../resources/h5'):
    for file in files:
        if file.endswith('.h5'):
            filepaths.append(os.path.normpath(os.path.join(root, file)))

# Retrieve the scans, smooth them and display
for i, path in enumerate(filepaths):
    
    originalScan, startGate, endGate = loader.load(path)
    smoothScan = median_filter(originalScan, (3, 3, 3), mode='constant', cval=0.0)
    maximumAmplitudeImage = ustool.getImageByMaximum(smoothScan[startGate:endGate])
    responseFunction = ustool.getResponseFunction(smoothScan, 0.01)
    
    figure, axes = plt.subplots(1, 2, figsize=(12, 2))
    axes[0].imshow(maximumAmplitudeImage, "gray")
    axes[1].plot(responseFunction)
    axes[0].text(196, 64, '[NUMBER: {}]'.format(i), horizontalalignment='center', verticalalignment='center')
    plt.setp(axes, xticks=[], yticks=[])
    plt.show(block=False)
    
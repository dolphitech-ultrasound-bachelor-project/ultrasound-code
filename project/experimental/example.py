# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 22:00:12 2020

@author: Stig
"""
import cv2

from sources import ustool
from sources import utility

# -----------------------------------------------------------------------------#
#                                   CODE                                      #
# -----------------------------------------------------------------------------#

# Retrieves the image from a scan, and makes it openCV compliant
image = ustool.returnimagebymaximum("123_4")

# Displays the image, with it's histogram
utility.display(image, histogram=True)

equalized = cv2.equalizeHist(image)
utility.display(equalized, histogram=True)

binary = utility.threshold(image, 150)
utility.display(binary)

edges = cv2.Canny(binary, 100, 200)
utility.display(edges)

wave = ustool.showwavebysensor("123_4",80,60)

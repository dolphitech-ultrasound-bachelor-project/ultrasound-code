# -*- coding: utf-8 -*-
"""
Created on 01.04.20

@author: StigA
@co-author: FranzA
"""

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.ticker as plticker
import tensorflow as tf
import numpy as np
import scipy as sp
from sklearn import model_selection
import pickle
import h5py
import sys
import cv2

from sources import loader
from sources import ustool

np.set_printoptions(suppress=True)
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(linewidth=1000)

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

filepaths = [
    "../resources/ultrasound1.h5",
    "../resources/ultrasound2.h5",
    "../resources/airbus1_1.h5",
    "../resources/airbus1_4.h5",
    "../resources/airbus2_1.h5",
    "../resources/airbus2_2.h5",
    "../resources/airbus2_3.h5",
    "../resources/airbus2_5.h5",
    "../resources/123_4.h5",
    "../resources/123_5.h5",
    "../resources/123_6.h5",
    "../resources/airbus1_5.h5",
    "../resources/airbus1_6.h5",
    "../resources/airbus2_4.h5",
    "../resources/boeing1_1.h5",
    "../resources/mono1_1.h5"
]

cScan, start, end = loader.load(filepaths[5])

with open('../resources/model/rfc.model', 'rb') as input1:
    model = pickle.load(input1)

aScans = sp.signal.resample(cScan[:, 50:-54, 54:-50], 128, axis=0)
aScans = np.reshape(aScans, (aScans.shape[0], aScans.shape[1] * aScans.shape[2]))
aScans = np.swapaxes(aScans, 0, 1)

predictionsBool = model.predict(aScans).reshape(24, 24)
predictions = np.zeros((24, 24))

for x in range(24):
    for y in range(24):
        if predictionsBool[x][y]:
            predictions[x][y] = 1
        else:
            predictions[x][y] = 0

print(np.around(predictions, decimals=2))

image = ustool.getImageByMaximum(cScan[start:end])
plt.figure()
plt.imshow(image, "gray")
plt.figure()
plt.imshow(image[50:-54, 54:-50], "gray")
plt.show()


downsampledCScan = sp.signal.resample(cScan, 128, axis=0)
downsampledCScan = np.reshape(downsampledCScan,
                              (downsampledCScan.shape[0], downsampledCScan.shape[1] * downsampledCScan.shape[2]))
downsampledCScan = np.swapaxes(downsampledCScan, 0, 1)
predictions = model.predict(downsampledCScan).reshape(128, 128)
plt.figure()
plt.imshow(predictions, "gray")
plt.show()


threshold = 0.50
predictions[predictions >= threshold] = 1
predictions[predictions < threshold] = 0
predictions = (predictions * 255).astype(np.uint8)
image = (image[:, :, None] + np.zeros(3)[None, None, :]).astype(np.uint8)
color = np.zeros(image.shape, image.dtype)
color[:, :] = (255, 0, 0)
colormask = cv2.bitwise_and(color, color, mask=predictions)
cv2.addWeighted(colormask, 0.25, image, 1, 0, image)
plt.figure()
plt.imshow(image)
plt.show()

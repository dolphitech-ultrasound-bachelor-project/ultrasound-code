# -*- coding: utf-8 -*-
"""
Created on Fri May  1 16:01:42 2020

@author: StigA
"""

import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import pickle
import cv2
import os

import sources.general.loader as loader
import sources.general.ustool as ustool
from sources.general.confusionMatrix import getConfusionMatrix
from sources.ascan.binaryClassification import binaryClassifyCScan


# Retrieve all the h5 files
filepaths = []
for root, directories, files in os.walk("./resources/h5"):
    for file in files:
        if file.endswith(".h5"):
            filepaths.append(os.path.normpath(os.path.join(root, file)))

labels = []
neuralNetworkPredictions = []
contourMatchPredictions = []

for i, scanpath in enumerate(filepaths):
    filename = os.path.basename(scanpath)
    filename = os.path.splitext(filename)[0]
    path = None
    guess1 = "resources/dataset/A-scan dataset/traindata/ascan_" + filename + ".dta"
    guess2 = "resources/dataset/A-scan dataset/testdata/ascan_" + filename + ".dta"
    train = False
    try:
        file = open(guess1)
        path = guess1
        train = True
    except IOError:
        path = guess2
        train = False

    dataset = []
    with open(path, 'rb') as file:
        while True:
            try:
                dataset = pickle.load(file)
            except EOFError:
                break

    originalMask = np.zeros((128, 128), dtype=np.uint8)
    for entry in dataset:
        if entry.damaged:
            originalMask[entry.position[0], entry.position[1]] = 255


    cScan, startGate, endGate = loader.load(scanpath)
    model = tf.keras.models.load_model("resources/model/aScanFCNNModelT")
    contours, predictions, timestamps = binaryClassifyCScan(
                                            cScan,
                                            model,
                                            medianFiltering=False,
                                            blockSize=8,
                                            predictionThreshold=0.50,
                                            contourThreshold=0.75
                                        )

    maximumAmplitudeImage = ustool.getImageByMaximum(cScan[startGate:endGate])
    maximumAmplitudeImage = cv2.cvtColor(maximumAmplitudeImage, cv2.COLOR_GRAY2RGB)
    cv2.drawContours(maximumAmplitudeImage, contours, -1, (255, 255, 0), 2)

    thresholdedPredictions = predictions.copy()
    thresholdedPredictions[predictions >= 0.50] = 1
    thresholdedPredictions[predictions < 0.50] = 0
    predictionMask = (thresholdedPredictions * 255).astype(np.uint8)

    unionMask = np.zeros((128, 128), np.uint8)
    cv2.drawContours(unionMask, contours, -1, 255, -1)

    gpmask = np.zeros((128,128,3), np.uint8)
    gpmask[:,:,1] = originalMask
    gpmask[:,:,2] = predictionMask

    gcmask = np.zeros((128,128,3), np.uint8)
    gcmask[:,:,1] = originalMask
    gcmask[:,:,2] = unionMask

    predictionImage = (ustool.normalize(predictions) * 255).astype(np.uint8)

    labels.extend((originalMask==255).flatten().tolist())
    neuralNetworkPredictions.extend((predictionMask==255).flatten().tolist())
    contourMatchPredictions.extend((unionMask==255).flatten().tolist())

    if train:
        print("TRAINING DATASET SAMPLE")
    else:
        print("TESTING DATASET SAMPLE")

    figure, axes = plt.subplots(1,5,figsize=(24,24))
    axes[0].imshow(ustool.getImageByMaximum(cScan[startGate:endGate]), "gray")
    axes[1].imshow(predictionImage, "gray")
    axes[2].imshow(gpmask)
    axes[3].imshow(maximumAmplitudeImage, "gray")
    axes[4].imshow(gcmask)
    plt.setp(axes, xticks=[], yticks=[])
    plt.gca().set_axis_off()
    plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0,
                hspace = 0.001, wspace = 0.05)
    plt.margins(0,0)
    plt.gca().xaxis.set_major_locator(plt.NullLocator())
    plt.gca().yaxis.set_major_locator(plt.NullLocator())

    if train:
        plt.savefig("resources/images/b8_nm_mt_tr_{}.png".format(i),bbox_inches='tight')
    else:
        plt.savefig("resources/images/b8_nm_mt_ts_{}.png".format(i),bbox_inches='tight')

    plt.show(block=False)

tn1, fp1, fn1, tp1 = getConfusionMatrix(labels, neuralNetworkPredictions)
tn2, fp2, fn2, tp2 = getConfusionMatrix(labels, contourMatchPredictions)




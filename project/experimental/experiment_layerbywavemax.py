# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 21:07:52 2020

@author: Stig
"""
import matplotlib.pyplot as plt
import numpy as np

from project.sources.general import ustool
from project.experimental import utility

# -----------------------------------------------------------------------------#
#                                   CODE                                      #
# -----------------------------------------------------------------------------#

x = 50
y = 90
resourcename = "ultrasound1"

wave = ustool.returnWaveBySensor(resourcename, x, y)
plt.figure()
plt.plot(wave)
index1 = np.argmax(wave)

plt.figure()
plt.plot(np.abs(wave))
index2 = np.argmax(np.abs(wave))

cumulative = np.cumsum(wave)
plt.figure()
plt.plot(cumulative)
index3 = np.argmax(cumulative)

plt.figure()
plt.plot(np.abs(cumulative))
index4 = np.argmax(np.abs(cumulative))

image = ustool.returnImageByMaximum(resourcename)
utility.display(image)

chosenIndex = index1
# chosenIndex = index2
# chosenIndex = index3
# chosenIndex = index4
layer = ustool.returnImageByLayer(resourcename, chosenIndex)
utility.display(layer)

print(str(int((chosenIndex / wave.size) * 100)) + "% breakthrough before hit")

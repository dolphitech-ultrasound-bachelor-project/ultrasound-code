"""
Helper library to apply & combine filters on images, and plot the results
"""
import pickle

import cv2
import matplotlib.pyplot as plt
import numpy as np

from PIL import Image, ImageFilter, ImageEnhance
from dataset.trainingDtaCreator import EntryFull


def openingFilter(images: list, kernelSize: int):
    """
    Applies CV2 opening filter on the images sent as parameter

    Args:
        images:     Which samples to apply opening filter on
        kernelSize: Size of the kernel for sliding window operations

    Returns: None
    """
    for i, image in enumerate(images):
        if isinstance(image, Image.Image):
            image = np.array(image)

        kernel = np.ones((kernelSize, kernelSize), np.uint8)
        images[i] = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)

    return images


def closingFilter(images: list, kernelSize: int = 3):
    """
    Applies CV2 closing filter on the images sent as parameter

    Args:
        images:     Which samples to apply closing filter on
        kernelSize: Size of the kernel for sliding window operations

    Returns: List of images after applying closing filter
    """
    for i, image in enumerate(images):
        if isinstance(image, Image.Image):
            image = np.array(image)

        kernel = np.ones((kernelSize, kernelSize), np.uint8)
        images[i] = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)

    return images


def contrasting(images: list, contrast: float = 2.0):
    """
    Applies sharpening to given images

    Args:
        images:     The images to change contrast on
        contrast:  The strength of the contrasting

    Returns: List of images after contrasting
    """
    for i, image in enumerate(images):
        if isinstance(image, np.ndarray):
            image = Image.fromarray(image)

        contraster = ImageEnhance.Contrast(image)
        images[i] = contraster.enhance(contrast)

    return images


def bilateralFilter(images: list, kernelSize: int = 3, sigmaColor: int = 5, sigmaSpace: int = 5):
    """
    Applies bilateral filtering on the given images

    Args:
        images:     List of images to apply filtering to
        kernelSize: Size of the sliding window
        sigmaColor: Adjusts cv2 color space
        sigmaSpace:

    Returns: List of images after filtering
    """
    for i, image in enumerate(images):
        if isinstance(image, Image.Image):
            image = np.array(image)

        images[i] = cv2.bilateralFilter(image, kernelSize, sigmaColor, sigmaSpace)

    return images


def sharpening(images: list, sharpness: float = 2.0):
    """
    Applies sharpening to given images

    Args:
        images:     The images to sharpen
        sharpness:  The strength of the sharpening

    Returns: List of images after sharpening
    """
    for i, image in enumerate(images):
        if isinstance(image, np.ndarray):
            image = Image.fromarray(image)

        sharpener = ImageEnhance.Sharpness(image)
        images[i] = sharpener.enhance(sharpness)

    return images


def medianFilter(images: list, kernelSize: int = 3):
    """
    Applies median filtering to given images

    Args:
        images:     Which images to median filter
        kernelSize: The size of the sliding window

    Returns: List of images after median filtering
    """
    for i, image in enumerate(images):
        if isinstance(image, np.ndarray):
            image = Image.fromarray(image)

        images[i] = image.filter(ImageFilter.MedianFilter(kernelSize))

    return images


def erosion(images: list, kernelSize: int, iterations: int = 1):
    """
    Applies erosion to a list of images

    Args:
        images:     Images to apply erosion to
        kernelSize: The sliding window size
        iterations: Amount of iterations to perform erosion

    Returns: List of eroded images
    """
    for i, image in enumerate(images):
        if isinstance(image, Image.Image):
            image = np.array(image)

        kernel = np.ones((kernelSize, kernelSize), np.uint8)
        images[i] = cv2.erode(image, kernel, iterations)

    return images


def dilation(images: list, kernelSize: int, iterations: int = 1):
    """
    Applies dilation to a list of images

    Args:
        images:     Images to apply dilation to
        kernelSize: The sliding window size
        iterations: Amount of iterations to perform erosion

    Returns: List of dilated images
    """
    for i, image in enumerate(images):
        kernel = np.ones((kernelSize, kernelSize), np.uint8)
        images[i] = cv2.dilate(image, kernel, iterations)

    return images


def plot(images: list, title: str = ""):
    """
    Plots all given images, with an empty title plot first. Mostly used for the report.
    Args:
        images: The images to plot
        title:  The title for the plots

    Returns: None
    """
    # Create title plot
    if title != "":
        plt.title(title)
        plt.show()

    fig: plt.Figure = plt.figure()
    fig.set_size_inches(6, 6)

    # Plot all images
    for image in images:
        plt.imshow(image, "gray")
        plt.show()


def plotFilterComparison(images: list = None, sharpness: float = 2.0, contrast: float = 2.0,
                         kernelSize: int = 3, sigmaColor: int = 5, sigmaSpace: int = 5, iterations: int = 1):
    """
    Opens four samples from the dataset, and plots different filtering techniques for these images
    Args:
        images:         Images to plot
        sharpness:      Strength of the sharpening filter
        contrast:       Strength of the contrast filter
        kernelSize:     Size of the kernel for sliding window operations
        sigmaColor:     Color space for cv2, higher vals => more distinct colors within the pixel windows are mixed
        sigmaSpace:     Coordinate space for cv2, higher vals => more distant pixels within the pixel windows influenced
        iterations:     Amount of iterations for erosion/dilation

    Returns: None
    """
    # Open/load the dataset
    if images is None:
        with open('../resources/dataset/dataset.dta', 'rb') as inputData:
            dta: list = pickle.load(inputData)

        # Load four sample images; '123_3', '123_5', 'airbus1_1', and 'airbus1_5'
        # 123_3
        sampleA: EntryFull = dta[0]
        imageA = Image.fromarray(sampleA.data)
        if sampleA.name != "123_3":
            raise ValueError("Sample A is incorrect, should be 123_3 but is " + sampleA.name)

        # 123_5
        sampleB: EntryFull = dta[2]
        imageB = Image.fromarray(sampleB.data)
        if sampleB.name != "123_5":
            raise ValueError("Sample B is incorrect, should be 123_5 but is " + sampleB.name)

        # airbus1_1
        sampleC: EntryFull = dta[4]
        imageC = Image.fromarray(sampleC.data)
        if sampleC.name != "airbus1_1":
            raise ValueError("Sample C is incorrect, should be airbus1_1 but is " + sampleC.name)

        # airbus1_5
        sampleD: EntryFull = dta[6]
        imageD = Image.fromarray(sampleD.data)
        if sampleD.name != "airbus1_5":
            raise ValueError("Sample D is incorrect, should be airbus1_5 but is " + sampleD.name)

        # List of the image data
        images = [imageA, imageB, imageC, imageD]

    # Run all plots
    if images is not None:
        plot(images, "Originals")
        plot(openingFilter(images, kernelSize), "Opening Filter")
        plot(closingFilter(images, kernelSize), "Closing Filter")
        plot(contrasting(images, contrast), "Contrasted")
        plot(sharpening(images, sharpness), "Sharpened")
        plot(bilateralFilter(images, kernelSize, sigmaColor, sigmaSpace), "Bilateral Filter")
        plot(medianFilter(images, kernelSize), "Median Filter")
        plot(erosion(images, kernelSize, iterations), "CV2 Erosion")
        plot(dilation(images, kernelSize, iterations), "CV2 Dilation")

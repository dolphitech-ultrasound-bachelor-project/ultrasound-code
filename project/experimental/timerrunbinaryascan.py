# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import cv2
import os

import sources.general.loader as loader
import sources.general.ustool as ustool
from sources.ascan.binaryClassification import binaryClassifyCScan


# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

filepaths = []
for root, directories, files in os.walk("./resources/h5"):
    for file in files:
        if file.endswith(".h5"):
            filepaths.append(os.path.normpath(os.path.join(root, file)))

model = tf.keras.models.load_model("resources/model/WDASCANFCNN")

timestamps = []
for filepath in filepaths:
    cScan, start, end = loader.load(filepath)
    timestamps.append(binaryClassifyCScan(
        cScan,
        model,
        medianFiltering=False,
        blockSize=8,
        predictionThreshold=0.50,
        contourThreshold=0.75
    )[2])

timestamps = [sum(i)/len(timestamps) for i in zip(*timestamps)]

print(
      "\n(0) - Total time [{0:.2f}S]".format(timestamps[0])
      + "\n(1) - Median Filtering time [{0:.2f}S]".format(timestamps[1])
      + "\n(2) - Blocks processing time [{0:.2f}S]".format(timestamps[2])
      + "\n(3) - Downsampling processing time [{0:.2f}S]".format(timestamps[3])
      + "\n(4) - Classifier Prediction time [{0:.2f}S]".format(timestamps[4])
      + "\n(5) - Interpolation processing time [{0:.2f}S]".format(timestamps[5])
      + "\n(6) - Slice processing time [{0:.2f}S]".format(timestamps[6])
      + "\n(7) - Contour processing time [{0:.2f}S]".format(timestamps[7])
)
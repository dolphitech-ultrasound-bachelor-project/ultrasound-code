"""
Retrieves information about h5 samples, or plots the maxamp image for a given sample
"""
import h5py
from h5py import Dataset
import matplotlib.pyplot as plt

from general import loader
from general.ustool import getImageByMaximum


def plotSample(resourceName: str):
    """
    Plots the max amp image for a given h5 sample

    Args:
        resourceName: str: The name of the h5 sample

    Returns: None
    """
    # Get data and gates
    data, startGate, endGate = loader.load("../resources/h5/" + resourceName + ".h5")

    # Get the max amp image
    image = getImageByMaximum(data[startGate: endGate])

    # Plot image
    plt.imshow(image, "gray")
    plt.title(resourceName)
    plt.show()
    plt.imsave("originalA.eps", image, cmap="gray")


def getTXElements(resourceName: str, printTX=True):
    """
    Gets the amount of TX elements used for a given h5 sample

    Args:
        resourceName: str: The name of the h5 sample
        printTX: bool: (True) Whether or not to print the TX element

    Returns: int: Amount of TX elements
    """
    # Load the h5 file for the sample
    file = h5py.File("../resources/h5/" + resourceName + ".h5", "r")

    # Get amount of TX
    txElements: Dataset = file["DCV2/frames/1/settings/trm_1_tx_lines"]

    # Print TX if enabled
    if printTX:
        print((resourceName + "\tTX: " + str(txElements[0])).rjust(45))

    # Return amount of TX
    return txElements


def getSpeedOfSound(resourceName: str, printSpeed=True):
    """

    Args:
        resourceName:
        printSpeed:

    Returns:

    """
    # Load the h5 file for the sample
    file = h5py.File("../resources/h5/" + resourceName + ".h5", "r")

    # Get the speed of sound for the sample
    avgPulses: Dataset = file["DCV2/frames/1/settings/trm_1_n_avg"]

    # Print speed of sound if enabled
    if printSpeed:
        print((resourceName + "\tN pulses: " + str(avgPulses[0])).rjust(45))

    # Return amount of TX
    return avgPulses


# Which files to get information about
resourceNames = ["123_3", "123_4", "123_5", "123_6", "airbus1_1", "airbus1_4", "airbus1_5",
                 "airbus1_6", "airbus2_1", "airbus2_2", "airbus2_3", "airbus2_4", "airbus2_5",
                 "boeing1_1", "dacon_1", "dacon_2", "fiat1_1", "maieen1_1", "mono1_1", "nasa_d2_1",
                 "nasa_d2_2", "nasa_d2_3", "sam1_1", "toyota_0718_no6_1", "toyota_0718_no7_1",
                 "toyota_0718_no7_2", "TRM_AF_3.5MHz_CFRP_5mm_Good", "TRM_AF_3.5MHz_CFRP_5mm_Impact",
                 "TRM_AF_3.5MHz_CFRP_5mm_Impact10", "TRM_AF_3.5MHz_CFRP_5mm_Impact11",
                 "TRM_AF_3.5MHz_CFRP_5mm_Impact12", "TRM_AF_3.5MHz_CFRP_5mm_Impact13",
                 "TRM_AF_3.5MHz_CFRP_5mm_Impact14", "TRM_AF_3.5MHz_CFRP_5mm_Impact15",
                 "TRM_AF_3.5MHz_CFRP_5mm_Impact16", "TRM_AF_3.5MHz_CFRP_5mm_Impact2",
                 "TRM_AF_3.5MHz_CFRP_5mm_Impact3", "TRM_AF_3.5MHz_CFRP_5mm_Impact4",
                 "TRM_AF_3.5MHz_CFRP_5mm_Impact5", "TRM_AF_3.5MHz_CFRP_5mm_Impact6",
                 "TRM_AF_3.5MHz_CFRP_5mm_Impact7", "TRM_AF_3.5MHz_CFRP_5mm_Impact8",
                 "TRM_AF_3.5MHz_CFRP_5mm_Impact9", "TRM_AF_3.5MHz_CFRP_5mm_Small_Impact",
                 "TRM_AF_3.5MHz_CFRP_5mm_Small_Impact2", "TRM_CI5.00MHz_CFRP_Impact"]

# Retrieve information about every file (based on resourceNames above)
for _, sample in enumerate(resourceNames):
    getSpeedOfSound(sample)
    getTXElements(sample)

# Which file to plot
sample = "123_3"

# Get the max amp image
data, startGate, endGate = loader.load("../resources/h5/" + sample + ".h5")
image = getImageByMaximum(data[startGate: endGate])

plt.imshow(image, "gray")
plt.show()

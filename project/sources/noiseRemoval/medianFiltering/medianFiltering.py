"""
Code for performing/plotting 3D median filtering
"""
from scipy.ndimage import median_filter

from general import loader, ustool
import matplotlib.pyplot as plt


def threeDimensionalMedianFiltering(resourceName: str, windowSize=3, plot=True):
    """
    Performs median filtering in 3D, on a given resource

    Args:
        resourceName: Which resource to perform filtering on
        windowSize:   Kernel/window size
        plot:         Whether or not to plot images/waves

    Returns: Median filtered C-scan data
    """
    # Load C-scan data
    cScan, startGate, endGate = loader.load("../../../resources/h5/" + resourceName + ".h5")

    # Plot original image + A-scan wave
    if plot:
        plt.figure()
        plt.imshow(ustool.getImageByMaximum(cScan[startGate:endGate]), "gray")
        plt.title("Original image")
        plt.show()

        plt.figure()
        plt.plot(ustool.getAScan(cScan, 64, 64))
        plt.title("Original A-scan wave")
        plt.show()

    # Run median filtering on the C-scan
    smoothScan = median_filter(cScan, (windowSize, windowSize, windowSize), mode="constant", cval=0.0)

    # Plot filtered image + A-scan wave
    if plot:
        # Plot the median filtered image
        plt.figure()
        plt.imshow(ustool.getImageByMaximum(smoothScan[startGate:endGate]), "gray")
        plt.title("Median filtered image")
        plt.show()

        # Plot the median filtered wave
        plt.figure()
        plt.plot(ustool.getAScan(smoothScan, 64, 64))
        plt.title("Median filtered A-scan wave")
        plt.show()

    return smoothScan

"""
"Main" file to run 3D median filtering
"""

from noiseRemoval.medianFiltering.medianFiltering import threeDimensionalMedianFiltering

resourceName = "123_3"
threeDimensionalMedianFiltering(resourceName, windowSize=3, plot=True)

"""
"Main" file to run 3D bilateral filtering
"""

from noiseRemoval.bilateralFiltering.bilateralFiltering import threeDimensionalBilateralFiltering

resourceName = "123_3"
threeDimensionalBilateralFiltering(resourceName, windowSize=5, sigmaRange=5, sigmaSpatial=5, plot=True)

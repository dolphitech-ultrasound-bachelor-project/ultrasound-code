# -*- coding: utf-8 -*-
"""
Code for performing/plotting 3D bilateral filtering
"""

import matplotlib.pyplot as plt
import numpy as np

from project.sources.general import loader
from project.sources.general import ustool


def threeDimensionalBilateralFiltering(resourceName: str, windowSize=5, sigmaRange=5, sigmaSpatial=5, plot=True):
    """
    Performs bilateral filtering in 3D, on a given resource

    Args:
        resourceName: Which resource to perform filtering on
        windowSize:   Kernel/window size
        sigmaRange:   Controls pixel value gap
        sigmaSpatial: Controls euclidean pixel distance gap
        plot:         Whether or not to plot images/waves

    Returns: Bilateral filtered C-scan data
    """

    # Load C-scan data
    cScan, startGate, endGate = loader.load("../../../resources/h5/" + resourceName + ".h5")

    # Calculate the sliding window shift
    shift = (windowSize // 2)

    # Plot original image + A-scan wave
    if plot:
        plt.figure()
        plt.imshow(ustool.getImageByMaximum(cScan[startGate:endGate]), "gray")
        plt.title("Original image")
        plt.show()

        plt.figure()
        plt.plot(ustool.getAScan(cScan, 64, 64))
        plt.title("Original A-scan wave")
        plt.show()

    # Perform bilateral filtering as described by Tomasi and Manduchi
    signs = np.sign(cScan)
    normalizedAbsolute = ustool.normalize(np.abs(cScan))
    normalizedAbsolute[normalizedAbsolute == 0] = 1E-6
    scale = np.abs(cScan) / normalizedAbsolute

    cScan[cScan == 0] = 1E-6
    cScan = np.abs(cScan)
    cScan = ustool.normalize(cScan)
    padded = np.pad(cScan, pad_width=shift, mode='constant')

    filtered = np.zeros(cScan.shape, cScan.dtype)

    x = np.linspace(-shift, shift, 2 * shift + 1, endpoint=True, dtype=int)
    y = np.linspace(-shift, shift, 2 * shift + 1, endpoint=True, dtype=int)
    z = np.linspace(-shift, shift, 2 * shift + 1, endpoint=True, dtype=int)
    x, y, z = np.meshgrid(x, y, z)

    spatialFilter = np.sqrt((x ** 2 + y ** 2 + z ** 2) ** 2)
    spatialFilter = np.exp(-(spatialFilter ** 2) / (2 * sigmaSpatial ** 2))

    for i in range(shift, padded.shape[1] - shift):
        for j in range(shift, padded.shape[2] - shift):
            for t in range(shift, padded.shape[0] - shift):
                window = padded[t - shift:t + shift + 1, i - shift:i + shift + 1, j - shift:j + shift + 1]
                rangeFilter = np.sqrt((window - padded[t, i, j]) ** 2)
                rangeFilter = np.exp(-(rangeFilter ** 2) / (2 * sigmaRange ** 2))

                weight = window * rangeFilter * spatialFilter
                normalization = rangeFilter * spatialFilter
                normalization[normalization == 0] = 1E-6
                pixel = weight.sum() / normalization.sum()
                filtered[t - shift, i - shift, j - shift] = pixel

    smoothScan = signs * (ustool.normalize(filtered) * scale)

    if plot:
        # Plot the bilateral filtered image
        plt.figure()
        plt.imshow(ustool.getImageByMaximum(filtered[startGate:endGate]), "gray")
        plt.imsave("bilateralFilteredMaxAmp3D.eps", ustool.getImageByMaximum(filtered[startGate:endGate]), cmap="gray")
        plt.title("Bilateral filtered image")
        plt.show()

        # Plot the bilateral filtered wave
        plt.figure()
        plt.plot(ustool.getAScan(smoothScan, 64, 64))
        plt.title("Bilateral filtered A-scan wave")
        plt.show()

    return filtered

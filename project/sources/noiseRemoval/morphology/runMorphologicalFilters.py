"""
"Main" file to run morphological operators/filters
"""

from noiseRemoval.morphology.morphologicalOperations import erosionFilter, dilationFilter, openingFilter, closingFilter

resourceName = "123_3"
erosionFilter(resourceName, windowSize=3, plot=True)
dilationFilter(resourceName, windowSize=3, plot=True)
openingFilter(resourceName, windowSize=3, plot=True)
closingFilter(resourceName, windowSize=3, plot=True)

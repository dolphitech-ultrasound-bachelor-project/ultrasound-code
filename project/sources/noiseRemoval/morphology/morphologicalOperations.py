"""
Code for performing/plotting erosion, dilation, opening, and closing filters on images
"""

import numpy as np
from cv2 import erode, dilate

from general import loader, ustool
import matplotlib.pyplot as plt


def erosionFilter(resourceName: str, windowSize=3, iterations=1, plot=True):
    """
    Performs erosion on a given resource

    Args:
        resourceName: Which resource to perform filtering on
        windowSize:   Kernel/window size
        iterations:   The amount of times to perform erosion iteratively
        plot:         Whether or not to plot images

    Returns: Eroded image

    """
    # Load C-scan data
    cScan, startGate, endGate = loader.load("../../../resources/h5/" + resourceName + ".h5")

    # Get max amp image
    image = ustool.getImageByMaximum(cScan[startGate:endGate])

    # Perform erosion on the image
    kernel = np.ones((windowSize, windowSize), np.uint8)
    erodedImage = erode(image, kernel, iterations)

    # Plot original/eroded images
    if plot:
        plt.imshow(image, "gray")
        plt.title("Original image")
        plt.show()

        plt.imshow(erodedImage, "gray")
        plt.title("Eroded image")
        plt.show()

    return erodedImage


def dilationFilter(resourceName: str, windowSize=3, iterations=1, plot=True):
    """
    Performs dilation on a given resource

    Args:
        resourceName: Which resource to perform filtering on
        windowSize:   Kernel/window size
        iterations:   The amount of times to perform dilation iteratively
        plot:         Whether or not to plot images

    Returns: Dilated image

    """
    # Load C-scan data
    cScan, startGate, endGate = loader.load("../../../resources/h5/" + resourceName + ".h5")

    # Get max amp image
    image = ustool.getImageByMaximum(cScan[startGate:endGate])

    # Perform dilation on the image
    kernel = np.ones((windowSize, windowSize), np.uint8)
    dilatedImage = dilate(image, kernel, iterations)

    # Plot original/eroded images
    if plot:
        plt.imshow(image, "gray")
        plt.title("Original image")
        plt.show()

        plt.imshow(dilatedImage, "gray")
        plt.title("Dilated image")
        plt.show()

    return dilatedImage


def openingFilter(resourceName: str, windowSize=3, iterations=1, plot=True):
    """
    Runs opening filter on an image (dilation of an eroded image)

        resourceName: Which resource to perform filtering on
        windowSize:   Kernel/window size
        iterations:   The amount of times to perform dilation + erosion iteratively
        plot:         Whether or not to plot images

    Returns: Opening-filtered image

    """
    # Load C-scan data
    cScan, startGate, endGate = loader.load("../../../resources/h5/" + resourceName + ".h5")

    # Get max amp image
    image = ustool.getImageByMaximum(cScan[startGate:endGate])

    # Use opening filter on the image
    kernel = np.ones((windowSize, windowSize), np.uint8)
    openedImage = dilate(erode(image, kernel, iterations), kernel, iterations)

    # Plot original/opening filtered images
    if plot:
        plt.imshow(image, "gray")
        plt.title("Original image")
        plt.show()

        plt.imshow(openedImage, "gray")
        plt.title("Opening filtered image")
        plt.show()

    return openedImage


def closingFilter(resourceName: str, windowSize=3, iterations=1, plot=True):
    """
    Runs closing filter on an image (erosion of a dilated image)

        resourceName: Which resource to perform filtering on
        windowSize:   Kernel/window size
        iterations:   The amount of times to perform dilation + erosion iteratively
        plot:         Whether or not to plot images

    Returns: Closing-filtered image

    """
    # Load C-scan data
    cScan, startGate, endGate = loader.load("../../../resources/h5/" + resourceName + ".h5")

    # Get max amp image
    image = ustool.getImageByMaximum(cScan[startGate:endGate])

    # Use opening filter on the image
    kernel = np.ones((windowSize, windowSize), np.uint8)
    closedImage = erode(dilate(image, kernel, iterations), kernel, iterations)

    # Plot original/closing filtered images
    if plot:
        plt.imshow(image, "gray")
        plt.title("Original image")
        plt.show()

        plt.imshow(closedImage, "gray")
        plt.title("Closing filtered image")
        plt.show()

    return closedImage

# -*- coding: utf-8 -*-
"""
This file concatinates data files that the user
selects into a original/unmodified dataset.
"""

import pickle
import random
import sys
import os

from typing import BinaryIO


def clearScreen():
    """
    Void function to clear the screen
    """

    print("\n" * 100)
    os.system('clear')


def printMenu(dataDict):
    """
    Void function to display to the user
    """

    print("-" * 100)
    print("| KEY\t| SELECTED\t| FILEPATH")
    print("-" * 100)
    for key, (filep, sel) in dataDict.items():
        print("| " + str(key) + "\t| " + str("YES" if sel else "NO") + "\t\t| " + filep)
        print("-" * 100)


def updateSelections(dataDict, start, end):
    """
    Function to update the selected files to concatinate
    into a dataset of A-scans

    Parameters
    ----------
    dataDict : Dictionary of {key, tuple(filepath - string, selected - boolean)}
        The dictionary that controls the selections
    start : int
        The start of the slice
    end : int
        The end of the slice
    """

    selection = ""
    while selection != "C" or not len(
            [selected for filepath, selected in dataDict.values() if selected]) > 0:

        clearScreen()
        print("Loaded all found '.dta' files...\n")
        printMenu(dataDict)
        selection = input("Add/Remove files to concatenate into a dataset (" + str(start) + "-" + str(
            end - 1) + "), C = CONTINUE, Q = QUIT: ")

        try:
            selection = int(selection)
            if start <= selection < end:
                dataDict[selection] = (dataDict[selection][0], not dataDict[selection][1])

        except ValueError:
            selection = selection.upper()
            if selection == "Q":
                sys.exit(0)


dataDictionary = dict()

# Retrieve all the '.dta' files from the project folders,
# and store them in a dictionary with their filepath and 'used' variable.
# The key is just a count from when it was loaded (0 = first, 1 = next, etc... )
COUNTER = 0
for root, directories, files in os.walk("../"):
    file: str
    for file in files:
        if file.endswith(".dta"):
            dataDictionary[COUNTER] = (os.path.join(root, file), False)
            COUNTER += 1

# Let the user chose what data to be use in the dataset
updateSelections(dataDictionary, 0, len(dataDictionary))

# Make a new list with all the selected paths
selectedFilepaths = [filepath for filepath, selected in dataDictionary.values() if selected]

dataset = []

# Asks if this dataset should be saved to a file
selected = input("Save this to a dataset file (Y/n): ").upper()
if not selected == "N":

    # Load all the selected files, and save them all as a randomized dataset
    binaryFile: BinaryIO
    for filepath in selectedFilepaths:
        with open(filepath, 'rb') as binaryFile:
            data = []

            while True:
                try:
                    data = pickle.load(binaryFile)
                except EOFError:
                    break

            dataset.extend(data)

    # Shuffles the dataset before saving
    random.shuffle(dataset)

    # Saves the dataset
    path = input("Give a path to a folder (From working directory): ")
    filename = input("Give a filename (Without extension): ")
    with open(os.path.normpath(path + '/' + filename + '.dta'), 'wb') as output:
        pickle.dump(dataset, output, pickle.HIGHEST_PROTOCOL)

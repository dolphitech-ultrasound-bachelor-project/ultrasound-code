# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 00:23:41 2020

This file contains definitions of A-scan, C-scan and maximum amplitude sample classes.
The .dta files for this project each contain a list of objects of these classes.

@author: StigA
"""


class EntryAScan:
    """
    Class representing a single A-scan dataset entry
    """

    def __init__(self, name, data, damaged, position, gates):
        self.name = name
        self.data = data
        self.damaged = damaged
        self.position = position
        self.gates = gates

    def __str__(self):
        return self.name + str(self.damaged) + str(self.position)


class EntryCScan:
    """
    Class representing a .h5 ultrasound scan
    """

    def __init__(self, name, data, damaged, gates, txe):
        self.name = name
        self.data = data
        self.damaged = damaged
        self.gates = gates
        self.txe = txe

    def __str__(self):
        return self.name + str(self.damaged) + str(self.txe)


class EntryMaxAmp:
    """
    Class representing a .h5 ultrasound scan, but only with the maximum amplitude image, not full C-scan
    """

    def __init__(self, name, maxamp, damaged, txe):
        self.name = name
        self.maxamp = maxamp
        self.damaged = damaged
        self.txe = txe

    def __str__(self):
        return self.name + str(self.damaged) + str(self.txe)

"""
Contains functions for transforming images, as well as an augment-function.

@author: Franz Vrolijk
@author: Shubham Pachori (original noise function)
"""
import pickle
import copy
import numpy as np
import cv2
from dataset.trainingDtaCreator import EntryMaxAmp


def crop1(image):
    """
    Crops the image
    Args:
        image: image to be cropped

    Returns: cropped image

    """
    # Crop the image, then resize to preserve dimensions
    image = image[1:104, 3:100]
    image = cv2.resize(image, (128, 128))
    return image


def crop2(image):
    """
        Crops the image
        Args:
            image: image to be cropped

        Returns: cropped image

    """
    # Crop the image, then resize to preserve dimensions
    image = image[5:108, 2:104]
    image = cv2.resize(image, (128, 128))
    return image


def cropX(image):
    """
        Rotates the image
        Args:
            image: image to be cropped

        Returns: cropped image

    """
    center = tuple(np.array(image.shape[1::-1]) / 2)
    rotMat = cv2.getRotationMatrix2D(center, 8, 1.0)
    image = cv2.warpAffine(image, rotMat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    image = image[10:98, 10:98]
    image = cv2.resize(image, (128, 128))
    return image


def allTransformations(origEntry, dataset):
    """
    Very "manual" code for applying a very specific set of transformations to an image,
    and saving a copy for each transformation.
    Args:
        origEntry: the original entry / image
        dataset: the dataset to save each new entry / image to

    """
    entry = copy.deepcopy(origEntry)
    entry.maxamp = np.flip(entry.maxamp, 0)
    dataset.append(entry)

    entry = copy.deepcopy(origEntry)
    entry.maxamp = np.rot90(np.flip(entry.maxamp, 0))
    dataset.append(entry)

    entry = copy.deepcopy(origEntry)
    entry.maxamp = np.rot90(np.rot90(np.flip(entry.maxamp, 0)))
    dataset.append(entry)

    entry = copy.deepcopy(origEntry)
    entry.maxamp = np.rot90(np.rot90(np.rot90(np.flip(entry.maxamp, 0))))
    dataset.append(entry)

    entry = copy.deepcopy(origEntry)
    entry.maxamp = cropX(np.flip(entry.maxamp, 0))
    dataset.append(entry)

    entry = copy.deepcopy(origEntry)
    entry.maxamp = np.rot90(entry.maxamp)
    dataset.append(entry)

    entry = copy.deepcopy(origEntry)
    entry.maxamp = np.rot90(np.rot90(entry.maxamp))
    dataset.append(entry)

    entry = copy.deepcopy(origEntry)
    entry.maxamp = np.rot90(np.rot90(np.rot90(entry.maxamp)))
    dataset.append(entry)

    entry = copy.deepcopy(origEntry)
    entry.maxamp = cropX(entry.maxamp)
    dataset.append(entry)

    entry = copy.deepcopy(origEntry)
    entry.maxamp = cropX(crop1(entry.maxamp))
    dataset.append(entry)

    entry = copy.deepcopy(origEntry)
    entry.maxamp = cropX(crop2(entry.maxamp))
    dataset.append(entry)


def augment():
    """
    Function will open dataset-files (both training and testing), and
    add a number of new entries to them. The new entries
    will be copies of previous entries, but containing new
    data (metadata/other member data) will be the same as original file.
    New dataset will have "_augmented" appended to the name, and will
    be saved in the same folder as the original datasets.
    """
    # Load file
    with open('resources/dataset/Image dataset/dataset_train_image.dta', 'rb') as input1:
        dta = pickle.load(input1)
    # Make a deep copy of the file
    dataCopy = copy.deepcopy(dta)
    # If file loaded successfully
    if isinstance(dta, list):
        # Iterate on each sample
        for sample in dta:
            # If sample is of our class
            if isinstance(sample, EntryMaxAmp):
                # Run every transformation on this sample, and save them to our deep copy
                # The reason why we don't save to the original "dta" object is that we are iterating on this list
                # (Could also be refactored to use range-based for-loop)
                allTransformations(sample, dataCopy)
    # Save new dataset as dataset_augmented.dta
    with open('resources/dataset/Image dataset/dataset_train_image_augmented.dta', 'wb') as output:
        pickle.dump(dataCopy, output, pickle.HIGHEST_PROTOCOL)

    with open('resources/dataset/Image dataset/dataset_test_image.dta', 'rb') as input1:
        dta = pickle.load(input1)
    # Make a deep copy of the file
    dataCopy = copy.deepcopy(dta)
    # If file loaded successfully
    if isinstance(dta, list):
        # Iterate on each sample
        for sample in dta:
            # If sample is of our class
            if isinstance(sample, EntryMaxAmp):
                # Run every transformation on this sample, and save them to our deep copy
                # The reason why we don't save to the original "dta" object is that we are iterating on this list
                # (Could also be refactored to use range-based for-loop)
                allTransformations(sample, dataCopy)
    # Save new dataset as dataset_augmented.dta
    with open('resources/dataset/Image dataset/dataset_test_image_augmented.dta', 'wb') as output:
        pickle.dump(dataCopy, output, pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    augment()

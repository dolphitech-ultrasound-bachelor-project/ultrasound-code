# -*- coding: utf-8 -*-
"""
This file loads a default (IE. unmodified!) dataset
and modifies it into a downsampled version that can be used.
"""
import copy
import os
import pickle
from scipy.signal import resample


dataset = []

# First the original dataset file is loaded
filepath = input("Path to original dataset: ")
with open(filepath, 'rb') as file:
    while True:
        try:
            dataset = pickle.load(file)
        except EOFError:
            break

# Then we create a downsampled version
datasetDownsampled = copy.deepcopy(dataset)
for i, entry in enumerate(datasetDownsampled):
    datasetDownsampled[i].data = resample(entry.data, 128, axis=0)

print("Dataset has been downsampled...")

# And save it to file
path = input("Give a path to a folder (From working directory): ")
filename = input("Give a filename (Without extension): ")
with open(os.path.normpath(path + '/' + filename + '.dta'), 'wb') as output:
    pickle.dump(datasetDownsampled, output, pickle.HIGHEST_PROTOCOL)

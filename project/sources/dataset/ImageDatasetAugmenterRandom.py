"""
(OUTDATED)
File used for image augmentation. Newer augmenter uses a specific set of augmentations,
this file uses a random set of augmentations.

@author: Franz Vrolijk
@author: Shubham Pachori (original noise function)
"""
import pickle
import random as rn
import copy
import numpy as np
import cv2
from dataset.trainingDtaCreator import EntryMaxAmp
from dataset.ImageDatasetAugmenter import cropX, crop1, crop2


def randomTransform(image, imNum=0, save=False):
    """
    Modifies an image a random number of times, in a few different ways.
    Saves the images to "inflateresults" folder.
    Args:
        imNum: number of picture for file name
        save: Bool, if you want to save the file or just return
        image: Image to be transformed

    Returns: nothing

    """

    # For rNum iterations, perform a random transformation
    rNum = rn.randint(1, 8)
    # Make sure rotate is only used once
    used = False

    for _ in range(0, rNum):
        rMethod = rn.randint(0, 4)
        if rMethod == 0:
            # Flip the image
            image = np.flip(image)
        elif rMethod == 1:
            # Rotate the image 90 degrees
            image = np.rot90(image)
        elif rMethod == 2:
            # Crop the image, then resize to preserve dimensions
            image = crop1(image)
        elif rMethod == 3:
            # Crop the image, then resize to preserve dimensions
            image = crop2(image)
        elif rMethod == 4:
            # Rotate the image, then resize to crop edges and preserve dimensions (max 1 use)
            if not used:
                used = True
                image = cropX(image)
            else:
                # Just rotate image if the method already has been used on image
                image = np.rot90(image)

        # if the user has enabled saving
        if save:
            # 50% chance to save at any step, and will always save at the last iteration
            if rn.randint(0, 1) == 1 or _ == rNum - 1:
                # Save the result
                cv2.imwrite("../resources/inflateresults/mod" + str(imNum) + ".png", image)
                imNum += 1
    # Return final image if you want to use this function in your code

    return imNum, image


def noisy(noiseType, image):
    """
    Modified version of code found here:
    https://stackoverflow.com/questions/22937589/how-to-add-noise-gaussian-salt-and-pepper-etc-to-image-in-python-with-opencv

    Args:
        noiseType: type of noise you want applied
        image: image to apply noise to

    Returns: image with noise

    @Author (original): Shubham Pachori
    Date created: Jun 3 '15

    @Author (modifications): Franz Vrolijk
    Date modified: Jan 30 '20
    """
    if noiseType == "gauss":
        row, col = image.shape
        mean = 0
        var = 0.01
        sigma = var ** 0.5
        gauss = np.random.normal(mean, sigma, (row, col))
        gauss = gauss.reshape(row, col)
        noisyImage = image + image * gauss
        return noisyImage
    if noiseType == "s&p":
        spRatio = 0.5
        amount = 0.01
        noisyImage = np.copy(image)
        # Salt mode
        numSalt = np.ceil(amount * image.size * spRatio)
        coords = [np.random.randint(0, i - 1, int(numSalt)) for i in image.shape]
        noisyImage[tuple(coords)] = 1

        # Pepper mode
        numPepper = np.ceil(amount * image.size * (1. - spRatio))
        coords = [np.random.randint(0, i - 1, int(numPepper)) for i in image.shape]
        noisyImage[tuple(coords)] = 0
        return noisyImage
    if noiseType == "speckle":
        row, col = image.shape
        gauss = np.random.randn(row, col)
        gauss = gauss.reshape(row, col)
        noisyImage = image + (image * gauss * 0.1)
        return noisyImage
    return None


def augment(fileName, num):
    """
    "Main" function.
    Opens the specified file, applies a maximum of "num" augmentations
    to each file in the dataset.
    """
    with open('../resources/' + fileName, 'rb') as input1:
        dta = pickle.load(input1)

    dataCopy = copy.deepcopy(dta)

    if isinstance(dta, list):
        for data in dta:
            if isinstance(data, EntryMaxAmp):
                for _ in range(0, rn.randint(1, num)):
                    entry = copy.deepcopy(data)
                    __, entry.maxamp = randomTransform(entry.maxamp)
                    dataCopy.append(entry)

    with open('../resources/dataset/dataset_augmented.dta', 'wb') as output:
        pickle.dump(dataCopy, output, pickle.HIGHEST_PROTOCOL)
    return dataCopy

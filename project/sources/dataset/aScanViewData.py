# -*- coding: utf-8 -*-
"""
Created on Sat Feb 22 14:34:35 2020

@author: StigA
"""
import matplotlib.pyplot as plt
import numpy as np
import pickle
import os

# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

# Retrieve all the h5 files
from typing.io import BinaryIO

filepaths = []
for root, directories, files in os.walk("./resources/dataset"):
    for file in files:
        if file.endswith(".dta"):
            filepaths.append(os.path.normpath(os.path.join(root, file)))

print("Files found in the '//project/resources/dataset' folder...")
for i, path in enumerate(filepaths):
    print("[", i, "]: ", path)

dataset = []
binFile: BinaryIO

with open(filepaths[int(input("Choose number: "))], 'rb') as binFile:
    while True:
        try:
            dataset = pickle.load(binFile)
        except EOFError:
            break

mask = np.zeros((128, 128), dtype=np.uint8)
for entry in dataset:
    if entry.damaged:
        mask[entry.position[0], entry.position[1]] = 255

plt.figure()
plt.imshow(mask, "gray")

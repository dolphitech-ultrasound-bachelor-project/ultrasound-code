# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 16:31:38 2020

@author: Stig
"""
from typing import List

import matplotlib.pyplot as plt
from scipy.ndimage import median_filter
from scipy.signal import find_peaks
from scipy.signal import peak_widths
import numpy as np
import pickle
import cv2
import os

from sources.dataset.datasetClasses import EntryAScan
import sources.general.loader as loader
import sources.general.ustool as ustool


def __clear():
    """
    Function to 'clear' the screen
    in a fake manner
    """

    print("\n" * 50)
    os.system('clear')


def __chooseFilePath():
    """
    Function that lets a user choose what
    file found in the project to create
    A-scan data with

    Returns
    -------
    path : string
        Path to the chosen file
    """

    filepaths = []
    for root, directories, files in os.walk("./resources/h5"):
        for file in files:
            if file.endswith(".h5"):
                filepaths.append(os.path.normpath(os.path.join(root, file)))

    for i, path in enumerate(filepaths):
        print("[", i, "]: ", path)

    return filepaths[int(input("Choose file: "))]


def __chooseAutomaticScanSlices(scan, gates):
    """
    Function that retrieves interest points
    from the response function (1%), and
    chooses around the peaks of said function.
    The user can then pick what slices are to be kept.

    Arguments
    -------
    scan : np.ndarray
        The scan to be used
    gates : Tuple of two integers
        The start gate and end gate

    Returns
    -------
    chosenScanSlices : list
        List of chosen scan slices, that was picked by
        the user, from the automaticly generated list
    """

    __clear()

    # Retrieve the response function with sigmoid of 1% length, and its peaks /w widths
    responseFunction = ustool.getResponseFunction(scan, 0.01)
    peaks = find_peaks(responseFunction)[0]
    widths = peak_widths(responseFunction, peaks)[0]

    # Retrieve all the slicing information around each peak
    indices = []
    for i, peak in enumerate(peaks):
        start = int(peak - widths[i])
        end = int(peak + widths[i])
        if start < 0:
            start = 0
        if end > responseFunction.size:
            end = responseFunction.size
        indices.append([(start, end), peak, responseFunction[peak]])

    # Sorted so that largest peaks are first (Most likely the interesting slices)
    indices = sorted(indices, key=lambda x: x[2], reverse=True)

    # Retrieve the slices
    scanSlices = []
    for (start, end), peak, height in indices:
        scanSlices.append((scan[start:end], (start, end)))

    # Display automaticly found slices, compared to the original images
    for i, (scanSlice, (start, end)) in enumerate(scanSlices):
        figure, axes = plt.subplots(1, 3, figsize=(8, 8))
        axes[0].imshow(ustool.getImageByMaximum(smoothScan), 'gray')
        axes[0].text(64, -16, 'Full Maximum\nAmplitude Image', ha='center', va='center')
        axes[1].imshow(ustool.getImageByMaximum(smoothScan[gates[0]:gates[1]]), 'gray')
        axes[1].text(64, -16, 'Gated Maximum\nAmplitude Image', ha='center', va='center')
        axes[2].imshow(ustool.getImageByMaximum(scanSlice), 'gray')
        axes[2].text(64, -16, 'NUMBER [{0}]\n({1}-{2})'.format(i, start, end), ha='center', va='center')
        plt.setp(axes, xticks=[], yticks=[])
        plt.show(block=False)

    # Let the user choose
    chosenIndexes = input(
            'Enter the numbers (separated by space) to bring generated'
            + "\nslice suggestions further, or a single '-1' for none: "
    ).strip().split()

    # Create a new list of the choices
    chosenScanSlices = [scanSlices[int(number)] for number in chosenIndexes]

    # If we did not want any, return an empty list
    if -1 in [int(number) for number in chosenIndexes]:
        chosenScanSlices.clear()

    return chosenScanSlices


def __addManualSlicing(scan):
    """
    Function that lets a user manually slice
    the input scan by setting range values
    in a given format

    Arguments
    -------
    scan : np.ndarray
        The scan to be used

    Returns
    -------
    manualScanSlices : list
        List of scan slices that was generated
        from manually selecting values
    """

    # Print help information
    print(
        "\nWrite the 'START:END' format, separated by '/', to create slices"
        + "\nExample: START:END/START:END/START:END"
    )

    # Generate slicing numbers
    __input = input("Input: ").strip().split('/')
    __input = [entry.split(':') for entry in __input]
    __input = [[int(start), int(end)] for [start, end] in __input]

    # Generate the slices
    manualScanSlices = []
    for (start, end) in __input:
        manualScanSlices.append([scan[start:end], (start, end)])

    return manualScanSlices


def __chooseManualScanSlices(scan, gates):
    """
    Function to choose what scan slices to use

    Arguments
    -------
    scan : np.ndarray
        The scan to be used
    gates : Tuple of two integers
        The start gate and end gate

    Returns
    -------
    chosenScanSlices : list
        The list of chosen scan slices
    """

    __clear()

    # Retrieve the response function with sigmoid of 1% length
    responseFunction = ustool.getResponseFunction(smoothScan, 0.01)

    # Create a empty list for slices that the user can add to
    chosenScanSlices = []

    # Looping menu program to choose slices
    __printManualSliceState(responseFunction, chosenScanSlices, scan, gates)
    __input = input('Menu choice: ').upper()

    while __input != 'C':
        {
            'A': lambda: chosenScanSlices.extend(__addManualSlicing(scan)),
            'Z': lambda: chosenScanSlices.pop(),
            'R': lambda: chosenScanSlices.pop(int(input("Number: "))),
            'RA': lambda: chosenScanSlices.clear()
        }.get(__input, lambda: None)()

        __printManualSliceState(responseFunction, chosenScanSlices, scan, gates)
        __input = input('Menu choice: ').upper()

    if len(chosenScanSlices) > 0:

        # Let the user choose
        chosenIndexes = input(
                'Enter the numbers (separated by space) to bring slice'
                + "\nsuggestions further, or a single '-1' for none: "
        ).strip().split()

        # Create a new list of the choices
        chosenScanSlices = [chosenScanSlices[int(number)] for number in chosenIndexes]

        # If we did not want any, return an empty list
        if -1 in [int(number) for number in chosenIndexes]:
            chosenScanSlices.clear()

    return chosenScanSlices


def __printManualSliceState(responseFunction, chosenSlices, scan, gates):
    """
    Function to print the manual slicing information

    Arguments
    -------
    responseFunction : np.array
        A 1D array representing the responseFunction
    chosenSlices : list
        List of the slices
    scan : np.ndarray
        The scan to be used
    gates : Tuple of two integers
        The start gate and end gate
    """

    __clear()

    # Plot the data state of the chosen slices
    if len(chosenSlices) > 0:

        numberOfColumns = 4
        numberOfRows = int(np.ceil(len(chosenSlices)/numberOfColumns))
        figure, axes = plt.subplots(numberOfRows, numberOfColumns, figsize=(4, 4))
        figure.suptitle('Chosen slices:')

        for i in range(numberOfRows*numberOfColumns):
            column = int(i % numberOfColumns)
            row = int(np.floor(i/numberOfColumns))
            if numberOfRows > 1:
                axes[row, column].axis('off')
            else:
                axes[i].axis('off')

        for i in range(len(chosenSlices)):
            column = int(i % numberOfColumns)
            row = int(np.floor(i/numberOfColumns))
            if numberOfRows > 1:
                axes[row, column].text(
                        64,
                        -32,
                        '[{0}]\n({1}-{2})'.format(i, chosenSlices[i][1][0], chosenSlices[i][1][1]),
                        va='center',
                        ha='center'
                )
                axes[row, column].imshow(ustool.getImageByMaximum(chosenSlices[i][0]), 'gray')
                axes[row, column].axis('on')
            else:
                axes[i].text(
                        64,
                        -32,
                        '[{0}]\n({1}-{2})'.format(i, chosenSlices[i][1][0], chosenSlices[i][1][1]),
                        va='center',
                        ha='center'
                )
                axes[i].imshow(ustool.getImageByMaximum(chosenSlices[i][0]), 'gray')
                axes[i].axis('on')

        plt.setp(axes, xticks=[], yticks=[])
        plt.show(block=False)

    # Plot the data state of the responseFunction
    plt.figure(figsize=(10, 4))
    plt.plot(responseFunction)
    plt.xticks(range(0, responseFunction.size, 25), fontsize=8)
    for i, (scanSlice, (start, end)) in enumerate(chosenSlices):
        plt.axvline(x=start, color='red')
        plt.axvline(x=end, color='red')
    plt.axvline(x=gates[0], color='yellow')
    plt.axvline(x=gates[1], color='yellow')
    plt.show(block=False)

    # Plot the original images
    figure, axes = plt.subplots(1, 2, figsize=(8, 8))
    axes[0].imshow(ustool.getImageByMaximum(scan), 'gray')
    axes[0].text(64, -24, 'Original Maximum\nAmplitude Image', va='center', ha='center')
    axes[1].imshow(ustool.getImageByMaximum(scan[gates[0]:gates[1]]), 'gray')
    axes[1].text(64, -24, 'Gated Maximum\nAmplitude Image', va='center', ha='center')
    plt.show(block=False)

    print(
        '-'*50
        + '\n \tSlicing Menu\t \n'
        + '-'*50
        + "\n(A) - Add a slicing"
        + "\n(Z) - Undo last slicing"
        + "\n(R) - Remove a certain slicing"
        + "\n(RA) - Remove all slicing"
        + "\n(C) - Continue and choose slices"
    )


def __addManualThreshold(chosenSlice):
    """
    Function to let a user choose a manual threshold
    on a single picked slice

    Arguments
    -------
    chosenSlice : list containing (1) the slice (2) tuple of start and end
        A single chosen slice to be
        manually thresholded

    Returns
    -------
    thresholded : list
        List containing a single thresholded image
    """

    __clear()

    # Starting information
    maximumAmplitudeImage = ustool.getImageByMaximum(chosenSlice[0])
    thresholded = np.zeros((128, 128), np.uint8)

    # Calculate the histogram and bins
    histogram, bins = np.histogram(maximumAmplitudeImage.ravel(), 256, [0, 256])

    # Plot the information
    figure, axes = plt.subplots(1, 3, figsize=(12, 4))
    axes[0].imshow(maximumAmplitudeImage, 'gray')
    axes[0].text(64, -16, 'Full Maximum\nAmplitude Image', ha='center', va='center')
    axes[1].imshow(thresholded, 'gray')
    axes[1].text(64, -16, 'Thresholded Maximum\nAmplitude Image', ha='center', va='center')
    axes[2].plot(histogram)
    axes[2].text(64, -16, 'Histogram', ha='center', va='center')
    plt.show(block=False)

    while True:

        # Process inputs
        __input = input("Select threshold value, or 'C' to confirm: ")

        try:
            threshold = int(__input)
            if 0 <= threshold <= 255:
                thresholded[maximumAmplitudeImage <= threshold] = 0
                thresholded[maximumAmplitudeImage > threshold] = 255

        except ValueError:
            __input = __input.upper()
            if __input in ('C'):
                return [thresholded]

        # Calculate the histogram and bins
        histogram, bins = np.histogram(maximumAmplitudeImage.ravel(), 256, [0, 256])

        # Plot the information
        figure, axes = plt.subplots(1, 3, figsize=(12, 4))
        axes[0].imshow(maximumAmplitudeImage, 'gray')
        axes[0].text(64, -16, 'Full Maximum\nAmplitude Image', ha='center', va='center')
        axes[1].imshow(thresholded, 'gray')
        axes[1].text(64, -16, 'Thresholded Maximum\nAmplitude Image', ha='center', va='center')
        axes[2].plot(histogram)
        axes[2].text(64, -16, 'Histogram', ha='center', va='center')
        plt.axvline(x=threshold, color='red')
        plt.setp(axes, xticks=[], yticks=[])
        plt.show(block=False)


def __addOtsusThreshold(chosenSlices, scan, gates):
    """
    Function to let a user choose from a list of
    slices which has been thresholded by Otsus method

    Arguments
    -------
    chosenSlices : list
        List of the slices
    scan : np.ndarray
        The scan to be used
    gates : Tuple of two integers
        The start gate and end gate

    Returns
    -------
    chosenThresholds : list
        The list of chosen thresholds from using Otsus
    """

    __clear()

    # Retrieve the thresholds
    thresholds = []
    for scanSlice, (start, end) in chosenSlices:
        maximumAmplitudeImage = ustool.getImageByMaximum(scanSlice)
        thresholds.append(cv2.threshold(maximumAmplitudeImage, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1])

    # Display all thresholds, compared to the original images
    for i, threshold in enumerate(thresholds):
        figure, axes = plt.subplots(1, 3, figsize=(8, 8))
        axes[0].imshow(ustool.getImageByMaximum(scan), 'gray')
        axes[0].text(64, -16, 'Full Maximum\nAmplitude Image', ha='center', va='center')
        axes[1].imshow(ustool.getImageByMaximum(scan[gates[0]:gates[1]]), 'gray')
        axes[1].text(64, -16, 'Gated Maximum\nAmplitude Image', ha='center', va='center')
        axes[2].imshow(threshold, 'gray')
        axes[2].text(64, -16, 'NUMBER [{0}]'.format(i), ha='center', va='center')
        plt.setp(axes, xticks=[], yticks=[])
        plt.show(block=False)

    # Let the user choose
    chosenIndexes = input(
            'Enter the numbers (separated by space) to bring generated'
            + "\nthresholds further, or a single '-1' for none: "
    ).strip().split()

    # Create a new list of the choices
    chosenThresholds = [thresholds[int(number)] for number in chosenIndexes]

    # If we did not want any, return an empty list
    if -1 in [int(number) for number in chosenIndexes]:
        chosenThresholds.clear()

    return chosenThresholds


def __chooseThresholds(mask, scan, gates, chosenSlices):
    """
    Function to let a user choose thresholds
    from a given list of slices

    Arguments
    -------
    mask : np.ndarray
        The segmentation mask
    scan : np.ndarray
        The scan to be used
    gates : Tuple of two integers
        The start gate and end gate
    chosenSlices : list
        List of the slices

    Returns
    -------
    chosenThresholds : list
        The list of chosen thresholds
    """

    # Create a empty list for slices that the user can add to
    chosenThresholds = []

    # Looping menu program to choose slices
    __printThresholdState(mask, scan, gates, chosenSlices, chosenThresholds)
    __input = input('Threshold choice: ').upper()

    while __input != 'C':
        {
            'M': lambda: chosenThresholds.extend(__addManualThreshold(chosenSlices[int(input("Slice number: "))])),
            'O': lambda: chosenThresholds.extend(__addOtsusThreshold(chosenSlices, scan, gates)),
            'Z': lambda: chosenThresholds.pop(),
            'R': lambda: chosenThresholds.pop(int(input("Number: "))),
            'RA': lambda: chosenThresholds.clear()
        }.get(__input, lambda: None)()

        __printThresholdState(mask, scan, gates, chosenSlices, chosenThresholds)
        __input = input('Threshold choice: ').upper()

    if len(chosenThresholds) > 0:

        # Let the user choose
        chosenIndexes = input(
                'Enter the numbers (separated by space) to bring thresholds'
                + "\nsuggestions further, or a single '-1' for none: "
        ).strip().split()

        # Create a new list of the choices
        chosenThresholds = [chosenThresholds[int(number)] for number in chosenIndexes]

        # If we did not want any, return an empty list
        if -1 in [int(number) for number in chosenIndexes] or len(chosenThresholds) == 0:
            chosenThresholds.clear()

    return chosenThresholds


def __printThresholdState(mask, scan, gates, chosenSlices, chosenThresholds):
    """
    Function to print threshold menu

    Arguments
    -------
    mask : np.ndarray
        The segmentation mask
    scan : np.ndarray
        The scan to be used
    gates : Tuple of two integers
        The start gate and end gate
    chosenSlices : list
        List of the slices
    chosenThresholds : list
        List of the thresholds
    """

    __printMainInformation(mask, chosenThresholds, chosenSlices, scan, gates)

    print(
        '-'*50
        + '\n \tThresholding Menu\t \n'
        + '-'*50
        + "\n(M) - Add a manual threshold"
        + "\n(O) - Add a otsus threshold"
        + "\n(Z) - Undo last thresholding"
        + "\n(R) - Remove a certain thresholding"
        + "\n(RA) - Remove all thresholding"
        + "\n(C) - Continue and choose thresholded images"
    )


def __printMainState(mask, scan, gates, chosenSlices, chosenThresholds):
    """
    Function to print a main menu

    Arguments
    -------
    mask : np.ndarray
        The segmentation mask
    scan : np.ndarray
        The scan to be used
    gates : Tuple of two integers
        The start gate and end gate
    chosenSlices : list
        List of the slices
    chosenThresholds : list
        List of the thresholds
    """

    __printMainInformation(mask, chosenThresholds, chosenSlices, scan, gates)

    print(
        '-'*50
        + '\n \tMain Menu\t \n'
        + '-'*50
        + "\n(A) - Automatic slice generation"
        + "\n(M) - Manual slice generation"
        + "\n(T) - Threshold chosen slices"
        + "\n(RC) - Remove slices"
        + "\n(RT) - Remove thresholds"
        + "\n(C) - Continue with the chosen mask"
    )


def __printMainInformation(mask, chosenThresholds, chosenSlices, scan, gates):
    """
    Function to print important information in a visible manner

    Arguments
    -------
    mask : np.ndarray
        The segmentation mask
    chosenThresholds : list
        List of the thresholds
    chosenSlices : list
        List of the slices
    scan : np.ndarray
        The scan to be used
    gates : Tuple of two integers
        The start gate and end gate
    """

    __clear()

    if len(chosenSlices) > 0:

        numberOfColumns = 4
        numberOfRows = int(np.ceil(len(chosenSlices)/numberOfColumns))
        figure, axes = plt.subplots(numberOfRows, numberOfColumns, figsize=(4, 4))
        figure.suptitle('Chosen slices:')

        for i in range(numberOfRows*numberOfColumns):
            column = int(i % numberOfColumns)
            row = int(np.floor(i/numberOfColumns))
            if numberOfRows > 1:
                axes[row, column].axis('off')
            else:
                axes[i].axis('off')

        for i in range(len(chosenSlices)):
            column = int(i % numberOfColumns)
            row = int(np.floor(i/numberOfColumns))
            if numberOfRows > 1:
                axes[row, column].text(
                        64,
                        -32,
                        '[{0}]\n({1}-{2})'.format(i, chosenSlices[i][1][0], chosenSlices[i][1][1]),
                        va='center',
                        ha='center'
                )
                axes[row, column].imshow(ustool.getImageByMaximum(chosenSlices[i][0]), 'gray')
                axes[row, column].axis('on')
            else:
                axes[i].text(
                        64,
                        -32,
                        '[{0}]\n({1}-{2})'.format(i, chosenSlices[i][1][0], chosenSlices[i][1][1]),
                        va='center',
                        ha='center'
                )
                axes[i].imshow(ustool.getImageByMaximum(chosenSlices[i][0]), 'gray')
                axes[i].axis('on')

        plt.setp(axes, xticks=[], yticks=[])
        plt.show(block=False)

    if len(chosenThresholds) > 0:

        numberOfColumns = 4
        numberOfRows = int(np.ceil(len(chosenThresholds)/numberOfColumns))
        figure, axes = plt.subplots(numberOfRows, numberOfColumns, figsize=(4, 4))
        figure.suptitle('Chosen thresholds:')

        for i in range(numberOfRows*numberOfColumns):
            column = int(i % numberOfColumns)
            row = int(np.floor(i/numberOfColumns))
            if numberOfRows > 1:
                axes[row, column].axis('off')
            else:
                axes[i].axis('off')

        for i in range(len(chosenThresholds)):
            column = int(i % numberOfColumns)
            row = int(np.floor(i/numberOfColumns))
            if numberOfRows > 1:
                axes[row, column].text(64, -32, '[{}]'.format(i), va='center', ha='center')
                axes[row, column].imshow(chosenThresholds[i], 'gray')
                axes[row, column].axis('on')
            else:
                axes[i].text(64, -32, '[{}]'.format(i), va='center', ha='center')
                axes[i].imshow(chosenThresholds[i], 'gray')
                axes[i].axis('on')

        plt.setp(axes, xticks=[], yticks=[])
        plt.show(block=False)

    # Retrieve the original and gated image
    maximumAmplitudeImage = cv2.cvtColor(ustool.getImageByMaximum(scan), cv2.COLOR_GRAY2RGB)
    gatedMaxmimumAmplitudeImage = cv2.cvtColor(ustool.getImageByMaximum(scan[gates[0]:gates[1]]), cv2.COLOR_GRAY2RGB)

    # Create a yellow mask overlay to the images
    yellow = np.zeros((128, 128, 3), np.uint8)
    yellow[:, :] = (255, 255, 0)
    yellowMask = cv2.bitwise_and(yellow, yellow, mask=mask)
    cv2.addWeighted(yellowMask, 0.50, maximumAmplitudeImage, 1, 0, maximumAmplitudeImage)
    cv2.addWeighted(yellowMask, 0.50, gatedMaxmimumAmplitudeImage, 1, 0, gatedMaxmimumAmplitudeImage)

    figure, axes = plt.subplots(1, 3, figsize=(8, 8))
    axes[0].imshow(maximumAmplitudeImage, 'gray')
    axes[0].text(64, -24, 'Original Maximum\nAmplitude Image', va='center', ha='center')
    axes[1].imshow(gatedMaxmimumAmplitudeImage, 'gray')
    axes[1].text(64, -24, 'Gated Maximum\nAmplitude Image', va='center', ha='center')
    axes[2].imshow(mask, 'gray')
    axes[2].text(64, -16, 'Current Mask', va='center', ha='center')
    plt.show(block=False)


# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

# Choose the file to load
path = __chooseFilePath()

# Retrieve the scan and smooth it
originalScan, startGate, endGate = loader.load(path)
smoothScan = median_filter(originalScan, (3, 3, 3), mode="constant", cval=0.0)

# Create slice selection  and threshold selection which the user can fill,
# also creat an empty mask which will be filled as the user uses the program
mask = np.zeros((128, 128), dtype=np.uint8)
chosenSlices: List = []
chosenThresholds: List = []

# Looping menu program to create a mask for damage
__printMainState(mask, originalScan, (startGate, endGate), chosenSlices, chosenThresholds)
__input = input('Menu choice: ').upper()

while __input != 'C':
    {
        'A': lambda: chosenSlices.extend(__chooseAutomaticScanSlices(smoothScan, (startGate, endGate))),
        'M': lambda: chosenSlices.extend(__chooseManualScanSlices(smoothScan, (startGate, endGate))),
        'T': lambda: chosenThresholds.extend(__chooseThresholds(mask, smoothScan, (startGate, endGate), chosenSlices)),
        'RC': lambda: chosenSlices.pop(int(input("Write number of slice: "))),
        'RT': lambda: chosenThresholds.pop(int(input("Write number of threshold: ")))
    }.get(__input, lambda: None)()

    # Clear the mask from previous
    mask = np.zeros((128, 128), dtype=np.uint8)

    # Update the mask to be a merge of the chosen thresholds
    for threshold in chosenThresholds:
        mask[threshold == 255] = 255

    __printMainState(mask, originalScan, (startGate, endGate), chosenSlices, chosenThresholds)
    __input = input('Menu choice: ').upper()

# The user is done so we start the saving process
if not input("Save this to file (Y/n): ").upper() == "N":

    damagedIndexes = np.where(mask > 0)
    damagedIndexes = list(zip(damagedIndexes[0], damagedIndexes[1]))
    notDamagedIndexes = np.where(mask == 0)
    notDamagedIndexes = list(zip(notDamagedIndexes[0], notDamagedIndexes[1]))

    data = []

    for y, x in damagedIndexes:
        aScan = originalScan[:, y, x]
        data.append(EntryAScan(os.path.basename(path), aScan, True, (y, x), (startGate, endGate)))

    for y, x in notDamagedIndexes:
        aScan = originalScan[:, y, x]
        data.append(EntryAScan(os.path.basename(path), aScan, False, (y, x), (startGate, endGate)))

    path = input("Give a path to a folder (From working directory): ")
    filename = input("Give a filename (Without extension): ")
    with open(os.path.normpath(path + "/" + filename + '.dta'), 'wb') as output:
        pickle.dump(data, output, pickle.HIGHEST_PROTOCOL)

"""
Module for creating a dataset (.dta file) from a number of h5 files
Creates two sets, one with full C-scan and one with only maxamp-image
This is for the training dataset without downsampling

"""
import pickle
import numpy as np
from scipy.signal import medfilt2d
from sources.general import loader
from sources.general.ustool import getImageByMaximum
from sources.dataset.datasetClasses import EntryCScan, EntryMaxAmp


def makeDataset():
    """
    Creates two datasets. Full and maxamp.
    """
    # A list of all our samples
    # Each inner list contains  [name, damage, tx_elements]
    entries = [
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact", True, 8],
        ["airbus1_4", True, 8],
        ["airbus2_5", True, 2],
        ["space_d2_1", True, 16],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact2", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact5", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact7", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact9", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact12", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact14", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Small_Impact2", True, 4],
        ["airbus1_1", True, 8],
        ["airbus2_3", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact3", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact8", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact10", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact13", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact15", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact4", True, 4],
        ["TRM_CI5.00MHz_CFRP_Impact", True, 8],
        ["123_5", False, 8],
        ["airbus1_5", False, 8],
        ["airbus2_4", False, 4],
        ["toyota_0718_no7_1", False, 8],
        ["toyota_0718_no6_1", False, 16],
        ["space_d2_3", False, 4],
        ["123_4", False, 4],
        ["dacon_2", False, 16],
        ["mono1_1", False, 8],
        ["maieen1_1", False, 16],
    ]
    # Make two empty datasets, one for the full C-scans and one for only images
    datasetCScan = []
    datasetMaxAmp = []

    # For each sample
    for entry in entries:
        # Load the data from .h5 file using loader
        data, startGate, endGate = loader.load("resources/h5/" + entry[0] + ".h5")

        # Crop with gates
        data = data[startGate: endGate]
        data = data.astype(np.float)

        # For each time step layer
        for i in range(len(data[:, 0, 0])):
            # 2D-median filter that layer
            data[i, :, :] = medfilt2d(data[i, :, :])

        # Make two objects from this data, one with the full C-scan and one with only the image
        entryCScan = EntryCScan(entry[0], data, entry[1], (startGate, endGate), entry[2])
        entryMaxAmp = EntryMaxAmp(entry[0], getImageByMaximum(data), entry[1], entry[2])

        # Append each object to its corresponding dataset
        datasetCScan.append(entryCScan)
        datasetMaxAmp.append(entryMaxAmp)

    # Save both datasets to file
    with open('resources/dataset/C-scan dataset/dataset_train_cscan.dta', 'wb') as output:
        pickle.dump(datasetCScan, output, pickle.HIGHEST_PROTOCOL)

    with open('resources/dataset/Image dataset/dataset_train_image.dta', 'wb') as output:
        pickle.dump(datasetMaxAmp, output, pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    makeDataset()

"""
Module for creating a dataset (.dta file) from a number of h5 files
Creates two sets, one with full C-scan and one with only maxamp-image
This file is for the testing set.
"""
import pickle
import cv2
import numpy as np
from scipy.signal import medfilt2d
from sources.general import loader
from sources.general.ustool import getImageByMaximum
from sources.dataset.datasetClasses import EntryCScan, EntryMaxAmp


def makeDataset():
    """
    Creates two datasets. Full and maxamp.
    """
    # A list of all our samples
    # Each inner list contains  [name, damage, tx_elements]
    entries = [
        ["TRM_AF_3.5MHz_CFRP_5mm_Small_Impact", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact11", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact16", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Impact6", True, 4],
        ["TRM_AF_3.5MHz_CFRP_5mm_Good", False, 4],
        ["airbus2_1", True, 16],
        ["dacon_1", True, 16],
        ["space_d2_2", True, 16],
        ["airbus2_2", True, 16],
        ["123_6", False, 8],
        ["airbus1_6", False, 8],
        ["toyota_0718_no7_2", False, 2],
        ["boeing1_1", False, 16],
        ["sam1_1", False, 8],
    ]
    # Make two empty datasets, one for the full C-scans and one for only images
    datasetCScan = []
    datasetMaxAmp = []

    # For each sample
    for entry in entries:
        # Load the data from .h5 file using loader
        data, startGate, endGate = loader.load("resources/h5/" + entry[0] + ".h5")

        # Crop with gates
        data = data[startGate: endGate]

        # Downsample
        reshapedCscan = np.zeros((64, 128, 128))
        # For each row or column
        for i in range(128):
            # Reshape each layer individually
            reshapedCscan[:, :, i] = cv2.resize(data[:, :, i], (128, 64))

        for i in range(64):
            reshapedCscan[i, :, :] = medfilt2d(reshapedCscan[i, :, :])

        # Make two objects from this data, one with the full C-scan and one with only the image
        entryCScan = EntryCScan(entry[0], reshapedCscan, entry[1], (startGate, endGate), entry[2])
        entryMaxAmp = EntryMaxAmp(entry[0], getImageByMaximum(reshapedCscan), entry[1], entry[2])

        # Append each object to its corresponding dataset
        datasetCScan.append(entryCScan)
        datasetMaxAmp.append(entryMaxAmp)

    # Save both datasets to file
    with open('resources/dataset/C-scan dataset/dataset_test_cscan_downsampled.dta', 'wb') as output:
        pickle.dump(datasetCScan, output, pickle.HIGHEST_PROTOCOL)

    with open('resources/dataset/Image dataset/dataset_test_image_downsampled.dta', 'wb') as output:
        pickle.dump(datasetMaxAmp, output, pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    makeDataset()

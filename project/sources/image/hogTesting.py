"""
Neural network for classifying two-channel images, containing
both grayscale and hog representations of the data
"""
import os
import pickle

import numpy as np
import tensorflow as tf
from skimage.feature import hog
from sklearn.utils import shuffle


def twoDimFcnn(dataset, labels):
    """
    Definition of the neural network
    :param dataset: data to train on
    :param labels: labels to train with
    :return: history of training, model
    """
    model = tf.keras.models.Sequential()

    model.add(
        tf.keras.layers.Conv2D(64, (3, 3), input_shape=(126, 126, 2)))
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(32, (2, 2), activation='relu'))
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(32, (2, 2), activation='relu'))

    model.add(tf.keras.layers.Flatten())

    model.add(tf.keras.layers.Dense(2048, activation='relu'))
    model.add(tf.keras.layers.Dense(1024, activation='relu'))
    model.add(tf.keras.layers.Dense(512, activation='relu'))
    model.add(tf.keras.layers.Dense(128, activation='sigmoid'))
    model.add(tf.keras.layers.Dense(64, activation='sigmoid'))
    model.add(tf.keras.layers.Dense(16, activation='sigmoid'))
    model.add(tf.keras.layers.Dense(1))

    model.compile(loss="binary_crossentropy",
                  optimizer="adam",
                  metrics=['accuracy'])
    model.summary()

    history = model.fit(dataset, labels, batch_size=16, validation_split=0.2, epochs=10)

    modelPath = "resources/model/hogCNNmodel"
    if not os.path.exists(modelPath):
        os.makedirs(modelPath)
        print("Created model folder in: ", modelPath)

    model.save(modelPath)

    return model, history


def main():
    """
    Loads training data, makes two-channel images, and runs training.
    """
    with open('resources/dataset/Image dataset/dataset_train_image_augmented.dta', 'rb') as input1:
        dta = pickle.load(input1)

    twoDimImages = np.zeros((len(dta), 2, 126, 126))
    labels = []

    for index, entry in enumerate(dta):
        twoDimImages[index, 0, :, :] = entry.maxamp[1:127, 1:127]
        twoDimImages[index, 1, :, :] = np.reshape(hog(entry.maxamp), (126, 126))
        labels.append(int(entry.damaged))

    twoDimImages = np.asarray(twoDimImages)
    labels = np.asarray(labels)

    twoDimImages = np.reshape(twoDimImages, (len(dta), 126, 126, 2))

    twoDimImages, labels = shuffle(twoDimImages, labels)

    history, model = twoDimFcnn(twoDimImages, labels)


if __name__ == "__main__":
    main()

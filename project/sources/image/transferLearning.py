"""
Attempt at utilizing transfer learning for classification
of maximum amplitude images.
@author: Franz Vrolijk
"""
import numpy as np
import pickle
import tensorflow as tf
from sklearn.utils import shuffle
from general.confusionMatrix import getConfusionMatrix


def maxAmpTL():
    """
    Loads training data, fits model to data.
    Loads test data, runs test and prints confusion matrix.
    """
    with open('resources/dataset/Image dataset/dataset_train_image_augmented.dta', 'rb') as input1:
        dta = pickle.load(input1)

    dataset = []
    labels = []

    for entry in dta:
        dataset.append(entry.maxamp)
        labels.append(entry.damaged)

    dataset, labels = shuffle(dataset, labels)

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    stackedData = np.zeros((dataset.shape[0], 128, 128, 3))
    for i in range(0, dataset.shape[0]):
        stackedData[i, :, :, :] = np.stack((dataset[i, :, :],) * 3, axis=-1)

    model, history = transferLearning(stackedData, labels)

    with open('resources/dataset/Image dataset/dataset_test_image.dta', 'rb') as input1:
        dta = pickle.load(input1)

    dataset = []
    labels = []

    for entry in dta:
        dataset.append(entry.maxamp)
        labels.append(entry.damaged)

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    stackedData = np.zeros((dataset.shape[0], 128, 128, 3))
    for i in range(0, dataset.shape[0]):
        stackedData[i, :, :, :] = np.stack((dataset[i, :, :],) * 3, axis=-1)

    predictions = model.predict(np.asarray(stackedData))
    predictions[predictions >= 0.5] = 1
    predictions[predictions < 0.5] = 0
    getConfusionMatrix(labels, predictions)


def transferLearning(data, labels):
    """
    Definition of transfer-learning based neural network.
    :param data: data to train on
    :param labels: labels to train with
    :return: history of train, model
    """
    model = tf.keras.models.Sequential()

    modelBase = tf.keras.applications.ResNet50(weights='imagenet', input_shape=(128, 128, 3), include_top=False)
    modelBase.layers.pop()
    for layer in modelBase.layers[:-4]:
        layer.trainable = False

    model.add(modelBase)

    model.add(tf.keras.layers.Flatten())

    model.add(tf.keras.layers.Dense(512))
    model.add(tf.keras.layers.Dropout(0.2))
    model.add(tf.keras.layers.Activation('relu'))

    model.add(tf.keras.layers.Dense(256))
    model.add(tf.keras.layers.Dropout(0.2))
    model.add(tf.keras.layers.Activation('relu'))

    model.add(tf.keras.layers.Dense(128))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(0.2))

    model.add(tf.keras.layers.Dense(64))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(tf.keras.layers.Dense(32))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(tf.keras.layers.Dense(16))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(8))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(4))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(2))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(1))

    model.compile(loss="binary_crossentropy",
                  optimizer='adam',
                  metrics=['accuracy'])

    model.summary()

    history = model.fit(data, labels, batch_size=2, validation_split=0.2, epochs=19)

    return model, history


if __name__ == "__main__":
    maxAmpTL()

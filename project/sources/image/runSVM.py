"""
SVM classification for max apmlitude C-scans.
Uses both grayscale, skimage-HOG and our own HOG
"""
import pickle

import numpy as np
from skimage.feature import hog
from sklearn import svm
from sklearn.utils import shuffle

from general.confusionMatrix import getConfusionMatrix
from general.ustool import getHOGFeatureVector


def runSVM():
    """
    Loads both training and testing data.
    Creates and fits three SVM classifiers with grayscale,
    sklearn-HOG and custom HOG representations of the data.
    Gets test predictions for all three classifiers.
    :return: predictions from classifiers
    """
    # Opens datafile
    with open('resources/dataset/Image dataset/dataset_train_image_augmented.dta', 'rb') as input1:
        dta = pickle.load(input1)

    with open('resources/dataset/Image dataset/dataset_test_image.dta', 'rb') as input1:
        dtaTest = pickle.load(input1)

    # Creates four empty lists data/labels for train/test
    dataset = []
    labels = []
    datasetTest = []
    labelsTest = []

    # First 20 entries to training set
    for entry in dta:
        dataset.append(entry)
    # Last 5 entries to test set
    for entry in dtaTest:
        datasetTest.append(entry)

    # Iterates training set, fetches labels and replaces Entry-objects with their maxamp-picture
    # (Because we don't need / want the entire Entry-object)
    for n in range(len(dataset)):
        labels.append(int(dataset[n].damaged))
        dataset[n] = dataset[n].maxamp
    # Same for the test set
    for n in range(len(datasetTest)):
        labelsTest.append(int(datasetTest[n].damaged))
        datasetTest[n] = datasetTest[n].maxamp

    # Converts to np array
    dataset = np.asarray(dataset)
    datasetTest = np.asarray(datasetTest)
    labels = np.asarray(labels)
    labelsTest = np.asarray(labelsTest)

    dataset, labels = shuffle(dataset, labels)
    datasetTest, labelsTest = shuffle(datasetTest, labelsTest)

    # Reshapes data from (nSamples, x, y) to (nSamples, x*y)
    # (AKA flattens images)
    nSamples, numX, numY = dataset.shape
    datasetReshaped = dataset.reshape((nSamples, numX * numY))
    nSamples, numX, numY = datasetTest.shape
    datasetTestReshaped = datasetTest.reshape((nSamples, numX * numY))

    # GRAYSCALE

    # Creates classifier, fits
    clf = svm.SVC()
    clf.fit(datasetReshaped, labels)

    # Runs predictions
    resultGray = clf.predict(datasetTestReshaped)
    # accGray = acc(resultGray, labelsTest)

    # STIG HOG

    hogsTrain = []
    hogsTest = []

    for pic in dataset:
        hogsTrain.append(getHOGFeatureVector(pic, blockSize=32))

    for pic in datasetTest:
        hogsTest.append(getHOGFeatureVector(pic, blockSize=32))

    hogsTrain = np.asarray(hogsTrain)
    hogsTest = np.asarray(hogsTest)

    # Creates classifier, fits
    clf = svm.SVC()
    clf.fit(hogsTrain, labels)

    # Runs predictions
    resultStigHog = clf.predict(hogsTest)
    # accStig = acc(resultStigHog, labelsTest)

    # SKIMAGE HOG

    # Empty lists for hogs
    hogsTrain = []
    hogsTest = []

    # Creates hogs
    for pic in dataset:
        hogsTrain.append(hog(pic, pixels_per_cell=(16, 16)))

    for pic in datasetTest:
        hogsTest.append(hog(pic, pixels_per_cell=(16, 16)))

    # Converts to np array
    hogsTrain = np.asarray(hogsTrain)
    hogsTest = np.asarray(hogsTest)

    # Creates classifier, fits
    clf = svm.SVC()
    clf.fit(hogsTrain, labels)

    # Runs predictions
    resultSkHog = clf.predict(hogsTest)
    # accSkimage = acc(resultSkHog, labelsTest)

    return resultGray, resultStigHog, resultSkHog, labelsTest


def main():
    """
    Runs model training and testing "nRuns" times.
    Collects results and prints confusion matrix
    for each of the classifiers.
    """
    nRuns = 10

    results = np.zeros((nRuns, 4, 14))
    for i in range(nRuns):
        print("\n\nRun " + str(i) + "... \n")
        results[i, :] = runSVM()

    totalGrayResults = list(np.asarray(results[:, 0, :]).flatten())
    totalStigHogResults = list(np.asarray(results[:, 1, :]).flatten())
    totalSkHogResults = list(np.asarray(results[:, 2, :]).flatten())
    totalLabels = list(np.asarray(results[:, 3, :]).flatten())

    print("\n\nGRAY:")
    getConfusionMatrix(totalLabels, totalGrayResults)
    print("\n\nSTIGHOG:")
    getConfusionMatrix(totalLabels, totalStigHogResults)
    print("\n\nSKIMHOG:")
    getConfusionMatrix(totalLabels, totalSkHogResults)


if __name__ == "__main__":
    main()

"""
Fully connected network for maximum amplitude image classification
"""
import pickle
import numpy as np
from sklearn.utils import shuffle
from sources.general.confusionMatrix import getConfusionMatrix
import tensorflow as tf


def maxAmpFCNN():
    """
    Loads training data, fits classifier to data.
    Loads test data, runs test and outputs confusion matrix.
    """
    with open('resources/dataset/Image dataset/dataset_train_image_augmented.dta', 'rb') as input1:
        dta = pickle.load(input1)

    dataset = []
    labels = []

    for entry in dta:
        dataset.append(entry.maxamp)
        labels.append(entry.damaged)

    dataset, labels = shuffle(dataset, labels)
    model, history = fullyConnectedNet(dataset, labels)

    with open('resources/dataset/Image dataset/dataset_test_image.dta', 'rb') as input1:
        dta = pickle.load(input1)

    dataset = []
    labels = []

    for entry in dta:
        dataset.append(entry.maxamp)
        labels.append(entry.damaged)

    predictions = model.predict(np.asarray(dataset))
    predictions[predictions >= 0.5] = 1
    predictions[predictions < 0.5] = 0
    getConfusionMatrix(labels, predictions)


def fullyConnectedNet(data, labels):
    """
    Definition of FCNN
    :param data: data to train on
    :param labels: labels to train with
    :return: history of train, model
    """
    data = np.asarray(data)
    labels = np.asarray(labels)

    # Construct model
    model = tf.keras.models.Sequential()

    model.add(tf.keras.layers.Flatten(input_shape=(128, 128)))

    model.add(tf.keras.layers.Dense(16384))
    model.add(tf.keras.layers.Dropout(0.2))
    model.add(tf.keras.layers.Activation('relu'))

    model.add(tf.keras.layers.Dense(400))
    model.add(tf.keras.layers.Activation('relu'))

    model.add(tf.keras.layers.Dense(100))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(20))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(5))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(1))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.compile(loss="binary_crossentropy",
                  optimizer="adam",
                  metrics=['accuracy'])

    result = model.fit(data, labels, batch_size=16, validation_split=0.2, epochs=3)

    return model, result


if __name__ == '__main__':
    maxAmpFCNN()

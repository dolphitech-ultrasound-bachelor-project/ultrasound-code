"""
Convolutional neural network for classifying maximum amplitude images
"""
import os
import pickle
import numpy as np
from sklearn.utils import shuffle
from general import confusionMatrix
import tensorflow as tf


def maxAmpCNN():
    """
    Loads training data, fits classifier to data.
    Loads test data, runs test and prints confusion matrix.
    """
    with open('resources/dataset/Image dataset/dataset_train_image_augmented.dta', 'rb') as input1:
        dta = pickle.load(input1)

    dataset = []
    labels = []

    for entry in dta:
        dataset.append(entry.maxamp)
        labels.append(entry.damaged)

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    dataset, labels = shuffle(dataset, labels)
    model, history = convNet(dataset, labels)

    with open('resources/dataset/Image dataset/dataset_test_image.dta', 'rb') as input1:
        dta = pickle.load(input1)

    dataset = []
    labels = []

    for entry in dta:
        dataset.append(entry.maxamp)
        labels.append(entry.damaged)

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    dataset, labels = shuffle(dataset, labels)
    dataset = np.expand_dims(dataset, -1)

    predictions = model.predict(dataset)

    predictions[predictions >= 0.5] = 1
    predictions[predictions < 0.5] = 0

    confusionMatrix.getConfusionMatrix(labels, predictions)


def convNet(data, labels):
    """
    Definition of CNN
    :param data: data to train on
    :param labels: labels to train with
    :return: history of train, model
    """
    data = np.expand_dims(data, -1)

    model = tf.keras.models.Sequential()

    model.add(tf.keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=(128, 128, 1)))
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(64, (3, 3), activation='relu'))

    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(64, activation='relu'))
    model.add(tf.keras.layers.Dense(16, activation='sigmoid'))
    model.add(tf.keras.layers.Dense(1))

    model.compile(loss="binary_crossentropy",
                  optimizer="adam",
                  metrics=['accuracy'])

    history = model.fit(data, labels, batch_size=1, validation_split=0.2, epochs=10)

    modelPath = "resources/model/maxAmpCNNmodel"
    if not os.path.exists(modelPath):
        os.makedirs(modelPath)
        print("Created model folder in: ", modelPath)

    model.save("modelPath")

    return model, history


if __name__ == "__main__":
    maxAmpCNN()

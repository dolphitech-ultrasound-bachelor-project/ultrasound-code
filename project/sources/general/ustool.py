# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 10:04:50 2020

@author: Stig
"""

import matplotlib.pyplot as plt
import numpy as np


def getAScan(cScan, x, y):
    """
    Function that returns a A-scan from C-scan data,
    by x and y position (0-127,0-127) of a sensor

    Args:
        cScan: The C-scan
        x: The x position of the sensor
        y: The y position of the sensor

    Returns:
        slicedData: Data sliced so that it returns the A-scan
    """

    return cScan[:, y, x]


def displayAScan(cScan, x, y):
    """
    Function that displays a A-scan from C-scan data,
    by x and y position (0-127,0-127) of a sensor

    Args:
        cScan: The C-scan
        x: The x position of the sensor
        y: The y position of the sensor
    """

    plt.figure()
    plt.plot(cScan[:, y, x])
    plt.show()


def getResponseFunction(cScan, percentage, method="mixed"):
    """
    Function that returns a response function,
    and its related dips from a C-scan

    Args:
        cScan: The C-scan
        percentage: The percentage to use of the total width of the wave, for sigma
        method: The method to use for extracting a average, default is 'mixed'

    Returns:
        responseFunction: The extracted response function
        dips: The dips of the function, including start and end
    """

    averageWave = None

    if method.lower() == "topdown":
        # Calculate the average sum of A-scan's for each timestep (TOPDOWN)
        averageWave = np.abs(cScan.sum(axis=(1, 2)) / (128 * 128))

    elif method.lower() == "side1":
        # Calculate the average A-scan by looking at
        averageWave = np.amax(np.abs(cScan), axis=1).sum(axis=1) / 128

    elif method.lower() == "side2":
        averageWave = np.amax(np.abs(cScan), axis=2).sum(axis=1) / 128

    else:
        cScanSum = np.abs(cScan.sum(axis=(1, 2)) / (128 * 128))
        aScanxSum = np.amax(np.abs(cScan), axis=1).sum(axis=1) / 128
        aScanySum = np.amax(np.abs(cScan), axis=2).sum(axis=1) / 128
        averageWave = (cScanSum + aScanxSum + aScanySum) / 3

    # Calculate the absolute of the averaged wave
    absoluteWave = np.abs(averageWave)

    # The domain of X values we are working with
    domain = absoluteWave.size

    # Generates a matrix of values that represents a subtraction of
    # -1, -2, ..., -N of each row as it goes downwards. Each row
    # in this matrix represents a gaussian distribution
    # on every value of the X-axis from 0-abswave.size.
    # When this is multiplied with the absolute wave values,
    # and then summed, we in fact get a smoothed distribution
    # of our original wavepeaks

    # Example of original matrix before gaussian:
    # +0 +1 +2 +3 +4 +5
    # -1 +0 +1 +2 +3 +4
    # -2 -1 +0 +1 +2 +3
    # -3 -2 -1 +0 +1 +2
    # ... and so on ...
    matrix = np.arange(domain)[None, :] - np.arange(domain)[:, None]

    # We assume a width of distribution that is a percentage of the X-axis size
    sigma = percentage * absoluteWave.size

    # The actual calculation, which creates a
    # gaussian distribution of a X-axis percentage width on each point
    c1 = 1 / (np.sqrt(2 * np.pi) * sigma)
    c2 = -0.5 * (matrix / sigma) ** 2
    gaussians = c1 * np.exp(c2)

    # Applies the absolute wave y-values to each gaussian distribution
    multiplicator = np.zeros(domain)[None, :] + absoluteWave[:, None]
    gaussians = gaussians * multiplicator

    # Sum all the peaks into eachother, thus creating a 'smoothed'
    # version of our original absolute wave function
    responseFunction = gaussians.sum(axis=0)

    return responseFunction


def getSlicedCScan(cScan, indices):
    """
    Function that slices a C-scan from
    a list of given slice-indices

    Args:
        cScan: The C-scan
        indices: List of ascending indices to slice

    Returns:
        slicedData: List of sliced data, containing the following on each:
            (1) The C-scan slice
            (2) The start indice of the slice
            (3) The end indice of the slice
    """

    # If no indices are given or improper indices, we return None
    if len(indices) == 0 or any(indice < 0 or indice > cScan.shape[0] for indice in indices):
        return None

    # We make sure that the list has only unique values
    # by converting it to set and then back to a list
    indices = list(set(indices))

    # Sort the list so that it is garanteed to be ascending
    indices.sort()

    # We add 0 and the last value into the list
    # for the algorithm, (if they are missing)
    if indices[0] > 0:
        indices.insert(0, 0)
    if indices[-1] < cScan.shape[0]:
        indices.insert(len(indices), cScan.shape[0])

    slices = []
    for i in range(1, len(indices)):
        slices.append([cScan[indices[i - 1]:indices[i]], (indices[i - 1], indices[i])])

    return slices


def getImageByMaximum(cScan):
    """
    Function that retrieves a 2D image
    from a C-scan (Based on maximum of A-scan)

    Args:
        cScan: The C-scan

    Returns:
        image: Image which has been normalized and scaled to np.uint8
    """

    # Picks out the maximum responses from ultrasound wave
    # IE; this function flattens downwards to a 2D image
    # where each pixel gets the maximum absolute value found in a wave
    image = np.amax(cScan, axis=0)

    return (normalize(image) * 255).astype(np.uint8)


def getImageByLayer(cScan, layer):
    """
    Function that retrieves a 2D image from a C-scan layer

    Args:
        cScan: The C-scan
        layer: The layer to retrieve the image from

    Returns:
        image: Image which has been normalized and scaled to np.uint8
    """

    return (normalize(np.abs(cScan[layer])) * 255).astype(np.uint8)


def normalize(data):
    """
    Function that returns a normalized version of the data.
    The new scale is between 0-1, where 1 is the same as the data's
    current maximum, and has nothing to do with maximum amplitude

    Args:
        data: Data to be normalized

    Returns:
        norm: The normalized data
    """

    norm = data.copy()
    norm = norm - norm.min()
    norm = (norm / (norm.max() - norm.min())).astype(np.float64)

    return norm


def getThresholdedImage(image, threshold):
    """
    Function that thresholds a given image by a given treshold
    Args:
        image: The image to threshold
        threshold: The threshold value

    Returns:
        thresholded:
            The thresholded image
    """

    thresholded = image.copy()
    thresholded[image > threshold] = np.iinfo(image.dtype).max
    thresholded[image <= threshold] = np.iinfo(image.dtype).min

    return thresholded


def getHOGFeatureVector(image, degreeDelta=45, blockSize=4):
    """
    Function that thresholds a given image by a given treshold
    Args:
        image: The image to extract HOG feature from
        degreeDelta: How many degrees between each bin
        blockSize: How many pixels each block contains

    Returns:
        featureVector: List of the normalized HOG's, which
        has been concatinated into a giant vector
    """

    # Calculate the variables by incoming parameteres
    TOTALDEGREES = 360
    BINDELTA = degreeDelta
    BINSIZE = int(TOTALDEGREES / BINDELTA) + 1

    IMAGESIZE = 128
    BLOCKSIZE = blockSize
    NUMBEROFBLOCKS = int(IMAGESIZE / BLOCKSIZE)

    # Normalize the image
    image = normalize(image)

    # We calculate the needed data
    # Note:
    # +180 is for bin division (range is -180 <-> 180, but now its 0-360).
    # To display arrows one should subtract 180 again
    gy = np.gradient(image, axis=0)
    gx = np.gradient(image, axis=1)
    mg = np.sqrt(gx ** 2 + gy ** 2)
    th = np.rad2deg(np.arctan2(gy, gx)) + 180

    # Empty array for the hogs
    hogs = np.zeros((NUMBEROFBLOCKS, NUMBEROFBLOCKS, BINSIZE))

    # Indexer that lets us vectorize a lot of the operations
    indexer = np.transpose((IMAGESIZE * np.arange(BLOCKSIZE)[:, None, None, None]
                            + np.arange(BLOCKSIZE)[None, :, None, None]
                            + BLOCKSIZE * IMAGESIZE * np.arange(NUMBEROFBLOCKS)[None, None, :, None]
                            + BLOCKSIZE * np.arange(NUMBEROFBLOCKS)[None, None, None, :]), axes=(2, 3, 0, 1))

    # The convolution from vectorization
    th_convolutions = th.flatten()[indexer.flatten()].reshape(indexer.shape)
    mg_convolutions = mg.flatten()[indexer.flatten()].reshape(indexer.shape)

    # Calculate the divide
    hog_indexes = (th_convolutions / BINDELTA)
    hog_indexes_ceiling = np.ceil(hog_indexes).astype(np.int)
    hog_indexes_floor = np.floor(hog_indexes).astype(np.int)

    # Calculate the divide votes
    floor_percentages = hog_indexes_ceiling - hog_indexes
    ceiling_percentages = 1 - floor_percentages
    ceiling_votes = ceiling_percentages * mg_convolutions
    floor_votes = floor_percentages * mg_convolutions

    # Add the votes into the hogs
    for y in range(NUMBEROFBLOCKS):
        for x in range(NUMBEROFBLOCKS):
            np.add.at(hogs[y, x],
                      hog_indexes_ceiling[y, x].flatten(),
                      ceiling_votes[y, x].flatten())

            np.add.at(hogs[y, x],
                      hog_indexes_floor[y, x].flatten(),
                      floor_votes[y, x].flatten())

    # We create the featureVectore of normalized hogs
    # in a square pattern, IE. we do not take each block
    # and normalize, but normalize over a square area
    featureVector = []
    for y in range(NUMBEROFBLOCKS - 1):
        for x in range(NUMBEROFBLOCKS - 1):
            flatHogs = np.concatenate([
                hogs[y, x], hogs[y, x + 1],
                hogs[y + 1, x], hogs[y + 1, x + 1]
            ], axis=0)
            normalizedFlatHogs = flatHogs.copy()
            normalizedFlatHogs[normalizedFlatHogs != 0] /= np.sqrt(normalizedFlatHogs.sum())
            featureVector.extend(normalizedFlatHogs)

    return featureVector

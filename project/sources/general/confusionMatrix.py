"""
Contains code to find the true negative, false positive, false negative, and true positive classifications
for a binary classification scenario, from labels and their corresponding predictions
"""

from typing import List
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import numpy as np


def getConfusionMatrix(labels: List, predictions: List):
    """
    Gets the confusion matrix (TN, FP, FN, and TP) for a given list of labels/predictions
    Also prints out the confusion matrix + other useful numbers for analysis

    Args:
        labels: The labels for data, ground truth
        predictions: The predictions from a classifier/model

    Returns: Confusion matrix as a tuple (tn, fp, fn, tp)

    """
    assert len(labels) == len(predictions), "Error: Labels and predictions must have the same length!"

    tn, fp, fn, tp = confusion_matrix(labels, predictions).ravel()
    predAmount = len(predictions)

    p = np.count_nonzero(labels)
    n = len(labels) - p

    print("Predictions:")
    print("True Negative (TN): \t", tn, "\t\t\t\t\t", round(tn / predAmount * 100, 2), "%", sep='')
    print("False Positive (FP): \t", fp, "\t\t\t\t\t", round(fp / predAmount * 100, 2), "%", sep='')
    print("False Negative (FN): \t", fn, "\t\t\t\t\t", round(fn / predAmount * 100, 2), "%", sep='')
    print("True Positive (TP): \t", tp, "\t\t\t\t\t", round(tp / predAmount * 100, 2), "%", sep='')
    print("Accuracy (ACC): \t\t\t\t\t\t", round((tn + tp) / predAmount * 100, 2), "%", sep='')

    if p > 0 and n > 0:
        print("Balanced Accuracy (BA): \t\t\t\t\t", round((tp / p + tn / n) / 2 * 100, 2), "%", sep='')
        print("Sensitivity, True Positive Rate (TPR): \t\t", round(tp / p * 100, 2), "%", sep='')
        print("Specificity, True Negative Rate (TNR): \t\t", round(tn / n * 100, 2), "%", sep='')
    else:
        print("Balanced Accuracy (BA): \t\t\t\t\t", "N/A")
        print("Sensitivity, True Positive Rate (TPR): \t\t", "N/A")
        print("Specificity, True Negative Rate (TNR): \t\t", "N/A")

    if tp > 0 or fp > 0:
        print("Precision, Positive Predictive Value (PPV): ", round(tp / (tp + fp) * 100, 2), "%", sep='')
    else:
        print("Precision, Positive Predictive Value (PPV):", "N/A")

    if tn > 0 or fn > 0:
        print("Negative Predictive Value (NPV): \t\t\t", round(tn / (tn + fn) * 100, 2), "%", sep='')
    else:
        print("Negative Predictive Value (NPV): \t\t\t", "N/A")

    if tp > 0 or fp > 0 or fn > 0:
        print("Harmonic mean of PPV & TPR (F1): \t\t\t", round(2 * tp / (2 * tp + fp + fn) * 100, 2), "%", sep='')

    else:
        print("Harmonic mean of PPV & TPR (F1): \t\t\t", "N/A")

    return tn, fp, fn, tp


def plotConfusionMatrix(labels: List, predictions: List, title: str = "Binary classification of material damage, "
                                                                      "confusion matrix"):
    """
    Plots a confusion matrix from a given list of labels/predictions.

    Code borrowed from https://www.tarekatwan.com/index.php/2017/12/how-to-plot-a-confusion-matrix-in-python/
    Retrieved 02.04.2020

    Args:
        labels: The labels for data, ground truth
        predictions: The predictions from a classifier/model
        title: Title to use for the plot

    Returns: None

    """
    matrix = confusion_matrix(labels, predictions)
    plt.imshow(matrix, interpolation='nearest', cmap=plt.cm.Wistia)
    classNames = ['Not damaged', 'Damaged']

    plt.title(title)
    plt.ylabel('Actual label')
    plt.xlabel('Predicted label')

    tickMarks = np.arange(len(classNames))
    plt.xticks(tickMarks, classNames, rotation=45)
    plt.yticks(tickMarks, classNames)

    s = [['TN', 'FP'], ['FN', 'TP']]
    for i in range(2):
        for j in range(2):
            plt.text(j, i, str(s[i][j]) + " = " + str(matrix[i][j]))

    plt.show()

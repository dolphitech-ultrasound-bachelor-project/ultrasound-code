# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 18:24:13 2020

@author: Stig
"""
import sys
from typing import Union
import h5py
import numpy as np

__SIZE = 128


def load(*args: str) -> Union[list, np.ndarray]:
    """
    Loads the given filepath(s) as H5 files

    Args:
        filepaths(s) to load

    Returns:
        List of datasets, or a dataset if only one string as argument.
        A dataset is a list containing the following on each:
            (1) The C-scan data
            (2) The startGate
            (3) The endGate
    """
    filepaths = []

    # Go through arguments, and make sure we only have strings,
    # also load said strings into a list of filepaths
    for o in args:
        if isinstance(o, list) and all(isinstance(s, str) for s in o):
            for path in o:
                filepaths.append(path)

        elif isinstance(o, str):
            filepaths.append(o)

        else:
            print("Arguments must be strings to filepaths only")
            sys.exit(1)

    # --------------------------------------------------------#
    #  ARGUMENTS HAVE BEEN CLEANED INTO A LIST OF RESOURCES   #
    # --------------------------------------------------------#

    dataSets = []

    for path in filepaths:
        # Open the file
        file = h5py.File(path, "r")

        # Read the data from the file
        data = file["DCV2/frames/1/Yic"][()]

        # Convert it to a list of layers (List of np.ndarray)
        data = data.reshape((data.shape[0], __SIZE, __SIZE), order='F')
        startGate = file["DCV2/frames/1/settings/amp_c_scan_gate_1_start"][()][0]
        endGate = file["DCV2/frames/1/settings/amp_c_scan_gate_1_end"][()][0]

        dataSets.append([data, startGate, endGate])

    # Make sure that we return the list of datasets, or if we only
    # have a single data, return it instead of list
    if len(dataSets) == 1:
        return dataSets[0]

    return dataSets

"""
Convolutional neural network for C-scan classification
"""
import pickle
import numpy as np
from sklearn.utils import shuffle
import sources.general.confusionMatrix as confMat
import tensorflow as tf


def runCnnCscan():
    """
    Loads training data, fits classifier to data.
    Loads test data, runs test and prints confusion matrix.
    """
    # Open data file
    with open('resources/dataset/C-scan dataset/dataset_train_cscan_downsampled_augmented.dta', 'rb') as input1:
        dta = pickle.load(input1)

    dataset = []
    labels = []
    # Make dataset and labels from datafile
    for entry in dta:
        dataset.append(entry.data)
        labels.append(int(entry.damaged))

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    dataset, labels = shuffle(dataset, labels)

    print(dataset.shape)
    # Run NN
    model, history = cscanCnn(dataset, labels)

    with open('resources/dataset/C-scan dataset/dataset_test_cscan_downsampled.dta', 'rb') as input1:
        dta = pickle.load(input1)

    dataset = []
    labels = []
    # Make dataset and labels from datafile
    for entry in dta:
        dataset.append(entry.data)
        labels.append(int(entry.damaged))

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    dataset, labels = shuffle(dataset, labels)

    predictions = model.predict(dataset)
    predictions[predictions >= 0.5] = 1
    predictions[predictions < 0.5] = 0

    confMat.getConfusionMatrix(labels, predictions)


def cscanCnn(data, labels):
    """
    Definition of neural network
    :param data: data to train on
    :param labels: labels to train with
    :return: history of training, model
    """
    # data = np.expand_dims(data, -1)

    labels = np.asarray(labels)

    # Construct model
    model = tf.keras.models.Sequential()

    model.add(tf.keras.layers.Conv2D(64, (3, 3), input_shape=(64, 128, 128)))

    model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(tf.keras.layers.Conv2D(64, (3, 3)))

    model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))

    model.add(tf.keras.layers.Flatten())

    model.add(tf.keras.layers.Dense(1024))
    model.add(tf.keras.layers.Dropout(0.1))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(512))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(tf.keras.layers.Dense(256))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(tf.keras.layers.Dense(128))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(64))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(32))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(8))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(1))

    model.summary()

    model.compile(loss="binary_crossentropy",
                  optimizer="adam",
                  metrics=['accuracy'])

    model.save("resources/model/cScanCNN")

    history = model.fit(data, labels, batch_size=8, validation_split=0.2, epochs=20)

    return model, history


if __name__ == "__main__":
    runCnnCscan()

import os

import tensorflow as tf
from tensorflow.keras import layers
from project.sources.general.confusionMatrix import getConfusionMatrix

import matplotlib.pyplot as plt
import numpy as np
import random
import pickle
import sys

np.set_printoptions(suppress=True)
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(linewidth=1000)

# Load training data
traindataset = []
with open("resources/dataset/C-scan dataset/dataset_train_cscan_downsampled_augmented.dta", 'rb') as file:
    while True:
        try:
            traindataset.extend(list(pickle.load(file)))
        except EOFError:
            break

# Shuffle training data and retrieve data + label
random.shuffle(traindataset)
x_train = np.asarray([entry.data for entry in traindataset])
x_train = np.expand_dims(x_train, -1)
y_train = np.asarray([entry.damaged for entry in traindataset])

# Load testing data
testdataset = []
with open("resources/dataset/C-scan dataset/dataset_test_cscan_downsampled.dta", 'rb') as file:
    while True:
        try:
            testdataset.extend(list(pickle.load(file)))
        except EOFError:
            break

# Shuffle testing data and retrieve data + label
random.shuffle(testdataset)
x_test = np.asarray([entry.data for entry in testdataset])
x_test = np.expand_dims(x_test, -1)
y_test = np.asarray([entry.damaged for entry in testdataset])

# Setup the CNN LSTM model
model = tf.keras.Sequential()
model.add(layers.ZeroPadding3D(padding=(3, 0, 0), input_shape=(64, 128, 128, 1)))
model.add(layers.Conv3D(
        32,
        strides=(1, 1, 1),
        kernel_size=(7, 128, 128),
        input_shape=(64, 128, 128, 1),
        data_format="channels_last",
        activation="tanh"
    )
)
model.add(layers.Reshape((64, 32)))
model.add(layers.LSTM(
        64,
        input_shape=(64, 32),
        return_sequences=False,
        dropout=0.2,
        recurrent_dropout=0.2
    )
)
model.add(layers.Dense(1, activation="sigmoid"))
model.summary()

model.compile(
    loss="binary_crossentropy",
    optimizer="adam",
    metrics=['accuracy']
)

# Train the classifier
history = model.fit(
    x_train,
    y_train,
    batch_size=1,
    validation_split=0.20,
    epochs=10
)

# Make testing predictions, and get confusion matrix
predictions = np.squeeze(model.predict(x_test), axis=1)
predictions[predictions >= 0.50] = 1
predictions[predictions < 0.50] = 0
getConfusionMatrix(y_test.tolist(), predictions.tolist())

# Create model/plot folders if needed
modelPath = "resources/model/cScanCNNLSTMModel/"
if not os.path.exists(modelPath):
    os.makedirs(modelPath)
    print("Created model folder in: ", modelPath)

# Plot acc/val_acc
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.savefig(modelPath + "cnnlstm_cscan_accuracy.png")
plt.show()

# Plot loss/val_loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.savefig(modelPath + "cnnlstm_cscan_loss.png")
plt.show()

# Save model to h5 file
model.save(modelPath + "cScanLSTMModel.h5")
print("Saved LSTM Model in: ", modelPath)

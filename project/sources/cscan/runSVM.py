"""
SVM for classifying entire C-scans
"""
import pickle

import numpy as np
from sklearn import svm
from sklearn.utils import shuffle

from dataset import trainingDtaCreator
from dataset import testingDtaCreator
from sources.general.confusionMatrix import getConfusionMatrix


def cScanSVM():
    """
    Loads training data, fits model to data
    Loads test data, runs test and prints confusion matrix
    """
    trainingDtaCreator.makeDataset()
    testingDtaCreator.makeDataset()

    with open('resources/dataset/C-scan dataset/dataset_train_cscan_downsampled_augmented.dta', 'rb') as input1:
        dta = pickle.load(input1)

    dataset = []
    labels = []
    # Make dataset from datafile

    for entry in dta:
        dataset.append(entry.data)
        labels.append(int(entry.damaged))

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    dataset, labels = shuffle(dataset, labels)

    data = np.zeros((len(dta), 64, 128, 128))

    # Reshape data
    for sample in range(len(dataset)):
        copy = np.zeros((64, 128, 128))
        for i in range(128):
            copy[:, :, i] = np.resize(dataset[sample][:, :, i], (64, 128))
        data[sample, :, :, :] = copy

    # Reshape to 2D
    nSamples, numX, numY, numZ = np.asarray(data.shape)
    dataset = data.reshape((nSamples, numX * numY * numZ))

    dataset, labels = shuffle(dataset, labels)

    # Fit classifier
    clf = svm.SVC()
    clf.fit(dataset, labels)

    # Load test set and test
    with open('resources/dataset/C-scan dataset/dataset_test_cscan_downsampled.dta', 'rb') as input1:
        testData = pickle.load(input1)

    testSet = []
    testLabels = []

    testSet, testLabels = shuffle(testSet, testLabels)

    for entry in testData:
        testSet.append(entry.data)
        testLabels.append(int(entry.damaged))

    testSet = np.asarray(testSet)

    data = np.zeros((len(testData), 64, 128, 128))

    # Reshape data
    for sample in range(len(testSet)):
        copy = np.zeros((64, 128, 128))
        for i in range(128):
            copy[:, :, i] = np.resize(testSet[sample][:, :, i], (64, 128))
        data[sample, :, :, :] = copy

    # Reshape to 2D
    nSamples, numX, numY, numZ = np.asarray(data.shape)
    testSet = data.reshape((nSamples, numX * numY * numZ))

    testSet, testLabels = shuffle(testSet, testLabels)

    predictions = clf.predict(testSet)

    getConfusionMatrix(testLabels, predictions)


if __name__ == "__main__":
    cScanSVM()

"""
Random forest classifier for A-scans

"""
import pickle
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils import shuffle
from general.confusionMatrix import getConfusionMatrix


def runRFC():
    """
    Loads training data, fits classifier to data.
    Loads test data, runs test and prints confusion matrix
    """
    # Opens datafile
    with open('resources/dataset/A-scan dataset/dataset_train_ascan_downsampled.dta', 'rb') as input1:
        dta = pickle.load(input1)

    # Shuffle datafile
    dta = shuffle(dta)

    # Creates empty lists data/labels
    dataset = []
    labels = []

    # Make training set
    for aScan in dta:
        dataset.append(aScan.data)
        labels.append(aScan.damaged)

    rfc = RandomForestClassifier(n_estimators=500, n_jobs=14)

    dataset = np.asarray(dataset)

    rfc.fit(dataset, labels)

    # Save model
    with open('resources/model/rfc.model', 'wb') as output:
        pickle.dump(rfc, output, pickle.HIGHEST_PROTOCOL)

    # TESTING:
    with open('resources/dataset/A-scan dataset/dataset_test_ascan_downsampled.dta', 'rb') as inp:
        testSet = pickle.load(inp)

    # Shuffle datafile
    testSet = shuffle(testSet)

    # Creates empty lists data/labels
    dataset = []
    labels = []

    # Make training set
    for aScan in testSet:
        dataset.append(aScan.data)
        labels.append(aScan.damaged)

    dataset = np.asarray(dataset)

    predictions = rfc.predict(dataset)

    getConfusionMatrix(labels, predictions)


if __name__ == "__main__":
    runRFC()

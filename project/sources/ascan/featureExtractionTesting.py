"""
File for feature extraction and training of neural network model with A-scan features.


Features V2:

• Maximum absolute amplitude of the waveform; -> np.amax(sample)
• Maximum peak-to-peak amplitude of the waveform; find peaks -> find p-p amp for all peaks -> find max
• Global pulse duration between 25% levels;
• Local fall time from peak to 25% level; -> find peak -> find time from peak to < 0.25 x peak?
• Local rise variance between 25% level and peak;
• Local fall variance between peak and 25% level;
• Local fall variance between peak and 50% level;
• Global rise variance between 25% level and peak;
• Global rise variance between 50% level and peak;
• Global fall variance between peak and 25% level;
• Global fall variance between peak and 50% level;
• Local rise variance between 25% level and peak of spectrum;
• Local rise variance between 50% level and peak of spectrum;
• Global rise variance between 50% level and peak of the spectrum.
"""
import os
import pickle
import numpy as np
import tensorflow as tf
from scipy.stats import skew
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils import shuffle
from general.confusionMatrix import getConfusionMatrix


def makeFeatureDataset():
    """
    Makes training dataset with features from A-scan dataset
    :return: nothing
    """
    with open('resources/dataset/A-scan dataset/dataset_train_ascan_downsampled.dta', 'rb') as input1:
        trainingSet = pickle.load(input1)

    # Creates four empty lists data/labels for train/test
    dataset = []
    labels = []

    # Make training set
    for aScan in trainingSet:
        dataset.append(aScan.data)
        labels.append(aScan.damaged)

    nSamples = len(dataset)

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    dataset, labels = shuffle(dataset, labels)
    dataset, labels = shuffle(dataset, labels)

    featureDataset = []

    # Make dataset with features and data + labels
    i = 0
    for sample in dataset:
        entry = []

        featureSample = [np.mean(sample), np.mean(np.abs(sample)), np.median(sample), np.amin(sample), np.amax(sample),
                         np.var(sample), np.std(sample), skew(sample)]
        featureSample = np.asarray(featureSample).flatten()

        # concatenated = np.concatenate([sample, featureSample])

        entry.append(featureSample)
        entry.append(labels[i])

        featureDataset.append(entry)

        print(str(i) + " of " + str(nSamples))
        i += 1

    featureDataset = np.asarray(featureDataset)

    with open('resources/dataset/A-scan dataset/dataset_train_ascan_features.dta', 'wb') as output:
        pickle.dump(featureDataset, output, pickle.HIGHEST_PROTOCOL)


def makeFeatureTestDataset():
    """
    Makes test dataset with features from A-scan test dataset
    :return: nothing
    """
    with open('resources/dataset/A-scan dataset/dataset_test_ascan_downsampled.dta', 'rb') as input1:
        trainingSet = pickle.load(input1)

    # Creates four empty lists data/labels for train/test
    dataset = []
    labels = []

    # Make training set
    for aScan in trainingSet:
        dataset.append(aScan.data)
        labels.append(aScan.damaged)

    nSamples = len(dataset)

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    dataset, labels = shuffle(dataset, labels)
    dataset, labels = shuffle(dataset, labels)

    featureDataset = []

    # Make dataset with features and data + labels
    i = 0
    for sample in dataset:
        entry = []

        featureSample = [np.mean(sample), np.mean(np.abs(sample)), np.median(sample), np.amin(sample), np.amax(sample),
                         np.var(sample), np.std(sample), skew(sample)]
        featureSample = np.asarray(featureSample).flatten()

        # concatenated = np.concatenate([sample, featureSample])

        entry.append(featureSample)
        entry.append(labels[i])

        featureDataset.append(entry)

        featureNames = ["Mean\t", "Mean abs", "Median", "Min \t", "Max \t", "Variance", "Std. dev.",
                        "Skewness"]

        print("SAMPLE DAMAGED: " + str(labels[i]))
        print("Features:")
        for x in range(len(featureNames)):
            print(str(featureNames[x]) + ": ", end="\t")
            print(str(featureSample[x]))

        print(str(i) + " of " + str(nSamples))
        i += 1

    featureDataset = np.asarray(featureDataset)

    with open('resources/dataset/A-scan dataset/dataset_test_ascan_features.dta', 'wb') as output:
        pickle.dump(featureDataset, output, pickle.HIGHEST_PROTOCOL)


def aScanFeature():
    """
    Loads training data and fits NN model to data.
    Loads testing data and runs test, outputting confusion matrix.
    :return: nothing
    """
    with open('resources/dataset/A-scan dataset/dataset_train_ascan_features.dta', 'rb') as input1:
        dta = pickle.load(input1)

    # Creates four empty lists data/labels for train/test
    dataset = []
    labels = []

    # Make training set
    for aScan in dta:
        # Each entry will be the features
        dataset.append(aScan[0])
        # Label is the same as always
        labels.append(int(aScan[1]))

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    dataset, labels = shuffle(dataset, labels)
    dataset, labels = shuffle(dataset, labels)

    numTrue = 0
    for label in labels:
        if label:
            numTrue += 1

    print("Num true: " + str(numTrue) + "/" + str(len(labels)))

    result, model = featureNN(dataset, labels)

    # TESTING:
    with open('resources/dataset/A-scan dataset/dataset_test_ascan_features.dta', 'rb') as inp:
        testSet = pickle.load(inp)

    # Creates empty lists data/labels
    dataset = []
    labels = []

    # Make training set
    for aScan in testSet:
        dataset.append(aScan[0])
        labels.append(int(aScan[1]))

    dataset = np.asarray(dataset)

    predictions = model.predict(dataset)
    predictions[predictions >= 0.5] = 1
    predictions[predictions < 0.5] = 0
    getConfusionMatrix(labels, predictions)


def rfcTesting(dataset, labels):
    """
    For testing with random forest classifier.
    Fits model through parameters, runs test with confusion matrix output.

    :param dataset: to fit to
    :param labels: to fit to
    :return: nothing
    """
    rfc = RandomForestClassifier(n_estimators=100, n_jobs=14)
    rfc.fit(dataset, labels)

    # TESTING:
    with open('resources/dataset/A-scan dataset/dataset_test_ascan_features.dta', 'rb') as inp:
        testSet = pickle.load(inp)

    # Shuffle datafile
    testSet = shuffle(testSet)

    # Creates empty lists data/labels
    dataset = []
    labels = []

    # Make training set
    for aScan in testSet:
        dataset.append(aScan[0])
        labels.append(int(aScan[1]))

    dataset = np.asarray(dataset)

    predictions = rfc.predict(dataset)

    getConfusionMatrix(labels, predictions)


def featureNN(data, labels):
    """
    Neural network definition
    :param data: dataset
    :param labels: labels for dataset
    :return: results from fit() (history), model
    """
    data, labels = shuffle(data, labels)

    # Construct model
    model = tf.keras.models.Sequential()

    model.add(tf.keras.layers.Dense(8))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(8))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(32))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(32))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(16))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(4))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(1))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.compile(loss="binary_crossentropy",
                  optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
                  metrics=['accuracy'])

    # es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=3)
    result = model.fit(data, labels, batch_size=16, validation_split=0.2, epochs=1)

    # Save model to file
    modelPath = "resources/model/aScanFeatureModel"
    if not os.path.exists(modelPath):
        os.makedirs(modelPath)
        print("Created model folder in: ", modelPath)

    model.save("modelPath")
    print("Saved model in: ", modelPath)

    return result, model


if __name__ == "__main__":
    aScanFeature()

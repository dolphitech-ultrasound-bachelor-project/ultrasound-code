import tensorflow as tf
from tensorflow.keras import layers
from sources.general.confusionMatrix import getConfusionMatrix

import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys

np.set_printoptions(suppress=True)
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(linewidth=1000)

traindataset = []
with open("resources/dataset/A-scan dataset/dataset_train_ascan_downsampled.dta", 'rb') as file:
    while True:
        try:
            traindataset.extend(list(pickle.load(file)))
        except EOFError:
            break
x_train = np.asarray([entry.data for entry in traindataset])
x_train = np.expand_dims(x_train, -1)
y_train = np.asarray([entry.damaged for entry in traindataset])

testdataset = []
with open("resources/dataset/A-scan dataset/dataset_test_ascan_downsampled.dta", 'rb') as file:
    while True:
        try:
            testdataset.extend(list(pickle.load(file)))
        except EOFError:
            break
x_test = np.asarray([entry.data for entry in testdataset])
x_test = np.expand_dims(x_test, -1)
y_test = np.asarray([entry.damaged for entry in testdataset])

model = tf.keras.Sequential()
model.add(layers.ZeroPadding1D(
    padding=(3),
    input_shape=(128, 1)
))
model.add(layers.Conv1D(
    8,
    strides=(1),
    kernel_size=(7),
    input_shape=(128, 1),
    data_format="channels_last",
    activation="sigmoid"
))
model.add(layers.LSTM(
    16,
    input_shape=(128, 8),
    return_sequences=True,
    dropout=0.2,
    recurrent_dropout=0.2
))
model.add(layers.LSTM(
    16,
    input_shape=(128, 16),
    return_sequences=False,
    dropout=0.2
))
model.add(layers.Dense(
    1, activation="sigmoid"
))

model.summary()

model.compile(
    loss="binary_crossentropy",
    optimizer="adam",
    metrics=['accuracy']
)

history = model.fit(
    x_train,
    y_train,
    batch_size=16384,
    validation_split=0.20,
    epochs=5000
)

model.save("resources/model/aScanLSTMModel.h5")
print("Saved LSTM Model")

plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

predictions = np.squeeze(model.predict(x_test), axis=1)
predictions[predictions >= 0.50] = 1
predictions[predictions < 0.50] = 0
getConfusionMatrix(y_test.tolist(), predictions.tolist())

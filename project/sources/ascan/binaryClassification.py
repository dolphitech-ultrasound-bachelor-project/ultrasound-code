# -*- coding: utf-8 -*-
"""
Performs binary classification on A-scans using a combined neural network + contour analysis method
"""
import sys
from scipy.ndimage import median_filter
from scipy.signal import find_peaks
from scipy.signal import peak_widths
from scipy.signal import argrelmin
from scipy.signal import resample
import numpy as np
import time
import cv2

import sources.general.ustool as ustool

np.set_printoptions(suppress=True)
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(linewidth=1000)


def __retrieveSlices(smoothScan, method, percentage, constant):
    """
    Function to return a slicing of the scan by a given method,
    which is either 'peakwidth', 'constant' or 'localminima'
    """

    # Extract at a constant rate
    if method.lower() == "constant":
        indices = list(range(0, smoothScan.shape[0], constant))
        return ustool.getSlicedCScan(smoothScan, indices)

    # Extract around peaks, by their corresponding widths
    if method.lower() == "peakwidth":
        responseFunction = ustool.getResponseFunction(smoothScan, percentage)
        peaks = find_peaks(responseFunction)[0]
        widths = peak_widths(responseFunction, peaks)[0]

        indices = []
        for i, peak in enumerate(peaks):
            start = int(peak - widths[i])
            end = int(peak + widths[i])
            if start < 0:
                start = 0
            if end > responseFunction.size:
                end = responseFunction.size
            indices.append([(start, end), peak, responseFunction[peak]])

        scanSlices = []
        for (start, end), peak, height in indices:
            scanSlices.append([smoothScan[start:end], (start, end)])

        return scanSlices

    # Extract by local minima separation
    else:
        responseFunction = ustool.getResponseFunction(smoothScan, percentage)
        localMinima = argrelmin(responseFunction)[0].tolist()
        return ustool.getSlicedCScan(smoothScan, localMinima)


def __retrieveContours(scanSlices):
    """
    Function to retrieve contours from a list of scan slices. Each scan slice
    will have its maximum amplitude image extracted, and thresholded
    """

    # List to collect the contours
    contours = []

    # Iterate over every slice, and add the found contours
    for scanSlice, (_, _) in scanSlices:
        maximumAmplitudeImage = ustool.getImageByMaximum(scanSlice)
        thresholded = cv2.threshold(maximumAmplitudeImage, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, (3, 3))
        thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, (3, 3))
        contours.extend(cv2.findContours(thresholded, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2])

    return contours


def __refineContoursWithPredictions(contours, thresholdedPredictions, voteThreshold, areaThreshold):
    """
    Function to refine a list of contours, based on the given predictions
    """

    # Normalize the predictions
    normalizedPredictions = thresholdedPredictions.copy()
    normalizedPredictions[normalizedPredictions == 0] = 1E-6
    normalizedPredictions = ustool.normalize(normalizedPredictions)

    refinedContours = []

    # Refine the contours based on the predictions
    for contour in contours:
        indexation = np.zeros((128, 128), np.uint8)
        indexation = (cv2.drawContours(indexation, [contour], -1, 255, -1) == 255)
        vote = normalizedPredictions[indexation].sum() / normalizedPredictions[indexation].size
        if vote >= voteThreshold and cv2.contourArea(contour) >= areaThreshold:
            refinedContours.append(contour)

    return refinedContours


def binaryClassifyCScan(
        cScan,
        classifierModel,
        responseFunctionMethod="localminima",
        responseFunctionPercentage=0.01,
        responseFunctionConstant=None,
        downsamplingLength=128,
        predictionThreshold=0.50,
        contourThreshold=0.75,
        contourAreaThreshold=64,
        medianFiltering=True,
        blockSize=None
):
    """
    Function to perform a binary classification on a C-scan sample.
    Arguments:
        cScan:
            The C-scan that is to be binary classified

        classifierModel:
            The classifier that is to be used

        responseFunctionMethod:
            The method for response function slicing

        responseFunctionPercentage:
            The percentage of N that is used for the response function

        responseFunctionConstant:
            If the method is 'constant', this variable
            is used to define the constant slicing length

        downsamplingLength:
            The downsampling length that is to be used,
            should match the A-scan length the classifier
            will be expecting when predicting

        predictionThreshold:
            The threshold for the classifier predictions
            0 < T < 1

        contourThreshold:
            The threshold for the contour votes
            0 < T < 1

        contourAreaThreshold:
            The threshold for the contour areas
            0 < T < 128^2

        medianFiltering:
            Flag that tells if the C-scan is to be
            median filtered or not before prediction

        blockSize:
            Integer that divides into block sizes for speed,
            by passing blockSize as None will turn blocks off.
            It should be an integer y of the function y = 2^n,
            such that block sizes are 2, 4, 8, 16, etc.

    Returns:
        contours:
            The resulting outer contours that
            was estimated to contain the damage

        predictions:
            The predictions made by the classifier

        timestamps:
            The list containing the timestamps for the different
            processes, which is divided into the following:
                (0) - Total time
                (1) - Median Filtering time
                (2) - Blocks processing time
                (3) - Downsampling processing time
                (4) - Classifier Prediction time
                (5) - Interpolation processing time
                (6) - Slice processing time
                (7) - Contour processing time
                (8) - Other processing time
    """

    # Create a timing list to contain the timestamps
    # It will begin by containing the first timestamp
    timestamps = []
    start = time.perf_counter()
    timestamps.append(start)

    # Create a copy of the C-scan, so that the original
    # is not modified in any way during the method
    originalScan = cScan.copy()

    # If we are to median filter update the scan accordingly
    start = time.perf_counter()
    if medianFiltering:
        originalScan = median_filter(originalScan, (3, 3, 3), mode="constant", cval=0.0)
    end = time.perf_counter()
    timestamps.append(end-start)

    # If we are to create averaged blocks
    start = time.perf_counter()
    if blockSize is not None:

        # Calculate the size variables to be used
        __SIZE = 128
        __NUMBER_OF_BLOCKS = __SIZE//blockSize
        __STRIDE = blockSize

        # Reshape so that we have a flat position dimension
        originalScan = originalScan.reshape((-1, __SIZE*__SIZE))

        # Vectorization sliding window by indexation array
        indexer = (
            __SIZE*np.arange(blockSize)[None, None, :, None]
            + np.arange(blockSize)[None, None, None, :]
            + __SIZE*__STRIDE*np.arange(__NUMBER_OF_BLOCKS)[:, None, None, None]
            + __STRIDE*np.arange(__NUMBER_OF_BLOCKS)[None, :, None, None]
        )

        # Retrieve the sliding windows and
        # reshape the windows to be flat.
        # Afterwards calculate the average of all the windows,
        # so that we have a "averaged block" representation
        # of the original array. Do note that the average is
        # only calculated per timestep, and not in 3D.
        originalScan = originalScan[:, indexer]
        originalScan = np.reshape(originalScan, (
            originalScan.shape[0],
            originalScan.shape[1],
            originalScan.shape[2],
            np.prod(originalScan.shape[3:]))
        )
        originalScan = np.average(originalScan, axis=3)
    end = time.perf_counter()
    timestamps.append(end-start)

    # Downsample the C-scan to the given length
    start = time.perf_counter()
    downsampled = resample(originalScan, downsamplingLength, axis=0)
    downsampled = np.reshape(downsampled, (downsampled.shape[0], np.prod(downsampled.shape[1:])))
    downsampled = np.swapaxes(downsampled, 0, 1)
    end = time.perf_counter()
    timestamps.append(end-start)

    # Retrieve the predictions from the classifier
    start = time.perf_counter()
    predictions = classifierModel.predict(downsampled).reshape(originalScan.shape[1:])
    end = time.perf_counter()
    timestamps.append(end-start)

    # If averaged blocks was used, the predictions
    # need to be rescaled to 128x128 (Interpolated)
    start = time.perf_counter()
    if blockSize is not None:
        predictions = predictions.repeat(blockSize, axis=0).repeat(blockSize, axis=1)
    end = time.perf_counter()
    timestamps.append(end-start)

    # Threshold the predictions, IE. we trust
    # anything that the network is more than X% sure of
    thresholdedPredictions = predictions.copy()
    thresholdedPredictions[predictions >= predictionThreshold] = 1
    thresholdedPredictions[predictions < predictionThreshold] = 0

    # Retrieve all the slice segments, a response function of 1%
    # which is separated by local minima is chosen
    start = time.perf_counter()
    scanSlices = __retrieveSlices(
        cScan,
        responseFunctionMethod,
        percentage=responseFunctionPercentage,
        constant=responseFunctionConstant
    )
    end = time.perf_counter()
    timestamps.append(end-start)

    # Retrieve all the found contours from the list of scan slices,
    # and refine them by looking at the predictions
    start = time.perf_counter()
    contours = __retrieveContours(scanSlices)
    contours = __refineContoursWithPredictions(
        contours,
        thresholdedPredictions,
        contourThreshold,
        contourAreaThreshold
    )

    # Create the union mask, and retrieve the outer contours from this mask
    unionMask = np.zeros((128, 128), np.uint8)
    cv2.drawContours(unionMask, contours, -1, 255, -1)
    outerUnionContours = cv2.findContours(unionMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    end = time.perf_counter()
    timestamps.append(end-start)

    # Update the total elapsed time in the first position
    timestamps[0] = time.perf_counter() - timestamps[0]

    # Add a 'other' timestamp for minor processing done
    timestamps.append(
        2*timestamps[0] - sum(timestamps)
    )

    # Return the result
    return outerUnionContours, predictions, timestamps

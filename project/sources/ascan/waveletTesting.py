"""
Testing discrete wavelet transforms through NNs
"""
import os
import pickle
import numpy as np
import pywt
from sklearn.utils import shuffle
import tensorflow as tf
from tensorflow_core.python.keras.callbacks import EarlyStopping
from project.sources.general.confusionMatrix import getConfusionMatrix


def getDwt(cScan):
    """
    Takes a C-scan as parameter.
    Replaces every A-scan in the C-scan with a DWT of the given A-scan
    :param cScan: C-scan with data, downsampled
    :return: C-scan with DWT waves
    """
    cScanDwt = np.zeros((2, 62, cScan.shape[1], cScan.shape[2]))
    # Iterate through every x,y coordinate in cScan
    for tup in np.ndindex(cScan.shape[1:]):
        cScanDwt[:, :, tup[0], tup[1]] = np.asarray(pywt.dwt(cScan[:, tup[0], tup[1]], wavelet="db38"))
    return cScanDwt


def runDWTFCNN():
    """
    Loads training data, and fits model to data.
    Loads test data, runs tests and prints confusion matrix
    """
    waveletMode = "db38"
    with open("resources/dataset/A-scan dataset/dataset_train_ascan_downsampled.dta", "rb") as dtaFile:
        trainDataset = pickle.load(dtaFile)

    dataset = []
    labels = []

    # Make training set
    for aScan in trainDataset:
        # Each entry will be the features
        dataset.append(pywt.dwt(aScan.data, wavelet=waveletMode))
        # Label is the same as always
        labels.append(aScan.damaged)

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    dataset, labels = shuffle(dataset, labels)

    result, model = waveletNN(dataset, labels)

    with open("resources/dataset/A-scan dataset/dataset_test_ascan_downsampled.dta", "rb") as dtaFile:
        testDataset = pickle.load(dtaFile)

    dataset = []
    labels = []

    # Make training set
    for aScan in testDataset:
        # Each entry will be the features
        dataset.append(pywt.dwt(aScan.data, wavelet=waveletMode))
        # Label is the same as always
        labels.append(aScan.damaged)

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    predictions = model.predict(dataset)
    predictions[predictions >= 0.5] = 1
    predictions[predictions < 0.5] = 0

    getConfusionMatrix(labels, predictions)


def waveletNN(data, labels):
    """
    Definition/structure of the wavelet neural network
    :param data: data to train on
    :param labels: labels to train with
    :return: history of training, model
    """
    # Construct model
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Flatten(input_shape=(2, 101)))

    model.add(tf.keras.layers.Dense(128))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(tf.keras.layers.Dense(64))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(tf.keras.layers.Dense(32))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(16))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(8))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(4))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(2))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(1))

    model.compile(loss="binary_crossentropy",
                  optimizer='adam',
                  metrics=['accuracy'])

    es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=2)
    result = model.fit(data, labels, batch_size=32, validation_split=0.2, epochs=3, callbacks=[es])

    # Save model to file
    modelPath = "resources/model/aScanDWTModel"
    if not os.path.exists(modelPath):
        os.makedirs(modelPath)
        print("Created model folder in: ", modelPath)

    model.save(modelPath)
    print("Saved model in: ", modelPath)

    return result, model


if __name__ == "__main__":
    runDWTFCNN()

# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 16:01:11 2020

@author: StigA
"""

import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import cv2
import os

import sources.general.loader as loader
import sources.general.ustool as ustool
from sources.ascan.binaryClassification import binaryClassifyCScan


def __loadSample():
    """
    Function to return a sample from user choice
    """

    # Retrieve all the h5 files
    filepaths = []
    for root, directories, files in os.walk("./resources/h5"):
        for file in files:
            if file.endswith(".h5"):
                filepaths.append(os.path.normpath(os.path.join(root, file)))

    print("Files found in the '//project/resources/h5' folder...")
    for i, path in enumerate(filepaths):
        print("[", i, "]: ", path)

    # Load the C-scan sample to predict
    return loader.load(filepaths[int(input("Enter number to load: "))])


# ----------------------------------------------------------------------------#
#                                   CODE                                      #
# ----------------------------------------------------------------------------#

cScan, start, end = __loadSample()
model = tf.keras.models.load_model("resources/model/WDASCANFCNN")
contours, predictions, timestamps = binaryClassifyCScan(
                                        cScan,
                                        model,
                                        medianFiltering=False,
                                        blockSize=8,
                                        predictionThreshold=0.50,
                                        contourThreshold=0.75
                                    )

maximumAmplitudeImage = ustool.getImageByMaximum(cScan[start:end])
maximumAmplitudeImage = cv2.cvtColor(maximumAmplitudeImage, cv2.COLOR_GRAY2RGB)
cv2.drawContours(maximumAmplitudeImage, contours, -1, (255, 255, 0), 1)
plt.figure()
plt.imshow(maximumAmplitudeImage)

plt.figure()
plt.imshow((ustool.normalize(predictions)*255).astype(np.uint8), "gray")

print(
      "\n(0) - Total time [{0:.2f}S]".format(timestamps[0])
      + "\n(1) - Median Filtering time [{0:.2f}S]".format(timestamps[1])
      + "\n(2) - Blocks processing time [{0:.2f}S]".format(timestamps[2])
      + "\n(3) - Downsampling processing time [{0:.2f}S]".format(timestamps[3])
      + "\n(4) - Classifier Prediction time [{0:.2f}S]".format(timestamps[4])
      + "\n(5) - Interpolation processing time [{0:.2f}S]".format(timestamps[5])
      + "\n(6) - Slice processing time [{0:.2f}S]".format(timestamps[6])
      + "\n(7) - Contour processing time [{0:.2f}S]".format(timestamps[7])
      + "\n(8) - Other processing time [{0:.2f}S]".format(timestamps[8])
)

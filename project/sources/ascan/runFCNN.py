"""
This file contains code for creating and running an FCNN
"""
import os
import pickle

import numpy as np
import tensorflow as tf
from sklearn.utils import shuffle
from tensorflow_core.python.keras.callbacks import EarlyStopping

from general.confusionMatrix import getConfusionMatrix


def aScanNeuralNet(data, labels):
    """
    Definition of FCNN
    :param data: data to train on
    :param labels: labels to train on
    :return: history, model
    """
    # Construct model
    model = tf.keras.models.Sequential()
    # Flatten input
    model.add(tf.keras.layers.Flatten(input_shape=(128,)))

    # 8 dense (fully connected) layers
    # with varying activation function and dropout layers
    model.add(tf.keras.layers.Dense(128))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(rate=0.2))

    model.add(tf.keras.layers.Dense(128))
    model.add(tf.keras.layers.Dropout(rate=0.2))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.add(tf.keras.layers.Dense(64))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(rate=0.2))

    model.add(tf.keras.layers.Dense(32))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(rate=0.2))

    model.add(tf.keras.layers.Dense(16))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(rate=0.2))

    model.add(tf.keras.layers.Dense(4))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(rate=0.2))

    model.add(tf.keras.layers.Dense(2))
    model.add(tf.keras.layers.Activation('sigmoid'))
    model.add(tf.keras.layers.Dropout(rate=0.2))

    model.add(tf.keras.layers.Dense(1))
    model.add(tf.keras.layers.Activation('sigmoid'))

    model.compile(loss="binary_crossentropy",
                  optimizer="adam",
                  metrics=['accuracy'])

    earlyStopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=3)
    result = model.fit(data, labels, batch_size=16, validation_split=0.2, epochs=100, callbacks=[earlyStopping])

    # Save model to file
    modelPath = "resources/model/downsampledAScanFCNNModel"
    if not os.path.exists(modelPath):
        os.makedirs(modelPath)
        print("Created model folder in: ", modelPath)

    model.save(modelPath)
    print("Saved downsampled A-scan FCNN model in: ", modelPath)

    return result, model


def main():
    """
    Loads model if it exists.
    If not, loads training data, and fits model to data.
    Loads test data and runs test, prints confusion matrix.
    :return: nothing
    """
    # Check if the model is already saved as a file, and if so, open it
    modelPath = "resources/model/downsampledAScanFCNNModel"
    if os.path.isdir(modelPath):
        model = tf.keras.models.load_model(modelPath)
        print("Loaded existing model from: ", modelPath, " - skipping training...")

    # Model does not exist, train it and save it to file
    else:
        # Open the training dataset
        with open('resources/dataset/A-scan dataset/dataset_train_ascan_downsampled.dta', 'rb') as dtaFile:
            trainDataset = pickle.load(dtaFile)

        # Retrieve data and label for every sample
        trainDataSamples: list = []
        trainLabelSamples: list = []

        for aScan in trainDataset:
            trainDataSamples.append(aScan.data)
            trainLabelSamples.append(int(aScan.damaged))

        # Shuffle the training dataset
        trainDataSamples, trainLabelSamples = shuffle(trainDataSamples, trainLabelSamples)

        # Convert the lists to np arrays
        trainData: np.ndarray = np.asarray(trainDataSamples)
        trainLabels: np.ndarray = np.asarray(trainLabelSamples)

        # Train the network
        history, model = aScanNeuralNet(trainData, trainLabels)

    # Open the testing dataset
    with open('resources/dataset/A-scan dataset/dataset_test_ascan_downsampled.dta', 'rb') as dtaFile:
        testDataset = pickle.load(dtaFile)

    # Retrieve data and label for every sample
    testDataSamples: list = []
    testLabelSamples: list = []

    for aScan in testDataset:
        testDataSamples.append(aScan.data)
        testLabelSamples.append(int(aScan.damaged))

    # Shuffle the testing dataset
    testDataSamples, testLabelSamples = shuffle(testDataSamples, testLabelSamples)

    # Convert the data to np array
    testData: np.ndarray = np.asarray(testDataSamples)

    # Get testing predictions
    predictions = model.predict(testData)

    # Convert to binary
    predictions[predictions >= 0.5] = 1
    predictions[predictions < 0.5] = 0

    # Retrieve the confusion matrix + classification scores
    getConfusionMatrix(testLabelSamples, predictions)


if __name__ == "__main__":
    main()

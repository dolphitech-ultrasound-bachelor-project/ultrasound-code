"""
Random forest classifier for A-scans

"""
import pickle

import numpy as np
from sklearn import svm
from sklearn.utils import shuffle

from general.confusionMatrix import getConfusionMatrix


def runSVM():
    """
    Loads training data, fits classifier to data.
    Loads test data, runs test and outputs confusion matrix
    """
    # Opens datafile
    with open('resources/dataset/A-scan dataset/dataset_train_ascan_downsampled.dta', 'rb') as input1:
        trainingSet = pickle.load(input1)

    # Creates four empty lists data/labels for train/test
    dataset = []
    labels = []

    # Make training set
    for aScan in trainingSet:
        dataset.append(aScan.data)
        labels.append(aScan.damaged)

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    dataset, labels = shuffle(dataset, labels)

    clf = svm.SVC(verbose=True)
    clf.fit(dataset, labels)

    # Save model
    with open('resources/model/ascanSVM.model', 'wb') as output:
        pickle.dump(clf, output, pickle.HIGHEST_PROTOCOL)

    # TESTING:
    with open('resources/dataset/A-scan dataset/dataset_test_ascan_downsampled.dta', 'rb') as inp:
        testSet = pickle.load(inp)

    # Shuffle datafile
    testSet = shuffle(testSet)

    # Creates empty lists data/labels
    dataset = []
    labels = []

    # Make testing set
    for aScan in testSet:
        dataset.append(aScan.data)
        labels.append(aScan.damaged)

    dataset = np.asarray(dataset)
    labels = np.asarray(labels)

    predictions = clf.predict(dataset)

    getConfusionMatrix(labels, predictions)


if __name__ == "__main__":
    runSVM()

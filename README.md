# Ultrasound Code
[![pipeline status](https://gitlab.com/dolphitech-ultrasound-bachelor-project/ultrasound-code/badges/master/pipeline.svg)](https://gitlab.com/dolphitech-ultrasound-bachelor-project/ultrasound-code/commits/master)

This project contains code for the bachelor thesis undertaken by Stian Næss Bratlie, Stig André Rosenlund, and Franz Arneberg Vrolijk.
The code and thesis was developed during studies at the [Norwegian University of Science and Technology](https://www.ntnu.edu/), on behalf of [Dolphitech](https://www.dolphitech.com/), 
a company specialized in ultrasound-based non-destructive testing with their flagship product, [Dolphicam 2.0](https://www.dolphitech.com/our-products/dolphicam2/).

All code is hereby openly available as stipulated by the MIT license.
The thesis report is available at [not yet](https://ntnuopen.ntnu.no/ntnu-xmlui/handle/11250/227491/discover).

The folder structure is important in the project, as the python code
assumes the resource folder, and others, are placed such that code can reach
it from either the working directory (which is the "project" folder), 
or as relative paths from the code itself.

The resources folder should be supplied with datasets, individual samples, and pretrained models from [google drive](https://drive.google.com/drive/folders/1gmfbLCcQA12AqWxkdGIOgoEBF5Li-m6q?usp=sharing).

Instructions on running this code can be found [here](https://docs.google.com/document/d/1CHNT0GPPr3fiqTSkA-X6xDuoSJFGTFULoJMdKQNBbag/edit?usp=sharing).